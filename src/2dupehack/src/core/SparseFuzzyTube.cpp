// Copyright 2010,2011,2012,2013,2014,2015 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of multidupehack.

// multidupehack is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// multidupehack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with multidupehack; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "SparseFuzzyTube.h"

SparseFuzzyTube::SparseFuzzyTube() : tube()
{
}

SparseFuzzyTube* SparseFuzzyTube::clone() const
{
  return new SparseFuzzyTube(*this);
}

DenseFuzzyTube* SparseFuzzyTube::getDenseRepresentation() const
{
  return new DenseFuzzyTube(tube);
}

void SparseFuzzyTube::print(vector<unsigned int>& prefix, ostream& out) const
{
  for (const pair<unsigned int, unsigned int>& hyperplane : tube)
    {
      printTuple(prefix, hyperplane.first, 1 - static_cast<double>(hyperplane.second) / Attribute::noisePerUnit, out);
    }
}

bool SparseFuzzyTube::setSelfLoopsInSymmetricAttributes(const unsigned int hyperplaneId, const unsigned int lastSymmetricAttributeId, const unsigned int dimensionId)
{
  // Necessarily symmetric
  if (densityThreshold == 0)
    {
      // *this must be turned into a DenseFuzzyTube
      return true;
    }
  tube.push_back(pair<unsigned int, unsigned int>(hyperplaneId, 0));
  return false;
}

bool SparseFuzzyTube::setTuple(const vector<unsigned int>& tuple, const unsigned int membership, vector<unsigned int>::const_iterator attributeIdIt, vector<vector<unsigned int>>::const_iterator oldIds2NewIdsIt, const vector<Attribute*>::iterator attributeIt)
{
  const unsigned int element = oldIds2NewIdsIt->at(tuple[*attributeIdIt]);
  (*attributeIt)->subtractPotentialNoise(element, membership);
  tube.push_back(pair<unsigned int, unsigned int>(element, Attribute::noisePerUnit - membership));
  return tube.size() >= densityThreshold;
}

void SparseFuzzyTube::sortTubes()
{
  tube.resize(tube.size());
  sort(tube.begin(), tube.end());
}

unsigned int SparseFuzzyTube::setPresent(const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  // *this necessarily relates to the present attribute
  return noiseOnValue((*attributeIt)->getChosenValue().getDataId());
}

unsigned int SparseFuzzyTube::setPresentAfterPotentialOrAbsentUsed(const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  // *this necessarily relates to the present attribute
  const Value& presentValue = (*attributeIt)->getChosenValue();
  const unsigned int noise = noiseOnValue(presentValue.getDataId());
  return noise;
}

unsigned int SparseFuzzyTube::noiseOnValue(const unsigned int valueDataId) const
{
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  const vector<pair<unsigned int, unsigned int>>::const_iterator noisyTupleIt = lower_bound(tube.begin(), tubeEnd, pair<unsigned int, unsigned int>(valueDataId, 0));
  if (noisyTupleIt != tubeEnd && noisyTupleIt->first == valueDataId)
    {
      return noisyTupleIt->second;
    }
  return Attribute::noisePerUnit;
}

unsigned int SparseFuzzyTube::noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const
{
  unsigned int noise = 0;
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, unsigned int>>::const_iterator tubeBegin = tube.begin();
  for (const unsigned int id : *dimensionIt)
    {
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, unsigned int>(id, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == id)
	{
	  noise += tubeBegin->second;
	}
      else
	{
	  noise += Attribute::noisePerUnit;
	}
    }
  return noise;
}


unsigned int SparseFuzzyTube::presentFixPresentValuesAfterPresentValueMet(Attribute& currentAttribute) const
{
  unsigned int newNoise = 0;
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, unsigned int>>::const_iterator tubeBegin = tube.begin();
  const vector<Value*>::iterator end = currentAttribute.presentEnd();
  for (vector<Value*>::iterator valueIt = currentAttribute.presentBegin(); valueIt != end; ++valueIt)
    {
      const unsigned int valueDataId = (*valueIt)->getDataId();
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, unsigned int>(valueDataId, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == valueDataId)
	{
	  (*valueIt)->addPresentNoise(tubeBegin->second);
	  newNoise += tubeBegin->second;
	}
      else
	{
	  (*valueIt)->addPresentNoise(Attribute::noisePerUnit);
	  newNoise += Attribute::noisePerUnit;
	}
    }
  return newNoise;
}

void SparseFuzzyTube::presentFixPotentialOrAbsentValuesAfterPresentValueMet(Attribute& currentAttribute) const
{
  const vector<Value*>::iterator absentBegin = currentAttribute.absentBegin();
  presentFixPotentialOrAbsentValuesAfterPresentValueMet(currentAttribute.potentialBegin(), absentBegin);
  presentFixPotentialOrAbsentValuesAfterPresentValueMet(absentBegin, currentAttribute.absentEnd());
}

void SparseFuzzyTube::presentFixPotentialOrAbsentValuesAfterPresentValueMet(const vector<Value*>::iterator begin, const vector<Value*>::iterator end) const
{
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, unsigned int>>::const_iterator tubeBegin = tube.begin();
  for (vector<Value*>::iterator valueIt = begin; valueIt != end; ++valueIt)
    {
      const unsigned int valueDataId = (*valueIt)->getDataId();
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, unsigned int>(valueDataId, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == valueDataId)
	{
	  (*valueIt)->addPresentNoise(tubeBegin->second);
	}
      else
	{
	  (*valueIt)->addPresentNoise(Attribute::noisePerUnit);
	}
    }
}


unsigned int SparseFuzzyTube::presentFixPresentValuesAfterPresentValueMetAndPotentialOrAbsentUsed(const Attribute& currentAttribute) const
{
  unsigned int newNoise = 0;
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, unsigned int>>::const_iterator tubeBegin = tube.begin();
  const vector<Value*>::const_iterator end = currentAttribute.presentEnd();
  for (vector<Value*>::const_iterator valueIt = currentAttribute.presentBegin(); valueIt != end; ++valueIt)
    {
      const unsigned int valueDataId = (*valueIt)->getDataId();
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, unsigned int>(valueDataId, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == valueDataId)
	{
	  newNoise += tubeBegin->second;
	}
      else
	{
	  newNoise += Attribute::noisePerUnit;
	}
    }
  return newNoise;
}


unsigned int SparseFuzzyTube::noiseOnValues(const vector<unsigned int>& valueDataIds) const
{
  unsigned int oldNoise = 0;
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, unsigned int>>::const_iterator tubeBegin = tube.begin();
  for (const unsigned int valueDataId : valueDataIds)
    {
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, unsigned int>(valueDataId, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == valueDataId)
	{
	  oldNoise += tubeBegin->second;
	}
      else
	{
	  oldNoise += Attribute::noisePerUnit;
	}
    }
  return oldNoise;
}

unsigned int SparseFuzzyTube::irrelevantFixPresentOrPotentialValuesAfterIrrelevantValueMet(Attribute& currentAttribute) const
{
  const vector<Value*>::iterator potentialBegin = currentAttribute.potentialBegin();
  return irrelevantFixPresentOrPotentialValuesAfterIrrelevantValueMet(currentAttribute.presentBegin(), potentialBegin) + irrelevantFixPresentOrPotentialValuesAfterIrrelevantValueMet(potentialBegin, currentAttribute.potentialEnd()) + noiseOnIrrelevant(currentAttribute);
}

unsigned int SparseFuzzyTube::irrelevantFixPresentOrPotentialValuesAfterIrrelevantValueMet(const vector<Value*>::iterator begin, const vector<Value*>::iterator end) const
{
  unsigned int oldNoise = 0;
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, unsigned int>>::const_iterator tubeBegin = tube.begin();
  for (vector<Value*>::iterator valueIt = begin; valueIt != end; ++valueIt)
    {
      const unsigned int valueDataId = (*valueIt)->getDataId();
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, unsigned int>(valueDataId, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == valueDataId)
      	{
      	  (*valueIt)->subtractPotentialNoise(tubeBegin->second);
      	  oldNoise += tubeBegin->second;
      	}
      else
      	{
      	  (*valueIt)->subtractPotentialNoise(Attribute::noisePerUnit);
      	  oldNoise += Attribute::noisePerUnit;
      	}
    }
  return oldNoise;
}

unsigned int SparseFuzzyTube::noiseOnIrrelevant(const Attribute& currentAttribute) const
{
  unsigned int oldNoise = 0;
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, unsigned int>>::const_iterator tubeBegin = tube.begin();
  vector<Value*>::const_iterator end = currentAttribute.irrelevantEnd();
  for (vector<Value*>::const_iterator valueIt = currentAttribute.irrelevantBegin(); valueIt != end; ++valueIt)
    {
      const unsigned int valueDataId = (*valueIt)->getDataId();
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, unsigned int>(valueDataId, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == valueDataId)
	{
	  oldNoise += tubeBegin->second;
	}
      else
	{
	  oldNoise += Attribute::noisePerUnit;
	}
    }
  return oldNoise;
}


unsigned int SparseFuzzyTube::absentFixPresentOrPotentialValuesAfterAbsentValueMet(Attribute& currentAttribute) const
{
  const vector<Value*>::iterator potentialBegin = currentAttribute.potentialBegin();
  return absentFixPresentOrPotentialValuesAfterAbsentValueMet(currentAttribute.presentBegin(), potentialBegin) + absentFixPresentOrPotentialValuesAfterAbsentValueMet(potentialBegin, currentAttribute.potentialEnd()) + noiseOnIrrelevant(currentAttribute);
}

unsigned int SparseFuzzyTube::absentFixPresentOrPotentialValuesAfterAbsentValueMet(const vector<Value*>::iterator begin, const vector<Value*>::iterator end) const
{
  unsigned int oldNoise = 0;
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, unsigned int>>::const_iterator tubeBegin = tube.begin();
  for (vector<Value*>::iterator valueIt = begin; valueIt != end; ++valueIt)
    {
      const unsigned int valueDataId = (*valueIt)->getDataId();
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, unsigned int>(valueDataId, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == valueDataId)
      	{
      	  (*valueIt)->subtractPotentialNoise(tubeBegin->second);
      	  oldNoise += tubeBegin->second;
      	}
      else
      	{
      	  (*valueIt)->subtractPotentialNoise(Attribute::noisePerUnit);
      	  oldNoise += Attribute::noisePerUnit;
      	}
    }
  return oldNoise;
}

unsigned int SparseFuzzyTube::absentFixPresentOrPotentialValuesAfterAbsentValueMetAndAbsentUsed(Attribute& currentAttribute) const
{
  const vector<Value*>::const_iterator potentialBegin = currentAttribute.potentialBegin();
  return absentFixPresentOrPotentialValuesAfterAbsentValueMetAndAbsentUsed(currentAttribute.presentBegin(), potentialBegin) + absentFixPresentOrPotentialValuesAfterAbsentValueMetAndAbsentUsed(potentialBegin, currentAttribute.potentialEnd()) + noiseOnIrrelevant(currentAttribute);
}

unsigned int SparseFuzzyTube::absentFixPresentOrPotentialValuesAfterAbsentValueMetAndAbsentUsed(const vector<Value*>::const_iterator begin, const vector<Value*>::const_iterator end) const
{
  unsigned int oldNoise = 0;
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, unsigned int>>::const_iterator tubeBegin = tube.begin();
  for (vector<Value*>::const_iterator valueIt = begin; valueIt != end; ++valueIt)
    {
      const unsigned int valueDataId = (*valueIt)->getDataId();
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, unsigned int>(valueDataId, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == valueDataId)
	{
	  oldNoise += tubeBegin->second;
	}
      else
	{
	  oldNoise += Attribute::noisePerUnit;
	}
    }
  return oldNoise;
}

void SparseFuzzyTube::absentFixAbsentValuesAfterAbsentValueMet(const vector<Value*>::iterator absentBegin, const vector<Value*>::iterator absentEnd) const
{
  const vector<pair<unsigned int, unsigned int>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, unsigned int>>::const_iterator tubeBegin = tube.begin();
  for (vector<Value*>::iterator valueIt = absentBegin; valueIt != absentEnd; ++valueIt)
    {
      const unsigned int valueDataId = (*valueIt)->getDataId();
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, unsigned int>(valueDataId, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == valueDataId)
	{
	  (*valueIt)->subtractPotentialNoise(tubeBegin->second);
	}
      else
	{
	  (*valueIt)->subtractPotentialNoise(Attribute::noisePerUnit);
	}
    }
}

#ifdef ASSERT
unsigned int SparseFuzzyTube::noiseSumOnPresent(const vector<Attribute*>::const_iterator valueAttributeIt, const Value& value, const vector<Attribute*>::const_iterator attributeIt) const
{
  if (attributeIt == valueAttributeIt)
    {
      return noiseOnValue(value.getDataId());
    }
  unsigned int noise = 0;
  const vector<Value*>::const_iterator end = (*attributeIt)->presentEnd();
  for (vector<Value*>::const_iterator valueIt = (*attributeIt)->presentBegin(); valueIt != end; ++valueIt)
    {
      noise += noiseOnValue((*valueIt)->getDataId());
    }
  return noise;
}

unsigned int SparseFuzzyTube::noiseSumOnPresentAndPotential(const vector<Attribute*>::const_iterator valueAttributeIt, const Value& value, const vector<Attribute*>::const_iterator attributeIt) const
{
  if (attributeIt == valueAttributeIt)
    {
      return noiseOnValue(value.getDataId());
    }
  unsigned int noise = 0;
  vector<Value*>::const_iterator end = (*attributeIt)->irrelevantEnd();
  for (vector<Value*>::const_iterator valueIt = (*attributeIt)->presentBegin(); valueIt != end; ++valueIt)
    {
      noise += noiseOnValue((*valueIt)->getDataId());
    }
  return noise;
}
#endif
