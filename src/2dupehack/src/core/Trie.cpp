// Copyright 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017 Loïc (lcerf@dcc.ufmg.br)

// This file is part of multidupehack.

// multidupehack is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// multidupehack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with multidupehack; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "Trie.h"

NoNoiseTube Trie::noNoiseTube = NoNoiseTube();

Trie::Trie(): hyperplanes()
{
}

Trie::Trie(const Trie& otherTrie): hyperplanes()
{
  copy(otherTrie);
}

Trie::Trie(Trie&& otherTrie): hyperplanes(std::move(otherTrie.hyperplanes))
{
}

Trie::Trie(const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd, const float densityThresholdParam): Trie(cardinalityIt, cardinalityEnd)
{
  cardinalityOfLastAttribute = *(cardinalityEnd - 1);
  if (Attribute::noisePerUnit == 1)
    {
      densityThreshold = densityThresholdParam * cardinalityOfLastAttribute / sizeof(unsigned int) / 8;
    }
  else
    {
      densityThreshold = densityThresholdParam * cardinalityOfLastAttribute / 2;
    }
}

Trie::Trie(const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd): hyperplanes()
{
  const unsigned int cardinality = *cardinalityIt;
  hyperplanes.reserve(cardinality);
  if (cardinalityIt + 2 == cardinalityEnd)
    {
      if (Attribute::noisePerUnit == 1)
      {
	for (unsigned int hyperplaneId = 0; hyperplaneId != cardinality; ++hyperplaneId)
	  {
	    hyperplanes.push_back(new SparseCrispTube());
	  }
	return;
      }
      for (unsigned int hyperplaneId = 0; hyperplaneId != cardinality; ++hyperplaneId)
      {
	hyperplanes.push_back(new SparseFuzzyTube());
      }
      return;
    }
  const vector<unsigned int>::const_iterator nbOfElementsInNextDiIt = cardinalityIt + 1;
  for (unsigned int hyperplaneId = 0; hyperplaneId != cardinality; ++hyperplaneId)
    {
      hyperplanes.push_back(new Trie(nbOfElementsInNextDiIt, cardinalityEnd));
    }
}

Trie::~Trie()
{
  for (AbstractData* hyperplane : hyperplanes)
    {
      if (hyperplane != &noNoiseTube)
	{
	  delete hyperplane;
	}
    }
}

Trie* Trie::clone() const
{
  return new Trie(*this);
}

Trie& Trie::operator=(const Trie& otherTrie)
{
  copy(otherTrie);
  return *this;
}

Trie& Trie::operator=(Trie&& otherTrie)
{
  hyperplanes = std::move(otherTrie.hyperplanes);
  return *this;
}

ostream& operator<<(ostream& out, const Trie& trie)
{
  vector<unsigned int> prefix;
  trie.print(prefix, out);
  return out;
}

void Trie::copy(const Trie& otherTrie)
{
  hyperplanes.reserve(otherTrie.hyperplanes.size());
  for (const AbstractData* hyperplane : otherTrie.hyperplanes)
    {
      hyperplanes.push_back(hyperplane->clone());
    }
}

void Trie::print(vector<unsigned int>& prefix, ostream& out) const
{
  unsigned int hyperplaneId = 0;
  for (AbstractData* hyperplane : hyperplanes)
    {
      prefix.push_back(hyperplaneId++);
      hyperplane->print(prefix, out);
      prefix.pop_back();
    }
}

void Trie::setHyperplane(const unsigned int hyperplaneOldId, const unordered_map<vector<unsigned int>, double, vector_hash<unsigned int>>::const_iterator begin, const unordered_map<vector<unsigned int>, double, vector_hash<unsigned int>>::const_iterator end, const vector<unsigned int>& attributeOrder, const vector<vector<unsigned int>>& oldIds2NewIds, vector<Attribute*>& attributes)
{
  const vector<Attribute*>::iterator nextAttributeIt =attributes.begin() + 1;
  const unsigned int hyperplaneId = oldIds2NewIds.front().at(hyperplaneOldId);
  AbstractData* hyperplane = dynamic_cast<AbstractData*>(hyperplanes[hyperplaneId]);
  for (unordered_map<vector<unsigned int>, double, vector_hash<unsigned int>>::const_iterator tupleIt = begin; tupleIt != end; ++tupleIt)
    {
      unsigned int membership = ceil(tupleIt->second * Attribute::noisePerUnit); // ceil to guarantee that every pattern to be returned is returned
      setTuple(hyperplane, tupleIt->first, membership, attributeOrder.begin(), ++oldIds2NewIds.begin(), nextAttributeIt);
      hyperplanes[hyperplaneId] = hyperplane; // since hyperplane cannot be a reference to a pointer anymore, we have to reaffect it manually
      attributes.front()->subtractPotentialNoise(hyperplaneId, membership);
    }
  sortTubes();
}

void Trie::setSelfLoops(const unsigned int firstSymmetricAttributeId, const unsigned int lastSymmetricAttributeId)
{
  setSelfLoopsBeforeSymmetricAttributes(firstSymmetricAttributeId, lastSymmetricAttributeId, 0);
}

void Trie::setSelfLoopsBeforeSymmetricAttributes(const unsigned int firstSymmetricAttributeId, const unsigned int lastSymmetricAttributeId, const unsigned int dimensionId)
{
  const unsigned int nextDimensionId = dimensionId + 1;
  if (dimensionId == firstSymmetricAttributeId)
    {
      unsigned int hyperplaneId = 0;
      for (AbstractData*& hyperplane : hyperplanes)
	{
	  setSelfLoopsInSymmetricAttributes(hyperplane, hyperplaneId++, lastSymmetricAttributeId, nextDimensionId);
	}
      return;
    }
  for (AbstractData* hyperplane : hyperplanes)
    {
      dynamic_cast<Trie*>(hyperplane)->setSelfLoopsBeforeSymmetricAttributes(firstSymmetricAttributeId, lastSymmetricAttributeId, nextDimensionId);
    }
}

void Trie::setSelfLoopsInSymmetricAttributes(AbstractData*& hyperplane, const unsigned int hyperplaneId, const unsigned int lastSymmetricAttributeId, const unsigned int nextDimensionId)
{
  if (hyperplane->setSelfLoopsInSymmetricAttributes(hyperplaneId, lastSymmetricAttributeId, nextDimensionId))
    {
      delete hyperplane;
      if (Attribute::noisePerUnit == 1)
	{
	  hyperplane = new DenseCrispTube(hyperplaneId);
	  return;
	}
      hyperplane = new DenseFuzzyTube(hyperplaneId);
    }
}

bool Trie::setSelfLoopsInSymmetricAttributes(const unsigned int hyperplaneId, const unsigned int lastSymmetricAttributeId, const unsigned int dimensionId)
{
  AbstractData*& hyperplane = hyperplanes[hyperplaneId];
  if (dimensionId == lastSymmetricAttributeId)
    {
      if (dynamic_cast<Tube*>(hyperplane))
	{
	  delete hyperplane;
	  hyperplane = &noNoiseTube;
	  return false;
	}
      dynamic_cast<Trie*>(hyperplane)->setSelfLoopsAfterSymmetricAttributes();
      return false;
    }
  setSelfLoopsInSymmetricAttributes(hyperplane, hyperplaneId, lastSymmetricAttributeId, dimensionId + 1);
  return false;
}

void Trie::setSelfLoopsAfterSymmetricAttributes()
{
  const vector<AbstractData*>::iterator end = hyperplanes.end();
  vector<AbstractData*>::iterator hyperplaneIt = hyperplanes.begin();
  if (dynamic_cast<Tube*>(*hyperplaneIt))
    {
      for (; hyperplaneIt != end; ++hyperplaneIt)
	{
	  delete *hyperplaneIt;
	  *hyperplaneIt = &noNoiseTube;
	}
      return;
    }
  for (; hyperplaneIt != end; ++hyperplaneIt)
    {
      dynamic_cast<Trie*>(*hyperplaneIt)->setSelfLoopsAfterSymmetricAttributes();
    }
}

bool Trie::setTuple(const vector<unsigned int>& tuple, const unsigned int membership, vector<unsigned int>::const_iterator attributeIdIt, vector<vector<unsigned int>>::const_iterator oldIds2NewIdsIt, const vector<Attribute*>::iterator attributeIt)
{
  const unsigned int element = oldIds2NewIdsIt->at(tuple[*attributeIdIt]);
  (*attributeIt)->subtractPotentialNoise(element, membership);
  setTuple(hyperplanes[element], tuple, membership, ++attributeIdIt, ++oldIds2NewIdsIt, attributeIt + 1);
  return false;
}

void Trie::setTuple(AbstractData*& hyperplane, const vector<unsigned int>& tuple, const unsigned int membership, vector<unsigned int>::const_iterator attributeIdIt, vector<vector<unsigned int>>::const_iterator oldIds2NewIdsIt, const vector<Attribute*>::iterator nextAttributeIt)
{
  if (hyperplane->setTuple(tuple, membership, attributeIdIt, oldIds2NewIdsIt, nextAttributeIt))
    {
      AbstractData* newHyperplane;
      if (Attribute::noisePerUnit == 1)
	{
	  newHyperplane = dynamic_cast<SparseCrispTube*>(hyperplane)->getDenseRepresentation();
	}
      else
	{
	  newHyperplane = dynamic_cast<SparseFuzzyTube*>(hyperplane)->getDenseRepresentation();
	}
      delete hyperplane;
      hyperplane = newHyperplane;
    }
}

unsigned int Trie::setPresent(const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == presentAttributeIt)
    {
      Value& presentValue = (*presentAttributeIt)->getChosenValue();
      return (hyperplanes[presentValue.getDataId()])->setPresentAfterPresentValueMet(nextAttributeIt);
    }
  presentFixPotentialOrAbsentValues((*attributeIt)->potentialBegin(), (*attributeIt)->absentEnd(), presentAttributeIt, nextAttributeIt);
  return presentFixPresentValues(**attributeIt, presentAttributeIt, nextAttributeIt);
}

unsigned int Trie::setSymmetricPresent(const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == presentAttributeIt)
    {
      // *this necessarily relates to the first symmetric attribute
      presentFixPotentialOrAbsentValues((*attributeIt)->potentialBegin() + 1, (*attributeIt)->absentEnd(), nextAttributeIt, nextAttributeIt);
      const unsigned int newNoise = presentFixPresentValues(**attributeIt, nextAttributeIt, nextAttributeIt);
      Value& presentValue = (*presentAttributeIt)->getChosenValue();
      return newNoise + hyperplanes[presentValue.getDataId()]->setSymmetricPresentAfterPresentValueMet(nextAttributeIt);
    }
  presentFixPotentialOrAbsentValuesBeforeSymmetricAttributes(**attributeIt, presentAttributeIt, nextAttributeIt);
  return presentFixPresentValuesBeforeSymmetricAttributes(**attributeIt, presentAttributeIt, nextAttributeIt);
}

unsigned int Trie::setSymmetricPresentAfterPotentialOrAbsentUsed(const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  if (attributeIt == presentAttributeIt)
    {
      // *this necessarily relates to the first symmetric attribute
      const Value& presentValue = (*presentAttributeIt)->getChosenValue();
      const unsigned int newNoiseInHyperplane = hyperplanes[presentValue.getDataId()]->setPresentAfterPresentValueMetAndPotentialOrAbsentUsed(attributeIt + 1);
      return newNoiseInHyperplane + presentFixPresentValuesAfterPotentialOrAbsentUsed(**attributeIt, presentAttributeIt + 1, attributeIt + 1);
    }
  return presentFixPresentValuesBeforeSymmetricAttributesAfterPotentialOrAbsentUsed(**attributeIt, presentAttributeIt, attributeIt + 1);
}

unsigned int Trie::setPresentAfterPresentValueMetAndPotentialOrAbsentUsed(const vector<Attribute*>::iterator attributeIt) const
{
  return presentFixPresentValuesAfterPresentValueMetAndPotentialOrAbsentUsed(**attributeIt, attributeIt + 1);
}

unsigned int Trie::setPresentAfterPresentValueMet(const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  presentFixPotentialOrAbsentValuesAfterPresentValueMet((*attributeIt)->potentialBegin(), (*attributeIt)->absentEnd(), nextAttributeIt);
  return presentFixPresentValuesAfterPresentValueMet(**attributeIt, nextAttributeIt);
}

unsigned int Trie::setSymmetricPresentAfterPresentValueMet(const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  presentFixPotentialOrAbsentValuesAfterPresentValueMet((*attributeIt)->potentialBegin() + 1, (*attributeIt)->absentEnd(), nextAttributeIt);
  return presentFixPresentValuesAfterPresentValueMet(**attributeIt, nextAttributeIt);
}

unsigned int Trie::setPresentAfterPotentialOrAbsentUsed(const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  if (attributeIt == presentAttributeIt)
    {
      const Value& presentValue = (*presentAttributeIt)->getChosenValue();
      const unsigned int newNoiseInHyperplane = hyperplanes[presentValue.getDataId()]->setPresentAfterPresentValueMetAndPotentialOrAbsentUsed(attributeIt + 1);
      return newNoiseInHyperplane;
    }
  return presentFixPresentValuesAfterPotentialOrAbsentUsed(**attributeIt, presentAttributeIt, attributeIt + 1);
}

unsigned int Trie::setAbsent(const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == absentAttributeIt)
    {
      return hyperplanes[(*absentAttributeIt)->getChosenAbsentValueDataId()]->setAbsentAfterAbsentValueMet(nextAttributeIt);
    }
  absentFixAbsentValues(**attributeIt, absentAttributeIt, nextAttributeIt);
  return absentFixPresentOrPotentialValues(**attributeIt, absentAttributeIt, nextAttributeIt);
}

unsigned int Trie::setIrrelevant(const vector<Attribute*>::iterator irrelevantAttributeIt, const vector<unsigned int>& irrelevantValueDataIds, const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == irrelevantAttributeIt)
    {
      unsigned int oldNoise = 0;
      for (const unsigned int irrelevantValueDataId : irrelevantValueDataIds)
	{
	  oldNoise += hyperplanes[irrelevantValueDataId]->setAbsentAfterAbsentValueMet(nextAttributeIt);
	}
      return oldNoise;
    }
  irrelevantFixAbsentValues(**attributeIt, irrelevantAttributeIt, irrelevantValueDataIds, nextAttributeIt);
  return irrelevantFixPresentOrPotentialValues(**attributeIt, irrelevantAttributeIt, irrelevantValueDataIds, nextAttributeIt);
}

unsigned int Trie::setSymmetricAbsent(const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == absentAttributeIt)
    {
      // *this necessarily relates to the first symmetric attribute
      unsigned int absentValueDataId = (*absentAttributeIt)->getChosenAbsentValueDataId();
      absentFixAbsentValues(**attributeIt, nextAttributeIt, nextAttributeIt);
      return dynamic_cast<AbstractData*>(hyperplanes[absentValueDataId])->setSymmetricAbsentAfterAbsentValueMet(nextAttributeIt) + absentFixPresentOrPotentialValues(**attributeIt, nextAttributeIt, nextAttributeIt);
    }
  absentFixAbsentValuesBeforeSymmetricAttributes(**attributeIt, absentAttributeIt, nextAttributeIt);
  return absentFixPresentOrPotentialValuesBeforeSymmetricAttributes(**attributeIt, absentAttributeIt, nextAttributeIt);
}

unsigned int Trie::setAbsentAfterAbsentValueMet(const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  absentFixAbsentValuesAfterAbsentValueMet((*attributeIt)->absentBegin(), (*attributeIt)->absentEnd(), nextAttributeIt);
  return absentFixPresentOrPotentialValuesAfterAbsentValueMet(**attributeIt, nextAttributeIt);
}

unsigned int Trie::setSymmetricAbsentAfterAbsentValueMet(const vector<Attribute*>::iterator attributeIt) const
{
  // *this necessarily relates to the second symmetric attribute
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  // The first absent value actually is the value set absent and there is no noise to be found at the intersection of a vertex (seen as an outgoing vertex) and itself (seen as an ingoing vertex)
  absentFixAbsentValuesAfterAbsentValueMet((*attributeIt)->absentBegin() + 1, (*attributeIt)->absentEnd(), nextAttributeIt);
  return absentFixPresentOrPotentialValuesAfterAbsentValueMet(**attributeIt, nextAttributeIt);
}

unsigned int Trie::setAbsentAfterAbsentUsed(const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == absentAttributeIt)
    {
      return hyperplanes[(*absentAttributeIt)->getChosenAbsentValueDataId()]->setAbsentAfterAbsentValueMetAndAbsentUsed(nextAttributeIt);
    }
  return absentFixPresentOrPotentialValuesAfterAbsentUsed(**attributeIt, absentAttributeIt, nextAttributeIt);
}

unsigned int Trie::setIrrelevantAfterAbsentUsed(const vector<Attribute*>::iterator irrelevantAttributeIt, const vector<unsigned int>& irrelevantValueDataIds, const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == irrelevantAttributeIt)
    {
      unsigned int oldNoise = 0;
      for (const unsigned int irrelevantValueDataId : irrelevantValueDataIds)
    {
      oldNoise += hyperplanes[irrelevantValueDataId]->setAbsentAfterAbsentValueMetAndAbsentUsed(nextAttributeIt);
    }
      return oldNoise;
    }
  return irrelevantFixPresentOrPotentialValuesAfterAbsentUsed(**attributeIt, irrelevantAttributeIt, irrelevantValueDataIds, nextAttributeIt);
}

unsigned int Trie::setSymmetricAbsentAfterAbsentUsed(const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  const vector<Attribute*>::iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == absentAttributeIt)
    {
      // *this necessarily relates to the first symmetric attribute
      return dynamic_cast<AbstractData*>(hyperplanes[(*absentAttributeIt)->getChosenAbsentValueDataId()])->setAbsentAfterAbsentValueMetAndAbsentUsed(nextAttributeIt) + absentFixPresentOrPotentialValuesAfterAbsentUsed(**attributeIt, nextAttributeIt, nextAttributeIt);
    }
  return absentFixPresentOrPotentialValuesBeforeSymmetricAttributesAfterAbsentUsed(**attributeIt, absentAttributeIt, nextAttributeIt);
}

unsigned int Trie::setAbsentAfterAbsentValueMetAndAbsentUsed(const vector<Attribute*>::iterator attributeIt) const
{
  return absentFixPresentOrPotentialValuesAfterAbsentValueMetAndAbsentUsed(**attributeIt, attributeIt + 1);
}

unsigned int Trie::presentFixPresentValues(Attribute& currentAttribute, const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int newNoise = 0;
  const vector<Value*>::iterator end = currentAttribute.presentEnd();
  for (vector<Value*>::iterator valueIt = currentAttribute.presentBegin(); valueIt != end; ++valueIt)
    {
      const unsigned int newNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setPresent(presentAttributeIt, nextAttributeIt);
      (*valueIt)->addPresentNoise(newNoiseInHyperplane);
      newNoise += newNoiseInHyperplane;
    }
  return newNoise;
}

unsigned int Trie::presentFixPresentValuesAfterPresentValueMet(Attribute& currentAttribute, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int newNoise = 0;
  const vector<Value*>::iterator end = currentAttribute.presentEnd();
  for (vector<Value*>::iterator valueIt = currentAttribute.presentBegin(); valueIt != end; ++valueIt)
    {
      const unsigned int newNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setPresentAfterPresentValueMet(nextAttributeIt);
      (*valueIt)->addPresentNoise(newNoiseInHyperplane);
      newNoise += newNoiseInHyperplane;
    }
  return newNoise;
}

void Trie::presentFixPotentialOrAbsentValues(const vector<Value*>::iterator begin, const vector<Value*>::iterator end, const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  for (vector<Value*>::iterator valueIt = begin; valueIt != end; ++valueIt)
    {
      const unsigned int newNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setPresentAfterPotentialOrAbsentUsed(presentAttributeIt, nextAttributeIt);
      (*valueIt)->addPresentNoise(newNoiseInHyperplane);
    }
}

unsigned int Trie::presentFixPresentValuesBeforeSymmetricAttributesAfterPotentialOrAbsentUsed(Attribute& currentAttribute, const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int newNoise = 0;
  const vector<Value*>::iterator end = currentAttribute.presentEnd();
  for (vector<Value*>::iterator valueIt = currentAttribute.presentBegin(); valueIt != end; ++valueIt)
    {
      // Since this is before the symmetric attributes, hyperplanes necessarily are tries
      const unsigned int newNoiseInHyperplane = dynamic_cast<Trie*>(hyperplanes[(*valueIt)->getDataId()])->setSymmetricPresentAfterPotentialOrAbsentUsed(presentAttributeIt, nextAttributeIt);
      newNoise += newNoiseInHyperplane;
    }
  return newNoise;
}

void Trie::presentFixPotentialOrAbsentValuesAfterPresentValueMet(const vector<Value*>::iterator begin, const vector<Value*>::iterator end, const vector<Attribute*>::iterator nextAttributeIt) const
{
  for (vector<Value*>::iterator valueIt = begin; valueIt != end; ++valueIt)
    {
      const unsigned int newNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setPresentAfterPresentValueMetAndPotentialOrAbsentUsed(nextAttributeIt);
      (*valueIt)->addPresentNoise(newNoiseInHyperplane);
    }
}

unsigned int Trie::presentFixPresentValuesAfterPotentialOrAbsentUsed(Attribute& currentAttribute, const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int newNoise = 0;
  const vector<Value*>::iterator end = currentAttribute.presentEnd();
  for (vector<Value*>::iterator valueIt = currentAttribute.presentBegin(); valueIt != end; ++valueIt)
    {
      const unsigned int newNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setPresentAfterPotentialOrAbsentUsed(presentAttributeIt, nextAttributeIt);
      newNoise += newNoiseInHyperplane;
    }
  return newNoise;
}

unsigned int Trie::presentFixPresentValuesAfterPresentValueMetAndPotentialOrAbsentUsed(const Attribute& currentAttribute, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int newNoise = 0;
  const vector<Value*>::const_iterator end = currentAttribute.presentEnd();
  for (vector<Value*>::const_iterator valueIt = currentAttribute.presentBegin(); valueIt != end; ++valueIt)
    {
      const unsigned int newNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setPresentAfterPresentValueMetAndPotentialOrAbsentUsed(nextAttributeIt);
      newNoise += newNoiseInHyperplane;
    }
  return newNoise;
}

void Trie::absentFixAbsentValues(Attribute& currentAttribute, const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  const vector<Value*>::iterator end = currentAttribute.absentEnd();
  for (vector<Value*>::iterator valueIt = currentAttribute.absentBegin(); valueIt != end; ++valueIt)
    {
      const unsigned int oldNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setAbsentAfterAbsentUsed(absentAttributeIt, nextAttributeIt);
      (*valueIt)->subtractPotentialNoise(oldNoiseInHyperplane);
    }
}

unsigned int Trie::absentFixPresentOrPotentialValues(Attribute& currentAttribute, const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int oldNoise = 0;
  vector<Value*>::iterator end = currentAttribute.potentialEnd();
  vector<Value*>::iterator valueIt = currentAttribute.presentBegin();
  for (; valueIt != end; ++valueIt)
    {
      const unsigned int oldNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setAbsent(absentAttributeIt, nextAttributeIt);
      (*valueIt)->subtractPotentialNoise(oldNoiseInHyperplane);
      oldNoise += oldNoiseInHyperplane;
    }
  end = currentAttribute.irrelevantEnd();
  for (; valueIt != end; ++valueIt)
    {
      oldNoise += hyperplanes[(*valueIt)->getDataId()]->setAbsent(absentAttributeIt, nextAttributeIt);
    }
  return oldNoise;
}

void Trie::irrelevantFixAbsentValues(Attribute& currentAttribute, const vector<Attribute*>::iterator irrelevantAttributeIt, const vector<unsigned int>& irrelevantValueDataIds, const vector<Attribute*>::iterator nextAttributeIt) const
{
  const vector<Value*>::iterator end = currentAttribute.absentEnd();
  for (vector<Value*>::iterator valueIt = currentAttribute.absentBegin(); valueIt != end; ++valueIt)
    {
      const unsigned int oldNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setIrrelevantAfterAbsentUsed(irrelevantAttributeIt, irrelevantValueDataIds, nextAttributeIt);
      (*valueIt)->subtractPotentialNoise(oldNoiseInHyperplane);
    }
}

void Trie::absentFixAbsentValuesBeforeSymmetricAttributes(Attribute& currentAttribute, const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  const vector<Value*>::iterator end = currentAttribute.absentEnd();
  for (vector<Value*>::iterator valueIt = currentAttribute.absentBegin(); valueIt != end; ++valueIt)
    {
      // Since this is before the symmetric attributes, hyperplanes necessarily are tries
      const unsigned int oldNoiseInHyperplane = dynamic_cast<Trie*>(hyperplanes[(*valueIt)->getDataId()])->setSymmetricAbsentAfterAbsentUsed(absentAttributeIt, nextAttributeIt);
      (*valueIt)->subtractPotentialNoise(oldNoiseInHyperplane);
    }
}

unsigned int Trie::irrelevantFixPresentOrPotentialValues(Attribute& currentAttribute, const vector<Attribute*>::iterator irrelevantAttributeIt, const vector<unsigned int>& irrelevantValueDataIds, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int oldNoise = 0;
  vector<Value*>::iterator end = currentAttribute.potentialEnd();
  vector<Value*>::iterator valueIt = currentAttribute.presentBegin();
  for (; valueIt != end; ++valueIt)
    {
      const unsigned int oldNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setIrrelevant(irrelevantAttributeIt, irrelevantValueDataIds, nextAttributeIt);
      (*valueIt)->subtractPotentialNoise(oldNoiseInHyperplane);
      oldNoise += oldNoiseInHyperplane;
    }
  end = currentAttribute.irrelevantEnd();
  for (; valueIt != end; ++valueIt)
    {
      oldNoise += hyperplanes[(*valueIt)->getDataId()]->setIrrelevant(irrelevantAttributeIt, irrelevantValueDataIds, nextAttributeIt);
    }
  return oldNoise;
}

unsigned int Trie::absentFixPresentOrPotentialValuesBeforeSymmetricAttributes(Attribute& currentAttribute, const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int oldNoise = 0;
  vector<Value*>::iterator end = currentAttribute.potentialEnd();
  vector<Value*>::iterator valueIt = currentAttribute.presentBegin();
  for (; valueIt != end; ++valueIt)
    {
      // Since this is before the symmetric attributes, hyperplanes necessarily are tries
      const unsigned int oldNoiseInHyperplane = dynamic_cast<Trie*>(hyperplanes[(*valueIt)->getDataId()])->setSymmetricAbsent(absentAttributeIt, nextAttributeIt);
      (*valueIt)->subtractPotentialNoise(oldNoiseInHyperplane);
      /*const unsigned int valueId = (*valueIt)->getPresentAndPotentialIntersectionId();
      for (vector<vector<unsigned int>>::iterator intersectionIt : intersectionIts)
	{
	  (*intersectionIt)[valueId] -= oldNoiseInHyperplane;
	}*/
      oldNoise += oldNoiseInHyperplane;
    }
  end = currentAttribute.irrelevantEnd();
  for (; valueIt != end; ++valueIt)
    {
      oldNoise += dynamic_cast<Trie*>(hyperplanes[(*valueIt)->getDataId()])->setSymmetricAbsent(absentAttributeIt, nextAttributeIt);
    }
  return oldNoise;
}

void Trie::absentFixAbsentValuesAfterAbsentValueMet(const vector<Value*>::iterator absentBegin, const vector<Value*>::iterator absentEnd, const vector<Attribute*>::iterator nextAttributeIt) const
{
  for (vector<Value*>::iterator valueIt = absentBegin; valueIt != absentEnd; ++valueIt)
    {
      const unsigned int oldNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setAbsentAfterAbsentValueMetAndAbsentUsed(nextAttributeIt);
      (*valueIt)->subtractPotentialNoise(oldNoiseInHyperplane);
    }
}

unsigned int Trie::absentFixPresentOrPotentialValuesAfterAbsentValueMet(Attribute& currentAttribute, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int oldNoise = 0;
    vector<Value*>::iterator end = currentAttribute.potentialEnd();
    vector<Value*>::iterator valueIt = currentAttribute.presentBegin();
    for (; valueIt != end; ++valueIt)
      {
        const unsigned int oldNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setAbsentAfterAbsentValueMet(nextAttributeIt);
        (*valueIt)->subtractPotentialNoise(oldNoiseInHyperplane);
        oldNoise += oldNoiseInHyperplane;
      }
    end = currentAttribute.irrelevantEnd();
    for (; valueIt != end; ++valueIt)
      {
        oldNoise += hyperplanes[(*valueIt)->getDataId()]->setAbsentAfterAbsentValueMet(nextAttributeIt);
      }
    return oldNoise;
}

unsigned int Trie::absentFixPresentOrPotentialValuesAfterAbsentUsed(Attribute& currentAttribute, const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int oldNoise = 0;
  vector<Value*>::iterator end = currentAttribute.potentialEnd();
  vector<Value*>::iterator valueIt = currentAttribute.presentBegin();
  for (; valueIt != end; ++valueIt)
    {
      const unsigned int oldNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setAbsentAfterAbsentUsed(absentAttributeIt, nextAttributeIt);
      oldNoise += oldNoiseInHyperplane;
    }
  end = currentAttribute.irrelevantEnd();
  for (; valueIt != end; ++valueIt)
    {
      oldNoise += hyperplanes[(*valueIt)->getDataId()]->setAbsentAfterAbsentUsed(absentAttributeIt, nextAttributeIt);
    }
  return oldNoise;
}

unsigned int Trie::irrelevantFixPresentOrPotentialValuesAfterAbsentUsed(Attribute& currentAttribute, const vector<Attribute*>::iterator irrelevantAttributeIt, const vector<unsigned int>& irrelevantValueDataIds, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int oldNoise = 0;
  vector<Value*>::iterator end = currentAttribute.potentialEnd();
  vector<Value*>::iterator valueIt = currentAttribute.presentBegin();
  for (; valueIt != end; ++valueIt)
    {
      const unsigned int oldNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setIrrelevantAfterAbsentUsed(irrelevantAttributeIt, irrelevantValueDataIds, nextAttributeIt);
      oldNoise += oldNoiseInHyperplane;
    }
  end = currentAttribute.irrelevantEnd();
  for (; valueIt != end; ++valueIt)
    {
      oldNoise += hyperplanes[(*valueIt)->getDataId()]->setIrrelevantAfterAbsentUsed(irrelevantAttributeIt, irrelevantValueDataIds, nextAttributeIt);
    }
  return oldNoise;
}

unsigned int Trie::absentFixPresentOrPotentialValuesBeforeSymmetricAttributesAfterAbsentUsed(Attribute& currentAttribute, const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int oldNoise = 0;
  vector<Value*>::iterator end = currentAttribute.potentialEnd();
  vector<Value*>::iterator valueIt = currentAttribute.presentBegin();
  for (; valueIt != end; ++valueIt)
    {
      // Since this is before the symmetric attributes, hyperplanes necessarily are tries
      const unsigned int oldNoiseInHyperplane = dynamic_cast<Trie*>(hyperplanes[(*valueIt)->getDataId()])->setSymmetricAbsentAfterAbsentUsed(absentAttributeIt, nextAttributeIt);
      //(*absentValueIntersectionIt)[static_cast<ValueNd*>(*valueIt)->getPresentAndPotentialIntersectionId()] -= oldNoiseInHyperplane;
      oldNoise += oldNoiseInHyperplane;
    }
  end = currentAttribute.irrelevantEnd();
  for (; valueIt != end; ++valueIt)
    {
      oldNoise += dynamic_cast<Trie*>(hyperplanes[(*valueIt)->getDataId()])->setSymmetricAbsentAfterAbsentUsed(absentAttributeIt, nextAttributeIt);
    }
  return oldNoise;
}

unsigned int Trie::absentFixPresentOrPotentialValuesAfterAbsentValueMetAndAbsentUsed(Attribute& currentAttribute, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int oldNoise = 0;
  vector<Value*>::iterator end = currentAttribute.potentialEnd();
  vector<Value*>::iterator valueIt = currentAttribute.presentBegin();
  for (; valueIt != end; ++valueIt)
    {
      const unsigned int oldNoiseInHyperplane = hyperplanes[(*valueIt)->getDataId()]->setAbsentAfterAbsentValueMetAndAbsentUsed(nextAttributeIt);
      oldNoise += oldNoiseInHyperplane;
    }
  end = currentAttribute.irrelevantEnd();
  for (; valueIt != end; ++valueIt)
    {
      oldNoise += hyperplanes[(*valueIt)->getDataId()]->setAbsentAfterAbsentValueMetAndAbsentUsed(nextAttributeIt);
    }
  return oldNoise;
}

unsigned int Trie::presentFixPresentValuesBeforeSymmetricAttributes(Attribute& currentAttribute, const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  unsigned int newNoise = 0;
  const vector<Value*>::iterator end = currentAttribute.presentEnd();
  for (vector<Value*>::iterator valueIt = currentAttribute.presentBegin(); valueIt != end; ++valueIt)
    {
      // Since this is before the symmetric attributes, hyperplanes necessarily are tries
      const unsigned int newNoiseInHyperplane = dynamic_cast<Trie*>(hyperplanes[(*valueIt)->getDataId()])->setSymmetricPresent(presentAttributeIt, nextAttributeIt);
      (*valueIt)->addPresentNoise(newNoiseInHyperplane);
      newNoise += newNoiseInHyperplane;
    }
  return newNoise;
}

void Trie::presentFixPotentialOrAbsentValuesBeforeSymmetricAttributes(Attribute& currentAttribute, const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator nextAttributeIt) const
{
  const vector<Value*>::iterator end = currentAttribute.absentEnd();
  for (vector<Value*>::iterator valueIt = currentAttribute.potentialBegin(); valueIt != end; ++valueIt)
    {
      // Since this is before the symmetric attributes, hyperplanes necessarily are tries
      const unsigned int newNoiseInHyperplane = dynamic_cast<Trie*>(hyperplanes[(*valueIt)->getDataId()])->setSymmetricPresentAfterPotentialOrAbsentUsed(presentAttributeIt, nextAttributeIt);
      (*valueIt)->addPresentNoise(newNoiseInHyperplane);
    }
}

void Trie::sortTubes()
{
  for (AbstractData* hyperplane : hyperplanes)
    {
      hyperplane->sortTubes();
    }
}

double Trie::noiseSum(const vector<vector<unsigned int>>& nSet) const
{
  double noise = 0;
  const vector<vector<unsigned int>>::const_iterator dimensionIt = nSet.begin();
  const vector<vector<unsigned int>>::const_iterator nextDimensionIt = dimensionIt + 1;
  for (const unsigned int id : *dimensionIt)
    {
      noise += hyperplanes[id]->noiseSum(nextDimensionIt);
    }
  return noise;
}

unsigned int Trie::noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const
{
  unsigned int noise = 0;
  const vector<vector<unsigned int>>::const_iterator nextDimensionIt = dimensionIt + 1;
  for (const unsigned int id : *dimensionIt)
    {
      noise += hyperplanes[id]->noiseSum(nextDimensionIt);
    }
  return noise;
}

const Tube* Trie::getTube(const unsigned int id) const
{
  return dynamic_cast<Tube*>(hyperplanes[id]); // in 2d, all hyperplanes are tubes
}

#ifdef ASSERT
unsigned int Trie::noiseSumOnPresent(const vector<Attribute*>::const_iterator valueAttributeIt, const Value& value, const vector<Attribute*>::const_iterator attributeIt) const
{
  const vector<Attribute*>::const_iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == valueAttributeIt)
    {
      return hyperplanes[value.getDataId()]->noiseSumOnPresent(valueAttributeIt, value, nextAttributeIt);
    }
  unsigned int noise = 0;
  const vector<Value*>::const_iterator end = (*attributeIt)->presentEnd();
  for (vector<Value*>::const_iterator valueIt = (*attributeIt)->presentBegin(); valueIt != end; ++valueIt)
    {
      noise += hyperplanes[(*valueIt)->getDataId()]->noiseSumOnPresent(valueAttributeIt, value, nextAttributeIt);
    }
  return noise;
}

unsigned int Trie::noiseSumOnPresentAndPotential(const vector<Attribute*>::const_iterator valueAttributeIt, const Value& value, const vector<Attribute*>::const_iterator attributeIt) const
{
  const vector<Attribute*>::const_iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == valueAttributeIt)
    {
      return hyperplanes[value.getDataId()]->noiseSumOnPresentAndPotential(valueAttributeIt, value, nextAttributeIt);
    }
  unsigned int noise = 0;
  const vector<Value*>::const_iterator end = (*attributeIt)->irrelevantEnd();
  for (vector<Value*>::const_iterator valueIt = (*attributeIt)->presentBegin(); valueIt != end; ++valueIt)
    {
      noise += hyperplanes[(*valueIt)->getDataId()]->noiseSumOnPresentAndPotential(valueAttributeIt, value, nextAttributeIt);
    }
  return noise;
}

unsigned int Trie::noiseSumOnPresent(const vector<Attribute*>::const_iterator firstValueAttributeIt, const Value& firstValue, const vector<Attribute*>::const_iterator secondValueAttributeIt, const Value& secondValue, const vector<Attribute*>::const_iterator attributeIt) const
{
  const vector<Attribute*>::const_iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == firstValueAttributeIt)
    {
      return hyperplanes[firstValue.getDataId()]->noiseSumOnPresent(secondValueAttributeIt, secondValue, nextAttributeIt);
    }
  unsigned int noise = 0;
  const vector<Value*>::const_iterator end = (*attributeIt)->presentEnd();
  for (vector<Value*>::const_iterator valueIt = (*attributeIt)->presentBegin(); valueIt != end; ++valueIt)
    {
      noise += dynamic_cast<Trie*>(hyperplanes[(*valueIt)->getDataId()])->noiseSumOnPresent(firstValueAttributeIt, firstValue, secondValueAttributeIt, secondValue, nextAttributeIt);
    }
  return noise;
}

unsigned int Trie::noiseSumOnPresentAndPotential(const vector<Attribute*>::const_iterator firstValueAttributeIt, const Value& firstValue, const vector<Attribute*>::const_iterator secondValueAttributeIt, const Value& secondValue, const vector<Attribute*>::const_iterator attributeIt) const
{
  const vector<Attribute*>::const_iterator nextAttributeIt = attributeIt + 1;
  if (attributeIt == firstValueAttributeIt)
    {
      return hyperplanes[firstValue.getDataId()]->noiseSumOnPresentAndPotential(secondValueAttributeIt, secondValue, nextAttributeIt);
    }
  unsigned int noise = 0;
  const vector<Value*>::const_iterator end = (*attributeIt)->irrelevantEnd();
  for (vector<Value*>::const_iterator valueIt = (*attributeIt)->presentBegin(); valueIt != end; ++valueIt)
    {
      noise += dynamic_cast<Trie*>(hyperplanes[(*valueIt)->getDataId()])->noiseSumOnPresentAndPotential(firstValueAttributeIt, firstValue, secondValueAttributeIt, secondValue, nextAttributeIt);
    }
  return noise;
}
#endif

vector<vector<vector<unsigned int>>::iterator> Trie::incrementIterators(const vector<vector<vector<unsigned int>>::iterator>& iterators)
{
  vector<vector<vector<unsigned int>>::iterator> nextIterators;
  nextIterators.reserve(iterators.size());
  for (const vector<vector<unsigned int>>::iterator it : iterators)
    {
      nextIterators.push_back(it + 1);
    }
  return nextIterators;
}
