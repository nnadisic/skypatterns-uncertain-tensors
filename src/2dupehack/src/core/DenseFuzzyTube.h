// Copyright 2010,2011,2012,2013,2014,2015,2016,2017 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of multidupehack.

// multidupehack is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// multidupehack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with multidupehack; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef DENSE_FUZZY_TUBE_H_
#define DENSE_FUZZY_TUBE_H_

#include "Tube.h"

class DenseFuzzyTube : public Tube
{
 public:
  DenseFuzzyTube(const vector<pair<unsigned int, unsigned int>>& sparseTube);
  DenseFuzzyTube(const unsigned int presentElement);

  void print(vector<unsigned int>& prefix, ostream& out) const;
  bool setTuple(const vector<unsigned int>& tuple, const unsigned int membership, vector<unsigned int>::const_iterator attributeIdIt, vector<vector<unsigned int>>::const_iterator oldIds2NewIdsIt, const vector<Attribute*>::iterator attributeIt);

  unsigned int noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const;

  unsigned int setPresent(const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator attributeIt) const;
  unsigned int setPresentAfterPotentialOrAbsentUsed(const vector<Attribute*>::iterator presentAttributeIt, const vector<Attribute*>::iterator attributeIt) const;

#ifdef ASSERT
  unsigned int noiseSumOnPresent(const vector<Attribute*>::const_iterator valueAttributeIt, const Value& value, const vector<Attribute*>::const_iterator attributeIt) const;
  unsigned int noiseSumOnPresentAndPotential(const vector<Attribute*>::const_iterator valueAttributeIt, const Value& value, const vector<Attribute*>::const_iterator attributeIt) const;
#endif

  unsigned int noiseOnValue(const unsigned int valueDataId) const;

 protected:
  vector<unsigned int> tube;

  DenseFuzzyTube* clone() const;

  unsigned int noiseOnValues(const vector<unsigned int>& valueDataIds) const;
  unsigned int presentFixPresentValuesAfterPresentValueMet(Attribute& currentAttribute) const;
  void presentFixPotentialOrAbsentValuesAfterPresentValueMet(Attribute& currentAttribute) const;
  unsigned int presentFixPresentValuesAfterPresentValueMetAndPotentialOrAbsentUsed(const Attribute& currentAttribute) const;

  unsigned int absentFixPresentOrPotentialValuesAfterAbsentValueMet(Attribute& currentAttribute) const;
  unsigned int irrelevantFixPresentOrPotentialValuesAfterIrrelevantValueMet(Attribute& currentAttribute) const;
  unsigned int absentFixPresentOrPotentialValuesAfterAbsentValueMetAndAbsentUsed(Attribute& currentAttribute) const;
  void absentFixAbsentValuesAfterAbsentValueMet(const vector<Value*>::iterator begin, const vector<Value*>::iterator end) const;

};

#endif /*DENSE_FUZZY_TUBE_H_*/
