// Copyright 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of multidupehack.

// multidupehack is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// multidupehack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with multidupehack; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "Attribute.h"

unsigned int Attribute::noisePerUnit;

#if defined DEBUG || defined VERBOSE_ELEMENT_CHOICE || defined ASSERT
vector<unsigned int> Attribute::internal2ExternalAttributeOrder;
#endif

unsigned int Attribute::maxId = 0;
unsigned int Attribute::orderedAttributeId;
vector<unsigned int> Attribute::epsilonVector;
vector<bool> Attribute::isClosedVector;
vector<vector<string>> Attribute::labelsVector;
vector<unsigned int> Attribute::minSizes;
unsigned int Attribute::minArea;

string Attribute::outputElementSeparator;
string Attribute::emptySetString;
string Attribute::elementNoiseSeparator;
bool Attribute::isNoisePrinted;

Attribute::Attribute(): id(maxId++), values(), potentialIndex(0), irrelevantIndex(0), absentIndex(0), presentAndPotentialIrrelevancyThreshold(0)
{
}

Attribute::Attribute(const vector<unsigned int>& nbOfValuesPerAttribute, const double epsilon, const unsigned int minSize, const vector<string>& labels): Attribute()
{
  const vector<unsigned int>::const_iterator nbOfValuesInThisAttributeIt = nbOfValuesPerAttribute.begin() + id;
  irrelevantIndex = *nbOfValuesInThisAttributeIt;
  absentIndex = irrelevantIndex;
  unsigned int sizeOfAValue = noisePerUnit;
  for (vector<unsigned int>::const_iterator nbOfValuesInAttributeIt = nbOfValuesPerAttribute.begin(); nbOfValuesInAttributeIt != nbOfValuesPerAttribute.end(); ++nbOfValuesInAttributeIt)
    {
      if (nbOfValuesInAttributeIt != nbOfValuesInThisAttributeIt)
	{
	  sizeOfAValue *= *nbOfValuesInAttributeIt;
	}
    }
  values.reserve(irrelevantIndex);
  for (unsigned int valueId = 0; valueId != *nbOfValuesInThisAttributeIt; ++valueId)
    {
      values.push_back(new Value(valueId, sizeOfAValue));
    }
  epsilonVector.push_back(epsilon * noisePerUnit);
  minSizes.push_back(minSize);
  labelsVector.push_back(labels);
}

Attribute::Attribute(const Attribute& parentAttribute): id(parentAttribute.id), values(), potentialIndex(parentAttribute.potentialIndex), irrelevantIndex(parentAttribute.irrelevantIndex), absentIndex(parentAttribute.absentIndex), presentAndPotentialIrrelevancyThreshold(parentAttribute.presentAndPotentialIrrelevancyThreshold)
{
}

Attribute::~Attribute()
{
  for (const Value* value : values)
    {
      delete value;
    }
}

Attribute* Attribute::clone() const
{
  return new Attribute(*this);
}

ostream& operator<<(ostream& out, const Attribute& attribute)
{
  attribute.printValues(attribute.values.begin(), attribute.values.begin() + attribute.irrelevantIndex, out);
  return out;
}

void Attribute::subtractSelfLoopsFromPotentialNoise(const unsigned int totalMembershipDueToSelfLoopsOnASymmetricValue, const unsigned int nbOfSymmetricElements)
{
  const unsigned int totalMembershipDueToSelfLoopsOnValue = totalMembershipDueToSelfLoopsOnASymmetricValue / irrelevantIndex * nbOfSymmetricElements;
  for (Value* value : values)
    {
      value->subtractPotentialNoise(totalMembershipDueToSelfLoopsOnValue);
    }
}

void Attribute::setPresentIntersections(const vector<Attribute*>::const_iterator parentAttributeIt, const vector<Attribute*>::const_iterator parentAttributeEnd, const unsigned int presentAttributeId)
{
  values.reserve((*parentAttributeIt)->values.size());
  vector<Value*>::const_iterator parentValueIt = (*parentAttributeIt)->values.begin();
  vector<Value*>::const_iterator end = parentValueIt + potentialIndex;
  for (; parentValueIt != end; ++parentValueIt)
    {
      values.push_back(new Value(**parentValueIt));
    }
  if (isEnumeratedAttribute(presentAttributeId))
    {
      values.push_back(new Value(**parentValueIt++));
      ++potentialIndex;
    }
  end = (*parentAttributeIt)->values.end();
  for (; parentValueIt != end; ++parentValueIt)
    {
      values.push_back(new Value(**parentValueIt));
    }
  if (isEnumeratedAttribute(presentAttributeId))
    {
      --potentialIndex;
    }
}

unsigned int Attribute::getId() const
{
  return id;
}

bool Attribute::closedAttribute() const
{
  return isClosedVector[id];
}

bool Attribute::symmetric() const
{
  return false; // TODO
}

Attribute* Attribute::thisOrFirstSymmetricAttribute()
{
  return this;
}

bool Attribute::isEnumeratedAttribute(const unsigned int enumeratedAttributeId) const
{
  return enumeratedAttributeId == id;
}

vector<unsigned int> Attribute::getPresentAndPotentialDataIds() const
{
  vector<unsigned int> dataIds;
  dataIds.reserve(irrelevantIndex);
  const vector<Value*>::const_iterator end = values.begin() + irrelevantIndex;
  for (vector<Value*>::const_iterator valueIt = values.begin(); valueIt != end; ++valueIt)
    {
      dataIds.push_back((*valueIt)->getDataId());
    }
  return dataIds;
}

vector<unsigned int> Attribute::getIrrelevantDataIds() const
{
  vector<unsigned int> dataIds;
  dataIds.reserve(absentIndex - irrelevantIndex);
  const vector<Value*>::const_iterator end = values.begin() + absentIndex;
  for (vector<Value*>::const_iterator valueIt = values.begin() + irrelevantIndex; valueIt != end; ++valueIt)
    {
      dataIds.push_back((*valueIt)->getDataId());
    }
  return dataIds;
}

vector<Value*>::const_iterator Attribute::presentBegin() const
{
  return values.begin();
}

vector<Value*>::const_iterator Attribute::presentEnd() const
{
  return values.begin() + potentialIndex;
}

vector<Value*>::const_iterator Attribute::potentialBegin() const
{
  return values.begin() + potentialIndex;
}

vector<Value*>::const_iterator Attribute::potentialEnd() const
{
  return values.begin() + irrelevantIndex;
}

vector<Value*>::const_iterator Attribute::irrelevantBegin() const
{
  return values.begin() + irrelevantIndex;
}

vector<Value*>::const_iterator Attribute::irrelevantEnd() const
{
  return values.begin() + absentIndex;
}

vector<Value*>::const_iterator Attribute::absentBegin() const
{
  return values.begin() + absentIndex;
}

vector<Value*>::const_iterator Attribute::absentEnd() const
{
  return values.end();
}

vector<Value*>::iterator Attribute::presentBegin()
{
  return values.begin();
}

vector<Value*>::iterator Attribute::presentEnd()
{
  return values.begin() + potentialIndex;
}

vector<Value*>::iterator Attribute::potentialBegin()
{
  return values.begin() + potentialIndex;
}

vector<Value*>::iterator Attribute::potentialEnd()
{
  return values.begin() + irrelevantIndex;
}

vector<Value*>::iterator Attribute::irrelevantBegin()
{
  return values.begin() + irrelevantIndex;
}

vector<Value*>::iterator Attribute::irrelevantEnd()
{
  return values.begin() + absentIndex;
}

vector<Value*>::iterator Attribute::absentBegin()
{
  return values.begin() + absentIndex;
}

vector<Value*>::iterator Attribute::absentEnd()
{
  return values.end();
}

unsigned int Attribute::sizeOfPresent() const
{
  return potentialIndex;
}

unsigned int Attribute::sizeOfPotential() const
{
  return irrelevantIndex - potentialIndex;
}

unsigned int Attribute::sizeOfPresentAndPotential() const
{
  return absentIndex;
}

bool Attribute::irrelevantEmpty() const
{
  return irrelevantIndex == absentIndex;
}

unsigned int Attribute::globalSize() const
{
  return values.size();
}

double Attribute::totalPresentAndPotentialNoise() const
{
  double totalNoise = 0;
  const vector<Value*>::const_iterator end = values.begin() + irrelevantIndex;
  for (vector<Value*>::const_iterator valueIt = values.begin(); valueIt != end; ++valueIt)
    {
      totalNoise += (*valueIt)->getPresentAndPotentialNoise();
    }
  return totalNoise;
}

double Attribute::averagePresentAndPotentialNoise() const
{
  return totalPresentAndPotentialNoise() / values.size();
}

void Attribute::sortPotentialAndAbsentButChosenPresentValueIfNecessary(const unsigned int presentAttributeId)
{
  if (id == orderedAttributeId && id != presentAttributeId)
    {
      sortPotentialAndAbsent(potentialIndex);
    }
}

void Attribute::sortPotentialIrrelevantAndAbsentButChosenAbsentValueIfNecessary(const unsigned int absentAttributeId)
{
  sortPotentialIrrelevantAndAbsentIfNecessary(absentAttributeId);
}

void Attribute::sortPotentialIrrelevantAndAbsentIfNecessary(const unsigned int absentAttributeId)
{
  if (id == orderedAttributeId && id != absentAttributeId)
    {
      sortPotentialIrrelevantAndAbsent(absentIndex);
    }
}

void Attribute::sortPotentialAndAbsent(const unsigned int realPotentialIndex)
{
  const vector<Value*>::iterator absentBegin = values.begin() + irrelevantIndex;
  sort(values.begin() + realPotentialIndex, absentBegin, Value::smallerDataId);
  sort(absentBegin, values.end(), Value::smallerDataId);
}

void Attribute::sortPotentialIrrelevantAndAbsent(const unsigned int realAbsentIndex)
{
  const vector<Value*>::iterator irrelevantBegin = values.begin() + irrelevantIndex;
  sort(values.begin() + potentialIndex, irrelevantBegin, Value::smallerDataId);
  sort(irrelevantBegin, values.begin() + absentIndex, Value::smallerDataId);
  sort(values.begin() + realAbsentIndex, values.end(), Value::smallerDataId);
}

unsigned int Attribute::getChosenValueDataId() const
{
  return values[potentialIndex]->getDataId();
}

Value& Attribute::getChosenValue() const
{
  return *values[potentialIndex];
}

void Attribute::repositionChosenPresentValue()
{
  if (id == orderedAttributeId)
    {
      repositionChosenPresentValueInOrderedAttribute();
    }
}

void Attribute::setChosenValuePresent()
{
  ++potentialIndex;
}

void Attribute::repositionChosenPresentValueInOrderedAttribute()
{
  const vector<Value*>::iterator valuesBegin = values.begin();
  vector<Value*>::iterator valueIt = valuesBegin + potentialIndex - 1;
  Value* chosenValue = *valueIt;
  const unsigned int chosenValueDataId = chosenValue->getDataId();
  if ((*valuesBegin)->getDataId() < chosenValueDataId)
    {
      for (vector<Value*>::iterator nextValueIt = valueIt - 1; (*nextValueIt)->getDataId() > chosenValueDataId; --nextValueIt)
	{
	  *valueIt = *nextValueIt;
	  --valueIt;
	}
      *valueIt = chosenValue;
      return;
    }
  rotate(valuesBegin, valueIt, valueIt + 1);
}

vector<unsigned int> Attribute::setChosenValueIrrelevant(const vector<Attribute*>::const_iterator attributeBegin, const vector<Attribute*>::const_iterator attributeEnd)
{
  vector<unsigned int> irrelevantValueDataIds = setChosenValueIrrelevant();
  if (irrelevantValueDataIds.empty())
    {
      return irrelevantValueDataIds;
    }
#ifdef MIN_SIZE_ELEMENT_PRUNING
  if (minArea == 0)
    {
      return irrelevantValueDataIds;
    }
  bool isAbsentToBeCleaned = false;
  while (possiblePresentAndPotentialIrrelevancy(attributeBegin, attributeEnd))
    {
      if (presentAndPotentialIrrelevant())
	{
          return vector<unsigned int>();
        }
      isAbsentToBeCleaned = true;
      const vector<unsigned int> newIrrelevantValueDataIds = findPresentAndPotentialIrrelevantValuesAndCheckTauContiguity().second;
      if (newIrrelevantValueDataIds.empty())
	{
          break;		// presentAndPotentialIrrelevancyThreshold will not change
        }
      irrelevantValueDataIds.insert(irrelevantValueDataIds.end(), newIrrelevantValueDataIds.begin(), newIrrelevantValueDataIds.end());
    }
  if (isAbsentToBeCleaned)
    {
      presentAndPotentialCleanAbsent();
    }
#endif
  return irrelevantValueDataIds;
}

vector<unsigned int> Attribute::setChosenValueIrrelevant()
{
  Value*& chosenAbsentValue = values[potentialIndex];
  const unsigned int chosenAbsentValueDataId = chosenAbsentValue->getDataId();
  swap(chosenAbsentValue, values[--irrelevantIndex]);
  return {chosenAbsentValueDataId};
}

unsigned int Attribute::getChosenAbsentValueDataId() const
{
  return values[absentIndex]->getDataId();
}

Value& Attribute::getChosenAbsentValue() const
{
  return *values[absentIndex];
}

void Attribute::setLastIrrelevantValueChosen(const vector<Attribute*>::const_iterator thisAttributeIt, const vector<Attribute*>::const_iterator attributeEnd)
{
  --absentIndex;
}

// It should be called after initialization only (values is ordered)
void Attribute::decrementPotentialNoise(const unsigned int valueId)
{
  values[valueId]->decrementPotentialNoise();
}

// It should be called after initialization only (values is ordered)
void Attribute::subtractPotentialNoise(const unsigned int valueId, const unsigned int noise)
{
  values[valueId]->subtractPotentialNoise(noise);
}

void Attribute::printValues(const vector<Value*>::const_iterator begin, const vector<Value*>::const_iterator end, ostream& out) const
{
  bool isFirstElement = true;
  vector<string>& labels = labelsVector[id];
  for (vector<Value*>::const_iterator valueIt = begin; valueIt != end; ++valueIt)
    {
      if (isFirstElement)
	{
	  isFirstElement = false;
	}
      else
	{
	  out << outputElementSeparator;
	}
      out << labels[(*valueIt)->getDataId()];
      if (isNoisePrinted)
	{
	  out << elementNoiseSeparator << static_cast<float>((*valueIt)->getPresentAndPotentialNoise()) / noisePerUnit;
	}
    }
  if (isFirstElement)
    {
      out << emptySetString;
    }
}

void Attribute::printValueFromDataId(const unsigned int valueDataId, ostream& out) const
{
  out << labelsVector[id][valueDataId];
}

#ifdef DEBUG
void Attribute::printPresent(ostream& out) const
{
  printValues(values.begin(), values.begin() + potentialIndex, out);
}

void Attribute::printPotential(ostream& out) const
{
  printValues(values.begin() + potentialIndex, values.begin() + irrelevantIndex, out);
}

void Attribute::printAbsent(ostream& out) const
{
  printValues(values.begin() + absentIndex, values.end(), out);
}

void Attribute::printValues(ostream& out) const
{
  printValues(values.begin(), values.end(), out);
}

void Attribute::printChosenValue(ostream& out) const
{
  out << labelsVector[id][values[potentialIndex]->getDataId()] << " in attribute " << internal2ExternalAttributeOrder[id];
}
#endif

#ifdef ASSERT
void Attribute::printValue(const Value& value, ostream& out) const
{
  out << labelsVector[id][value.getDataId()] << " in attribute " << internal2ExternalAttributeOrder[id];
}
#endif

#if defined DEBUG || defined VERBOSE_ELEMENT_CHOICE || defined ASSERT
void Attribute::setInternal2ExternalAttributeOrder(const vector<unsigned int>& internal2ExternalAttributeOrderParam)
{
  internal2ExternalAttributeOrder = internal2ExternalAttributeOrderParam;
}
#endif

Attribute* Attribute::chooseValue(const vector<Attribute*>& attributes)
{
  const vector<Attribute*>::const_iterator attributeEnd = attributes.end();
  const vector<Attribute*>::const_iterator attributeBegin = attributes.begin();
  vector<Attribute*>::const_iterator attributeIt = attributeBegin;
#ifdef TWO_MODE_ELEMENT_CHOICE
  bool isAnAttributeSymmetric = false;
  for (vector<unsigned int>::const_iterator epsilonIt = epsilonVector.begin(); attributeIt != attributeEnd; ++attributeIt)
    {
      isAnAttributeSymmetric = isAnAttributeSymmetric || (*attributeIt)->symmetric();
      unsigned int presentArea = noisePerUnit;
      if ((*attributeIt)->potentialIndex == 0)
	{
	  vector<Attribute*>::const_iterator otherAttributeIt = attributeBegin;
	  for (; otherAttributeIt != attributeIt; ++otherAttributeIt)
	    {
	      presentArea *= (*otherAttributeIt)->potentialIndex;
	    }
	  while (++otherAttributeIt != attributeEnd)
	    {
	      presentArea *= (*otherAttributeIt)->potentialIndex;
	    }
	}
      else
	{
	  unsigned int smallestOtherPresentSize = numeric_limits<unsigned int>::max();
	  for (vector<Attribute*>::const_iterator otherAttributeIt = attributeBegin; otherAttributeIt != attributeEnd; ++otherAttributeIt)
	    {
	      if (otherAttributeIt != attributeIt)
		{
		  if ((*otherAttributeIt)->potentialIndex < smallestOtherPresentSize && (*otherAttributeIt)->potentialIndex != (*otherAttributeIt)->irrelevantIndex)
		    {
		      if (smallestOtherPresentSize != numeric_limits<unsigned int>::max())
			{
			  presentArea *= smallestOtherPresentSize;
			}
		      smallestOtherPresentSize = (*otherAttributeIt)->potentialIndex;
		    }
		  else
		    {
		      presentArea *= (*otherAttributeIt)->potentialIndex;
		    }
		}
	    }
	  if (++smallestOtherPresentSize == 0)
	    {
	      // only *attributeIt has potential elements
	      break;
	    }
	  presentArea *= smallestOtherPresentSize;
	}
      if (presentArea > *epsilonIt++)
	{
	  break;
	}
    }
  if (attributeIt == attributeEnd)
    {
#ifdef VERBOSE_ELEMENT_CHOICE
      cout << "No value set present allows to reach an epsilon" << endl;
#endif
      for (attributeIt = attributeBegin; (*attributeIt)->potentialIndex == (*attributeIt)->irrelevantIndex; ++attributeIt)
	{
        }
      Attribute* bestAttribute = *attributeIt; // not nullptr because it may be impossible to exceed any epsilon and yet chooseValue is called if some attribute with potential values is unclosed
      double bestCost = numeric_limits<double>::infinity();
      vector<Attribute*> orderedAttributes = attributes;
      sort(orderedAttributes.begin(), orderedAttributes.end());
      const vector<Attribute*>::iterator orderedAttributeEnd = orderedAttributes.end();
      for (vector<Attribute*>::iterator orderedAttributeIt = orderedAttributes.begin(); orderedAttributeIt != orderedAttributeEnd; ++orderedAttributeIt)
  	{
  	  Attribute* currentAttribute = *orderedAttributeIt;
#ifdef VERBOSE_ELEMENT_CHOICE
  	  cout << "Multiplicative partitions of attribute " << internal2ExternalAttributeOrder[currentAttribute->id] << "'s floor(epsilon) + 1: " << epsilonVector[currentAttribute->id] / noisePerUnit + 1 << endl;
#endif
  	  vector<Attribute*>::const_iterator nextAttributeIt = orderedAttributes.erase(orderedAttributeIt);
  	  vector<unsigned int> factorization;
  	  factorization.reserve(maxId - 1);
	  currentAttribute->multiplicativePartition(epsilonVector[currentAttribute->id] / noisePerUnit + 1, maxId - 1, 1, isAnAttributeSymmetric, orderedAttributes, factorization, bestCost, bestAttribute);
  	  orderedAttributeIt = orderedAttributes.insert(nextAttributeIt, currentAttribute);
  	}
      bestAttribute = bestAttribute->thisOrFirstSymmetricAttribute();
      double appeal = 0;
      bestAttribute->chooseValue(bestAttribute->getIndexOfValueToChoose(attributeBegin, attributeEnd, appeal));
      return bestAttribute;
    }
#endif
  for (attributeIt = attributeBegin; (*attributeIt)->potentialIndex == (*attributeIt)->irrelevantIndex; ++attributeIt)
    {
    }
  Attribute* bestAttribute = *attributeIt;
  double bestAppeal = -1;
  unsigned int indexOfValueToChoose = bestAttribute->getIndexOfValueToChoose(attributeBegin, attributeEnd, bestAppeal);
  while (++attributeIt != attributeEnd)
    {
      if ((*attributeIt)->potentialIndex != (*attributeIt)->irrelevantIndex)
	{
	  double appeal = bestAppeal;
	  const unsigned int indexOfValueToChooseInAttribute = (*attributeIt)->getIndexOfValueToChoose(attributeBegin, attributeEnd, appeal);
	  if (appeal > bestAppeal)
	    {
	      bestAppeal = appeal;
	      indexOfValueToChoose = indexOfValueToChooseInAttribute;
	      bestAttribute = *attributeIt;
	    }
	}
    }
  bestAttribute = bestAttribute->thisOrFirstSymmetricAttribute();
  bestAttribute->chooseValue(indexOfValueToChoose);
  return bestAttribute;
}

#ifdef TWO_MODE_ELEMENT_CHOICE
void Attribute::findBetterEnumerationStrategy(const vector<Attribute*>& attributes, const vector<unsigned int>& factorization, const bool isAnElementTakenInThis, double& bestCost, Attribute*& bestAttribute) const
{
  Attribute* bestAttributeForCurrentPermutation = const_cast<Attribute*>(this);
  double cost;
  if (isAnElementTakenInThis)
    {
      cost = irrelevantIndex - potentialIndex;
    }
  else
    {
      cost = 1;
    }
  bool isNotFirstSymmetric = false;
  vector<unsigned int>::const_reverse_iterator factorIt = factorization.rbegin(); // reverse to take the greatest factor among those for symmetric attributes
  for (Attribute* attribute : attributes)
    {
      if (attribute->irrelevantIndex < *factorIt)
	{
	  cost = numeric_limits<double>::infinity();
	  break;
	}
      if (attribute->symmetric())
	{
	  if (isNotFirstSymmetric)
	    {
	      ++factorIt;
	      continue;
	    }
	  isNotFirstSymmetric = true;
	}
      cost *= math::binomial_coefficient<double>(attribute->irrelevantIndex, *factorIt);
      if (attribute->potentialIndex < *factorIt && bestAttributeForCurrentPermutation == this)
	{
	  bestAttributeForCurrentPermutation = attribute;
	}
      ++factorIt;
    }
  if (cost < bestCost)
    {
      bestCost = cost;
      bestAttribute = bestAttributeForCurrentPermutation;
    }
}

void Attribute::multiplicativePartition(const double number, const unsigned int nbOfFactors, const unsigned int begin, const bool isAnAttributeSymmetric, vector<Attribute*>& attributes, vector<unsigned int>& factorization, double& bestCost, Attribute*& bestAttribute) const
{
  if (nbOfFactors == 1)
    {
      const unsigned int factor = ceil(number);
      if (factor >= begin)
	{
	  factorization.push_back(factor);
#ifdef VERBOSE_ELEMENT_CHOICE
	  cout << ' ';
	  for (const unsigned int f : factorization)
	    {
	      cout << ' ' << f;
	    }
	  cout << endl;
#endif
	  if (isAnAttributeSymmetric)
	    {
	      // the whole factorization is at one element of exceeding epsilon because of the self loops with no noise
	      do
		{
		  findBetterEnumerationStrategy(attributes, factorization, false, bestCost, bestAttribute);
		} while (next_permutation(attributes.begin(), attributes.end()));
	      factorization.pop_back();
	      return;
	    }
	  do
	    {
	      // one element to remove to be at one element of exceeding epsilon
	      unsigned int previousFactor = 0;
	      for (unsigned int& f : factorization)
	      	{
	      	  if (f == previousFactor)
	      	    {
	      	      continue;
	      	    }
	      	  --f;
	      	  findBetterEnumerationStrategy(attributes, factorization, potentialIndex == 0, bestCost, bestAttribute);
	      	  previousFactor = ++f;
	      	}
	      findBetterEnumerationStrategy(attributes, factorization, false, bestCost, bestAttribute);
	    } while (next_permutation(attributes.begin(), attributes.end()));
	  factorization.pop_back();
	}
      return;
    }
  const unsigned int end = ceil(pow(number, 1. / nbOfFactors)) + 1;
  for (unsigned int factor = begin; factor != end; ++factor)
    {
      factorization.push_back(factor);
      multiplicativePartition(number / factor, nbOfFactors - 1, factor, isAnAttributeSymmetric, attributes, factorization, bestCost, bestAttribute);
      factorization.pop_back();
    }
}
#endif

unsigned int Attribute::getIndexOfValueToChoose(const double presentCoeff, const double presentAndPotentialCoeff, const double bestPossibleAppealWithThis, double& bestAppeal) const
{
  if (bestPossibleAppealWithThis > bestAppeal)
    {
      double appealOfValueToChoose = 0;
      const vector<Value*>::const_iterator end = values.begin() + irrelevantIndex;
      vector<Value*>::const_iterator valueToChooseIt = values.begin() + potentialIndex;
      for (vector<Value*>::const_iterator potentialValueIt = valueToChooseIt; potentialValueIt != end; ++potentialValueIt)
	{
	  const double appealOfValue = presentCoeff * (*potentialValueIt)->getPresentNoise() + presentAndPotentialCoeff * (*potentialValueIt)->getPresentAndPotentialNoise();
	  if (appealOfValue > appealOfValueToChoose)
	    {
	      appealOfValueToChoose = appealOfValue;
	      valueToChooseIt = potentialValueIt;
	    }
	}
#if defined VERBOSE_ELEMENT_CHOICE && ENUMERATION_PROCESS == 1
      cout << "Appeal of attribute " << internal2ExternalAttributeOrder[id] << ": " << appealOfValueToChoose / noisePerUnit << " with element " << labelsVector[id][(*valueToChooseIt)->getDataId()] << endl;
#endif
      if (appealOfValueToChoose > bestAppeal)
	{
	  bestAppeal = appealOfValueToChoose;
	  return valueToChooseIt - values.begin();
	}
    }
#if defined VERBOSE_ELEMENT_CHOICE && ENUMERATION_PROCESS == 1
  else
    {
      cout << "Appeal of attribute " << internal2ExternalAttributeOrder[id] << " cannot be higher than " << bestAppeal / noisePerUnit << endl;
    }
#endif
  return potentialIndex;
}

unsigned int Attribute::getIndexOfValueToChoose(const vector<Attribute*>::const_iterator attributeBegin, const vector<Attribute*>::const_iterator attributeEnd, double& bestAppeal) const
{
  unsigned int presentArea = 1;
  unsigned int presentAndPotentialArea = 1;
  unsigned int testedArea = 0;
  for (vector<Attribute*>::const_iterator potentialAttributeIt = attributeBegin; potentialAttributeIt != attributeEnd; ++potentialAttributeIt)
    {
      if (*potentialAttributeIt != this)
      	{
	  presentArea *= (*potentialAttributeIt)->potentialIndex;
	  presentAndPotentialArea *= (*potentialAttributeIt)->irrelevantIndex;
	  unsigned int yesFactor = (*potentialAttributeIt)->irrelevantIndex - (*potentialAttributeIt)->potentialIndex;
      	  for (vector<Attribute*>::const_iterator presentAttributeIt = attributeBegin; presentAttributeIt != attributeEnd; ++presentAttributeIt)
      	    {
      	      if (*presentAttributeIt != this && presentAttributeIt != potentialAttributeIt)
      	  	{
      	  	  yesFactor *= (*presentAttributeIt)->potentialIndex;
      	  	}
      	    }
	  testedArea += yesFactor;
	}
    }
  if (testedArea == 0)
    {
#if ENUMERATION_PROCESS == 1
      if (presentArea == 0)
	{
#endif
#ifdef TWO_MODE_ELEMENT_CHOICE
	  return potentialIndex;
#endif
	  if (potentialIndex == 0)
	    {
#ifdef VERBOSE_ELEMENT_CHOICE
	      cout << "Appeal of attribute " << internal2ExternalAttributeOrder[id] << ": 0" << endl;
#endif
	      if (bestAppeal > 0)
		{
	           return 0;
	        }
	      bestAppeal = 0;
	      double appeal = 0;
	      return getIndexOfValueToChoose(1, 0, 1, appeal);
	    }
#ifdef VERBOSE_ELEMENT_CHOICE
	  cout << "Appeal of attribute " << internal2ExternalAttributeOrder[id] << ": -1" << endl;
#endif
	  if (bestAppeal == -1)
	    {
	      double appeal = 0;
	      return getIndexOfValueToChoose(1, 0, 1, appeal);
	    }
	  return 0;
#if ENUMERATION_PROCESS == 1
	}
      // only this has potential elements
      bestAppeal = 1;
      double appeal = 0;
      return getIndexOfValueToChoose(1, 0, 1, appeal);
#endif
    }
#if ENUMERATION_PROCESS == 0
#ifdef VERBOSE_ELEMENT_CHOICE
  cout << "Appeal of attribute " << internal2ExternalAttributeOrder[id] << ": " << testedArea << endl;
#endif
  if (testedArea > bestAppeal)
    {
      bestAppeal = testedArea;
      double appeal = 0;
      return getIndexOfValueToChoose(1, 0, 1, appeal);
    }
#else
  const double presentAndPotentialCoeff = static_cast<double>(testedArea) / (presentAndPotentialArea - presentArea);
  double appeal = bestAppeal;
  const unsigned int indexOfBestValueInThis = getIndexOfValueToChoose(-presentAndPotentialCoeff + maxId - 1, presentAndPotentialCoeff, static_cast<double>(testedArea + (maxId - 1) * presentArea) * noisePerUnit, appeal);
  if (appeal != bestAppeal)
    {
      bestAppeal = appeal;
      return indexOfBestValueInThis;
    }
#endif
  return 0;
}

void Attribute::chooseValue(const unsigned int indexOfValue)
{
  swap(values[potentialIndex], values[indexOfValue]);
}

void Attribute::setPotentialValueIrrelevant(const vector<Value*>::iterator potentialValueIt)
{
  swap(*potentialValueIt, values[--irrelevantIndex]);
}

bool Attribute::findIrrelevantValuesAndCheckTauContiguity(const vector<Attribute*>::iterator attributeBegin, const vector<Attribute*>::iterator attributeEnd)
{
  // A potential value is irrelevant if at least one previous present values is not extensible with it
  vector<Value*>::iterator potentialEnd = values.begin() + irrelevantIndex;
  for (vector<Value*>::iterator potentialValueIt = values.begin() + potentialIndex; potentialValueIt != potentialEnd; )
    {
      if (valueDoesNotExtendPresent(*potentialValueIt, attributeBegin, attributeEnd))
	{
#ifdef DEBUG
	  cout << labelsVector[id][(*potentialValueIt)->getDataId()] << " in attribute " << internal2ExternalAttributeOrder[id] << " will never be present nor extend any future pattern" << endl;
#endif
	  swap(*potentialValueIt, *--potentialEnd);
	  --irrelevantIndex;
	}
      else
	{
	  ++potentialValueIt;
	}
    }
  return false;
}

#ifdef MIN_SIZE_ELEMENT_PRUNING
void Attribute::resetPresentAndPotentialIrrelevancyThreshold()
{
  presentAndPotentialIrrelevancyThreshold = 0;
}

bool Attribute::possiblePresentAndPotentialIrrelevancy(const vector<Attribute*>::const_iterator attributeBegin, const vector<Attribute*>::const_iterator attributeEnd)
{
  if ((presentAndPotentialIrrelevancyThreshold != 0 && minArea == 0) || irrelevantIndex == 0) // only irrelevantIndex == 0 needed for correctness but minSizeIrrelevancyThreshold takes time, whereas presentAndPotentialIrrelevancyThreshold != 0 && minArea == 0 does not
    {
      return false;
    }
  const unsigned int threshold = minSizeIrrelevancyThreshold(attributeBegin, attributeEnd);
  if (threshold <= presentAndPotentialIrrelevancyThreshold)
    {
      return false;
    }
  presentAndPotentialIrrelevancyThreshold = threshold;
  return true;
}

bool Attribute::presentAndPotentialIrrelevant() const
{
  const vector<Value*>::const_iterator end = values.begin() + potentialIndex;
  vector<Value*>::const_iterator presentValueIt = values.begin();
  for (; presentValueIt != end && (*presentValueIt)->getPresentAndPotentialNoise() <= presentAndPotentialIrrelevancyThreshold; ++presentValueIt)
    {
    }
#ifdef DEBUG
  if (presentValueIt != end)
    {
      cout << labelsVector[id][(*presentValueIt)->getDataId()] << " in attribute " << internal2ExternalAttributeOrder[id] << " contains too much potential noise given the size constraints -> Prune!" << endl;
    }
#endif
  return presentValueIt != end;
}

pair<bool, vector<unsigned int>> Attribute::findPresentAndPotentialIrrelevantValuesAndCheckTauContiguity()
{
  vector<unsigned int> newIrrelevantValueDataIds;
  const vector<Value*>::iterator potentialBegin = values.begin() + potentialIndex;
  vector<Value*>::iterator potentialEnd = values.begin() + irrelevantIndex;
  for (vector<Value*>::iterator potentialValueIt = potentialBegin; potentialValueIt != potentialEnd; )
    {
      // **potentialValueIt is irrelevant if it contains too much noise in any extension satisfying the minimal size constraints
      if (presentAndPotentialIrrelevantValue(**potentialValueIt))
	{
	  newIrrelevantValueDataIds.push_back((*potentialValueIt)->getDataId());
	  swap(*potentialValueIt, *--potentialEnd);
	  --irrelevantIndex;
	}
      else
	{
	  ++potentialValueIt;
	}
    }
  return pair<bool, vector<unsigned int>>(false, newIrrelevantValueDataIds);
}

bool Attribute::presentAndPotentialIrrelevantValue(const Value& value) const
{
#ifdef DEBUG
  if (value.getPresentAndPotentialNoise() > presentAndPotentialIrrelevancyThreshold)
    {
      cout << "Given the minimal size constraints, " << labelsVector[id][value.getDataId()] << " in attribute " << internal2ExternalAttributeOrder[id] << " will never be present nor extend any future pattern" << endl;
    }
#endif
  return value.getPresentAndPotentialNoise() > presentAndPotentialIrrelevancyThreshold;
}

void Attribute::presentAndPotentialCleanAbsent()
{
  const vector<Value*>::iterator absentBegin = values.begin() + absentIndex;
  for (vector<Value*>::iterator absentValueIt = absentBegin; absentValueIt != values.end(); )
    {
      if ((*absentValueIt)->getPresentAndPotentialNoise() > presentAndPotentialIrrelevancyThreshold)
	{
	  removeAbsentValue(absentValueIt);
	}
      else
	{
	  ++absentValueIt;
	}
    }
}

unsigned int Attribute::minSizeIrrelevancyThreshold(const vector<Attribute*>::const_iterator attributeBegin, const vector<Attribute*>::const_iterator attributeEnd) const
{
  // Sum epsilon with the number of non-self-loops tuples in the maximal pattern and subtract the number of non-self-loops tuples in the minimal pattern
  bool isOneMinSize0 = false;
  unsigned int minNbOfSymmetricElements = 0;
  unsigned int maxNbOfSymmetricElements = 0;
  vector<float> minAreaDividedByProductsOfSubsequentMinSizes;
  minAreaDividedByProductsOfSubsequentMinSizes.reserve(maxId);
  minAreaDividedByProductsOfSubsequentMinSizes.push_back(-.5 + minArea); // -.5 to avoid round-off issues
  vector<unsigned int> minSizesOfOrthogonalAttributes;
  minSizesOfOrthogonalAttributes.reserve(maxId - 1);
  unsigned int maxAreaOfValue = 1;
  vector<Attribute*> orthogonalAttributes;
  orthogonalAttributes.reserve(maxId - 1);
  vector<Attribute*>::const_iterator attributeIt = attributeBegin;
  for (; *attributeIt != this; ++attributeIt)
    {
      orthogonalAttributes.push_back(*attributeIt);
    }
  while (++attributeIt != attributeEnd)
    {
      orthogonalAttributes.push_back(*attributeIt);
    }
  const vector<Attribute*>::const_reverse_iterator rend = orthogonalAttributes.rend();
  for (vector<Attribute*>::const_reverse_iterator orthogonalAttributeIt = orthogonalAttributes.rbegin(); orthogonalAttributeIt != rend; ++orthogonalAttributeIt)
    {
      const unsigned int minSize = max((*orthogonalAttributeIt)->potentialIndex, minSizes[(*orthogonalAttributeIt)->id]);
      if (minSize == 0)
	{
	  minSizesOfOrthogonalAttributes.push_back(1);
	  minAreaDividedByProductsOfSubsequentMinSizes.push_back(minAreaDividedByProductsOfSubsequentMinSizes.back());
	  isOneMinSize0 = true;
	}
      else
	{
	  minSizesOfOrthogonalAttributes.push_back(minSize);
	  minAreaDividedByProductsOfSubsequentMinSizes.push_back(minAreaDividedByProductsOfSubsequentMinSizes.back() / minSize);
	}
      maxAreaOfValue *= (*orthogonalAttributeIt)->absentIndex;
      if ((*orthogonalAttributeIt)->symmetric())
	{
	  if (symmetric())
	    {
	      // To compute the minimal number of non-self loops on a symmetric value, the cardinality of this dimension is not fixed to the maximum
	      minAreaDividedByProductsOfSubsequentMinSizes.back() /= minSize;
	    }
	  if ((*orthogonalAttributeIt)->absentIndex > maxNbOfSymmetricElements) // absentIndex can differ between symmetric attributes!
      	    {
	      maxNbOfSymmetricElements = (*orthogonalAttributeIt)->absentIndex;
	      minNbOfSymmetricElements = minSize;
	    }
	}
    }
  if (maxNbOfSymmetricElements == 0)
    {
      if (minAreaDividedByProductsOfSubsequentMinSizes.back() > irrelevantIndex)
  	{
#ifdef MIN_AREA_REFINEMENT
	  unsigned int actualMinArea = numeric_limits<unsigned int>::max();
  	  orthogonalAttributes.front()->computeMinArea(minSizesOfOrthogonalAttributes.rbegin(), minAreaDividedByProductsOfSubsequentMinSizes.rbegin() + 1, orthogonalAttributes.begin(), orthogonalAttributes.end(), irrelevantIndex, actualMinArea);
  	  return epsilonVector[id] + (maxAreaOfValue - actualMinArea / irrelevantIndex) * noisePerUnit;
#else
	  return epsilonVector[id] + (maxAreaOfValue - ceil(minArea / irrelevantIndex)) * noisePerUnit;
#endif
  	}
      if (isOneMinSize0)
  	{
  	  return epsilonVector[id] + (maxAreaOfValue - ceil(minArea / irrelevantIndex)) * noisePerUnit;
  	}
      return epsilonVector[id] + (maxAreaOfValue - round(1. / minAreaDividedByProductsOfSubsequentMinSizes.back() * (-.5 + minArea))) * noisePerUnit;
    }
  if (symmetric())
    {
      const unsigned int minSize = max(potentialIndex, minSizes[id]);
      if (minAreaDividedByProductsOfSubsequentMinSizes.back() * minSize > irrelevantIndex)
	{
#ifdef MIN_AREA_REFINEMENT
	  unsigned int minNbOfNonSelfLoopsOnASymmetricValue = numeric_limits<unsigned int>::max();
	  orthogonalAttributes.front()->computeMinNbOfNonSelfLoopsOnASymmetricValue(minSizesOfOrthogonalAttributes.rbegin(), minAreaDividedByProductsOfSubsequentMinSizes.rbegin() + 1, orthogonalAttributes.begin(), orthogonalAttributes.end(), 0, 1, minNbOfNonSelfLoopsOnASymmetricValue);
	  return epsilonVector[id] + (maxAreaOfValue - maxAreaOfValue / maxNbOfSymmetricElements - minNbOfNonSelfLoopsOnASymmetricValue) * noisePerUnit;
#else
	  return epsilonVector[id] + (maxAreaOfValue - ceil(minArea / irrelevantIndex)) * noisePerUnit;
#endif
	}
      if (isOneMinSize0)
	{
	  return epsilonVector[id] + (maxAreaOfValue - ceil(minArea / irrelevantIndex)) * noisePerUnit;
	}
      const unsigned int minAreaOfValue = round(1. / minAreaDividedByProductsOfSubsequentMinSizes.back() * (-.5 + minArea) / minSize);
      return epsilonVector[id] + (maxAreaOfValue - maxAreaOfValue / maxNbOfSymmetricElements - minAreaOfValue + minAreaOfValue / minNbOfSymmetricElements) * noisePerUnit;
    }
  if (minAreaDividedByProductsOfSubsequentMinSizes.back() > irrelevantIndex)
    {
#ifdef MIN_AREA_REFINEMENT
      unsigned int minNbOfNonSelfLoops = numeric_limits<unsigned int>::max();
      orthogonalAttributes.front()->computeMinNbOfNonSelfLoopsWithSymmetricAttributes(minSizesOfOrthogonalAttributes.rbegin(), minAreaDividedByProductsOfSubsequentMinSizes.rbegin() + 1, orthogonalAttributes.begin(), orthogonalAttributes.end(), 0, irrelevantIndex, minNbOfNonSelfLoops);
      return epsilonVector[id] + (maxAreaOfValue - maxAreaOfValue / maxNbOfSymmetricElements - minNbOfNonSelfLoops / irrelevantIndex) * noisePerUnit;
#else
      return epsilonVector[id] + (maxAreaOfValue - ceil(minArea / irrelevantIndex)) * noisePerUnit;
#endif
    }
  if (isOneMinSize0)
    {
      return epsilonVector[id] + (maxAreaOfValue - ceil(minArea / irrelevantIndex)) * noisePerUnit;
    }
  const unsigned int minAreaOfValue = round(1. / minAreaDividedByProductsOfSubsequentMinSizes.back() * (-.5 + minArea));
  return epsilonVector[id] + (maxAreaOfValue - maxAreaOfValue / maxNbOfSymmetricElements - minAreaOfValue + minAreaOfValue / minNbOfSymmetricElements) * noisePerUnit;
}

#ifdef PRE_PROCESS
void Attribute::setPresentAndPotentialIrrelevancyThreshold(const vector<Attribute*>::const_iterator attributeBegin, const vector<Attribute*>::const_iterator attributeEnd)
{
  presentAndPotentialIrrelevancyThreshold = minSizeIrrelevancyThreshold(attributeBegin, attributeEnd);
}
#endif

#ifdef MIN_AREA_REFINEMENT
void Attribute::computeMinArea(const vector<unsigned int>::const_reverse_iterator minSizeIt, const vector<float>::const_reverse_iterator minAreaDividedByProductOfSubsequentMinSizesIt, const vector<Attribute*>::const_iterator thisIt, const vector<Attribute*>::const_iterator attributeEnd, const unsigned int currentArea, unsigned int& minArea) const
{
  const vector<Attribute*>::const_iterator nextIt = thisIt + 1;
  if (nextIt == attributeEnd)
    {
      unsigned int factor = ceil(*minAreaDividedByProductOfSubsequentMinSizesIt / currentArea);
      if (factor <= irrelevantIndex)
      	{
	  if (factor < *minSizeIt)
	    {
	      factor = *minSizeIt;
	    }
	  factor *= currentArea;
      	  if (factor < minArea)
      	    {
      	      minArea = factor;
      	    }
      	}
      return;
    }
  const vector<unsigned int>::const_reverse_iterator nextMinSizesIt = minSizeIt + 1;
  const vector<float>::const_reverse_iterator nextMinAreaDividedByProductOfSubsequentMinSizesIt = minAreaDividedByProductOfSubsequentMinSizesIt + 1;
  const unsigned int end = min(static_cast<unsigned int>(ceil(*minAreaDividedByProductOfSubsequentMinSizesIt / currentArea)), irrelevantIndex);
  for (unsigned int factor = *minSizeIt; factor <= end; ++factor)
    {
      (*nextIt)->computeMinArea(nextMinSizesIt, nextMinAreaDividedByProductOfSubsequentMinSizesIt, nextIt, attributeEnd, factor * currentArea, minArea);
    }
}

void Attribute::computeMinNbOfNonSelfLoopsWithSymmetricAttributes(const vector<unsigned int>::const_reverse_iterator minSizeIt, const vector<float>::const_reverse_iterator minAreaDividedByProductOfSubsequentMinSizesIt, const vector<Attribute*>::const_iterator thisIt, const vector<Attribute*>::const_iterator attributeEnd, const unsigned int nbOfSymmetricElements, const unsigned int currentArea, unsigned int& minNbOfNonSelfLoops) const
{
  const vector<Attribute*>::const_iterator nextIt = thisIt + 1;
  if (nextIt == attributeEnd)
    {
      unsigned int factor = ceil(*minAreaDividedByProductOfSubsequentMinSizesIt / currentArea);
      if (factor <= irrelevantIndex)
	{
	  if (factor < *minSizeIt)
	    {
	      factor = *minSizeIt;
	    }
	  factor *= currentArea;
	  factor -= factor / nbOfSymmetricElements;
	  if (factor < minNbOfNonSelfLoops)
	    {
	      minNbOfNonSelfLoops = factor;
	    }
	}
      return;
    }
  const vector<unsigned int>::const_reverse_iterator nextMinSizesIt = minSizeIt + 1;
  const vector<float>::const_reverse_iterator nextMinAreaDividedByProductOfSubsequentMinSizesIt = minAreaDividedByProductOfSubsequentMinSizesIt + 1;
  const unsigned int end = min(static_cast<unsigned int>(ceil(*minAreaDividedByProductOfSubsequentMinSizesIt / currentArea)), irrelevantIndex);
  for (unsigned int factor = *minSizeIt; factor <= end; ++factor)
    {
      (*nextIt)->computeMinNbOfNonSelfLoopsWithSymmetricAttributes(nextMinSizesIt, nextMinAreaDividedByProductOfSubsequentMinSizesIt, nextIt, attributeEnd, nbOfSymmetricElements, factor * currentArea, minNbOfNonSelfLoops);
    }
}

void Attribute::computeMinNbOfNonSelfLoopsOnASymmetricValue(const vector<unsigned int>::const_reverse_iterator minSizeIt, const vector<float>::const_reverse_iterator minAreaDividedByProductOfSubsequentMinSizesIt, const vector<Attribute*>::const_iterator thisIt, const vector<Attribute*>::const_iterator attributeEnd, const unsigned int nbOfSymmetricElements, const unsigned int currentArea, unsigned int& minNbOfNonSelfLoopsOnAValue) const
{
  const vector<Attribute*>::const_iterator nextIt = thisIt + 1;
  if (nextIt == attributeEnd)
    {
      unsigned int factor = ceil(*minAreaDividedByProductOfSubsequentMinSizesIt / currentArea);
      if (factor <= irrelevantIndex)
	{
	  if (factor < *minSizeIt)
	    {
	      factor = *minSizeIt;
	    }
	  factor *= currentArea / nbOfSymmetricElements;
	  factor -= factor / nbOfSymmetricElements;
	  if (factor < minNbOfNonSelfLoopsOnAValue)
	    {
	      minNbOfNonSelfLoopsOnAValue = factor;
	    }
	}
      return;
    }
  const vector<unsigned int>::const_reverse_iterator nextMinSizesIt = minSizeIt + 1;
  const vector<float>::const_reverse_iterator nextMinAreaDividedByProductOfSubsequentMinSizesIt = minAreaDividedByProductOfSubsequentMinSizesIt + 1;
  const unsigned int end = min(static_cast<unsigned int>(ceil(*minAreaDividedByProductOfSubsequentMinSizesIt / currentArea)), irrelevantIndex);
  for (unsigned int factor = *minSizeIt; factor <= end; ++factor)
    {
      (*nextIt)->computeMinNbOfNonSelfLoopsOnASymmetricValue(nextMinSizesIt, nextMinAreaDividedByProductOfSubsequentMinSizesIt, nextIt, attributeEnd, nbOfSymmetricElements, factor * currentArea, minNbOfNonSelfLoopsOnAValue);
    }
}
#endif
#endif

bool Attribute::valueDoesNotExtendPresent(const Value* value, const vector<Attribute*>::const_iterator attributeBegin, const vector<Attribute*>::const_iterator attributeEnd) const
{
  const vector<unsigned int>::const_iterator thisEpsilonIt = epsilonVector.begin() + id;
  if (value->getPresentNoise() > *thisEpsilonIt)
    {
      return true;
    }
  if(id == 0) // first attribute 2d
    {
      const vector<Attribute*>::const_iterator otherAttribute = attributeBegin + 1;
      return !value->extendsFuturePresent((*otherAttribute)->presentBegin(), (*otherAttribute)->presentEnd(), epsilonVector.back());
    }
  // second attribute 2d
  return !value->extendsPastPresent((*attributeBegin)->presentBegin(), (*attributeBegin)->presentEnd(), epsilonVector.front());
}

vector<unsigned int> Attribute::eraseIrrelevantValues()
{
  vector<unsigned int> dataIds;
  dataIds.reserve(absentIndex - irrelevantIndex);
  const vector<Value*>::iterator begin = values.begin() + irrelevantIndex;
  const vector<Value*>::iterator end = values.begin() + absentIndex;
  for (vector<Value*>::iterator irrelevantValueIt = begin; irrelevantValueIt != end; ++irrelevantValueIt)
    {
      dataIds.push_back((*irrelevantValueIt)->getDataId());
      delete *irrelevantValueIt;
    }
  if (id == orderedAttributeId)
    {
      sort(dataIds.begin(), dataIds.end());
    }
  values.erase(begin, end);
  absentIndex = irrelevantIndex;
  return dataIds;
}

bool Attribute::unclosed(const vector<Attribute*>::const_iterator attributeBegin, const vector<Attribute*>::const_iterator attributeEnd) const
{
  const vector<Value*>::const_iterator absentEnd = values.end();
  vector<Value*>::const_iterator absentValueIt = values.begin() + irrelevantIndex;
  for (; absentValueIt != absentEnd && valueDoesNotExtendPresentAndPotential(*absentValueIt, attributeBegin, attributeEnd); ++absentValueIt)
    {
    }
#ifdef DEBUG
  if (absentValueIt != absentEnd)
    {
      cout << labelsVector[id][(*absentValueIt)->getDataId()] << " in attribute " << internal2ExternalAttributeOrder[id] << " extends any future pattern -> Prune!" << endl;
    }
#endif
  return absentValueIt != absentEnd;
}

bool Attribute::valueDoesNotExtendPresentAndPotential(const Value* value, const vector<Attribute*>::const_iterator attributeBegin, const vector<Attribute*>::const_iterator attributeEnd) const
{
  const vector<unsigned int>::const_iterator thisEpsilonIt = epsilonVector.begin() + id;
  if (value->getPresentAndPotentialNoise() > *thisEpsilonIt)
    {
      return true;
    }
  if(id == 0) // first attribute 2d
      {
        const vector<Attribute*>::const_iterator otherAttribute = attributeBegin + 1;
        return !value->extendsFuturePresentAndPotential((*otherAttribute)->presentBegin(), (*otherAttribute)->potentialEnd(), epsilonVector.back());
      }
    // second attribute 2d
    return !value->extendsPastPresentAndPotential((*attributeBegin)->presentBegin(), (*attributeBegin)->potentialEnd(), epsilonVector.front());
}

void Attribute::cleanAbsent(const vector<Attribute*>::const_iterator attributeBegin, const vector<Attribute*>::const_iterator attributeEnd)
{
  const vector<Value*>::iterator absentBegin = values.begin() + absentIndex;
  for (vector<Value*>::iterator absentValueIt = absentBegin; absentValueIt != values.end(); )
    {
      if (valueDoesNotExtendPresent(*absentValueIt, attributeBegin, attributeEnd))
	{
	  removeAbsentValue(absentValueIt);
	}
      else
	{
	  ++absentValueIt;
	}
    }
}

void Attribute::removeAbsentValue(vector<Value*>::iterator absentValueIt)
{
#ifdef DEBUG
  cout << labelsVector[id][(*absentValueIt)->getDataId()] << " in attribute " << internal2ExternalAttributeOrder[id] << " will never extend any future pattern" << endl;
#endif
  delete *absentValueIt;
  *absentValueIt = values.back();
  values.pop_back();
}

bool Attribute::finalizable() const
{
  if (isClosedVector[id] || potentialIndex == irrelevantIndex)
    {
      const unsigned int epsilon = epsilonVector[id];
      const vector<Value*>::const_iterator end = values.begin() + irrelevantIndex;
      vector<Value*>::const_iterator valueIt = values.begin();
      for (; valueIt != end && (*valueIt)->getPresentAndPotentialNoise() <= epsilon; ++valueIt)
	{
	}
      return valueIt == end;
    }
  return false;
}

vector<unsigned int> Attribute::finalize() const
{
  vector<unsigned int> dataIdsOfValuesSetPresent;
  dataIdsOfValuesSetPresent.reserve(irrelevantIndex - potentialIndex);
  const vector<Value*>::const_iterator potentialEnd = values.begin() + irrelevantIndex;
  for (vector<Value*>::const_iterator potentialValueIt = values.begin() + potentialIndex; potentialValueIt != potentialEnd; ++potentialValueIt)
    {
      dataIdsOfValuesSetPresent.push_back((*potentialValueIt)->getDataId());
    }
  return dataIdsOfValuesSetPresent;
}

unsigned int Attribute::lastAttributeId()
{
  return maxId - 1;
}

const vector<unsigned int>& Attribute::getEpsilonVector()
{
  return epsilonVector;
}

unsigned int Attribute::getMinSize(const unsigned int attributeId)
{
  return minSizes[attributeId];
}

unsigned int Attribute::getMinArea()
{
  return minArea;
}

void Attribute::setMinAreaIsClosedVectorAndIsStorageAllDense(const unsigned int minAreaParam, const vector<bool>& isClosedVectorParam, const bool isStorageAllDense)
{
  unsigned int minAreaAccordingToMinSizes = 1;
  for (const unsigned int minSize : minSizes)
    {
      minAreaAccordingToMinSizes *= minSize;
    }
  if (minAreaAccordingToMinSizes < minAreaParam)
    {
      minArea = minAreaParam;
    }
  else
    {
      minArea = 0;
    }
  isClosedVector = isClosedVectorParam;
  if (isStorageAllDense)
    {
      orderedAttributeId = numeric_limits<unsigned int>::max();
      return;
    }
  orderedAttributeId = maxId - 1;
}

void Attribute::setOutputFormat(const char* outputElementSeparatorParam, const char* emptySetStringParam, const char* elementNoiseSeparatorParam, const bool isNoisePrintedParam)
{
  outputElementSeparator = outputElementSeparatorParam;
  emptySetString = emptySetStringParam;
  elementNoiseSeparator = elementNoiseSeparatorParam;
  isNoisePrinted = isNoisePrintedParam;
}

void Attribute::printOutputElementSeparator(ostream& out)
{
  out << outputElementSeparator;
}

void Attribute::printEmptySetString(ostream& out)
{
  out << emptySetString;
}

bool Attribute::lessAppealingIrrelevant(const Attribute* attribute, const Attribute* otherAttribute)
{
  return (attribute->absentIndex - attribute->irrelevantIndex) * otherAttribute->values.size() < (otherAttribute->absentIndex - otherAttribute->irrelevantIndex) * attribute->values.size();
}
