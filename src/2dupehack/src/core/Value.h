// Copyright 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of multidupehack.

// multidupehack is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// multidupehack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with multidupehack; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef VALUE_H_
#define VALUE_H_

#include "../../Parameters.h"

#include <vector>

// avoid circular dependency
class Trie;
class Tube;

using namespace std;

class Value
{
 public:
  static Trie* data;

  Value(const unsigned int dataId);
  Value(const unsigned int dataId, const unsigned int presentAndPotentialNoise);
  Value(const Value& otherValue);

  virtual ~Value();

  unsigned int getDataId() const;
  unsigned int getPresentNoise() const;
  unsigned int getPresentAndPotentialNoise() const;

  void incrementPresentNoise();
  void addPresentNoise(const unsigned int noise);
  void decrementPotentialNoise();
  void subtractPotentialNoise(const unsigned int noise);

  bool extendsPastPresent(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const;
  bool extendsFuturePresent(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const;
  bool extendsPastPresentAndPotential(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const;
  bool extendsFuturePresentAndPotential(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const;

  bool symmetricValuesExtendPastPresent(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const;
  bool symmetricValuesExtendFuturePresent(const Value& symmetricValue, const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const;
  bool symmetricValuesExtendPastPresentAndPotential(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const;
  bool symmetricValuesExtendFuturePresentAndPotential(const Value& symmetricValue, const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const;

  static bool smallerDataId(const Value* value, const Value* otherValue);

 protected:
  unsigned int dataId;
  unsigned int presentNoise;
  unsigned int presentAndPotentialNoise;
};

#endif /*VALUE_H_*/
