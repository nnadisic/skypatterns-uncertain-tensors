// Copyright 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of multidupehack.

// multidupehack is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// multidupehack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with multidupehack; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "Value.h"

#include "Trie.h"

Trie* Value::data;

Value::Value(const unsigned int dataIdParam): dataId(dataIdParam), presentNoise(0), presentAndPotentialNoise(0)
{
}

Value::Value(const unsigned int dataIdParam, const unsigned int presentAndPotentialNoiseParam): dataId(dataIdParam), presentNoise(0), presentAndPotentialNoise(presentAndPotentialNoiseParam)
{
}

Value::Value(const Value& parent): dataId(parent.dataId), presentNoise(parent.presentNoise), presentAndPotentialNoise(parent.presentAndPotentialNoise)
{
}

Value::~Value()
{
}

unsigned int Value::getDataId() const
{
  return dataId;
}

unsigned int Value::getPresentNoise() const
{
  return presentNoise;
}

unsigned int Value::getPresentAndPotentialNoise() const
{
  return presentAndPotentialNoise;
}

void Value::incrementPresentNoise()
{
  ++presentNoise;
}

void Value::addPresentNoise(const unsigned int noise)
{
  presentNoise += noise;
}

void Value::decrementPotentialNoise()
{
  --presentAndPotentialNoise;
}

void Value::subtractPotentialNoise(const unsigned int noise)
{
  presentAndPotentialNoise -= noise;
}

bool Value::extendsPastPresent(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const
{
  vector<Value*>::const_iterator valueIt = valueBegin;
  for (; valueIt != valueEnd && (*valueIt)->presentNoise + data->getTube((*valueIt)->dataId)->noiseOnValue(dataId) <= threshold; ++valueIt)
    {
    }
  return valueIt == valueEnd;
}

bool Value::extendsFuturePresent(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const
{
  vector<Value*>::const_iterator valueIt = valueBegin;
  const Tube* tube = data->getTube(dataId);
  for (; valueIt != valueEnd && (*valueIt)->presentNoise + tube->noiseOnValue((*valueIt)->dataId) <= threshold; ++valueIt)
    {
    }
  return valueIt == valueEnd;
}

bool Value::extendsPastPresentAndPotential(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const
{
  vector<Value*>::const_iterator valueIt = valueBegin;
  for (; valueIt != valueEnd && (*valueIt)->presentAndPotentialNoise + data->getTube((*valueIt)->dataId)->noiseOnValue(dataId) <= threshold; ++valueIt)
    {
    }
  return valueIt == valueEnd;
}

bool Value::extendsFuturePresentAndPotential(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const
{
  vector<Value*>::const_iterator valueIt = valueBegin;
  const Tube* tube = data->getTube(dataId);
  for (; valueIt != valueEnd && (*valueIt)->presentAndPotentialNoise + tube->noiseOnValue((*valueIt)->dataId) <= threshold; ++valueIt)
    {
    }
  return valueIt == valueEnd;
}

bool Value::symmetricValuesExtendPastPresent(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const
{
  vector<Value*>::const_iterator valueIt = valueBegin;
  for (; valueIt != valueEnd && (*valueIt)->presentNoise + data->getTube((*valueIt)->dataId)->noiseOnValue(dataId) + data->getTube((*valueIt)->dataId + 1)->noiseOnValue(dataId) <= threshold; ++valueIt)
    {
    }
  return valueIt == valueEnd;
}

bool Value::symmetricValuesExtendFuturePresent(const Value& symmetricValue, const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const
{
  const Tube* tube1 = data->getTube(dataId + 1);
  const Tube* tube2 = data->getTube(dataId);
  vector<Value*>::const_iterator valueIt = valueBegin;
  for (; valueIt != valueEnd && (*valueIt)->presentNoise + tube1->noiseOnValue((*valueIt)->dataId) + tube2->noiseOnValue((*valueIt)->dataId) <= threshold; ++valueIt)
    {
    }
  return valueIt == valueEnd;
}

bool Value::symmetricValuesExtendPastPresentAndPotential(const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const
{
  vector<Value*>::const_iterator valueIt = valueBegin;
  for (; valueIt != valueEnd && (*valueIt)->presentAndPotentialNoise + data->getTube((*valueIt)->dataId)->noiseOnValue(dataId) + data->getTube((*valueIt)->dataId + 1)->noiseOnValue(dataId) <= threshold; ++valueIt)
    {
    }
  return valueIt == valueEnd;
}

bool Value::symmetricValuesExtendFuturePresentAndPotential(const Value& symmetricValue, const vector<Value*>::const_iterator valueBegin, const vector<Value*>::const_iterator valueEnd, const unsigned int threshold) const
{
  const Tube* tube1 = data->getTube(dataId + 1);
  const Tube* tube2 = data->getTube(dataId);
  vector<Value*>::const_iterator valueIt = valueBegin;
  for (; valueIt != valueEnd && (*valueIt)->presentAndPotentialNoise + tube1->noiseOnValue((*valueIt)->dataId) + tube2->noiseOnValue((*valueIt)->dataId) <= threshold; ++valueIt)
    {
    }
  return valueIt == valueEnd;
}

bool Value::smallerDataId(const Value* value, const Value* otherValue)
{
  return value->getDataId() < otherValue->getDataId();
}
