// Copyright 2010,2011,2012,2013,2014,2015,2016,2017 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of multidupehack.

// multidupehack is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// multidupehack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with multidupehack; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "Tube.h"

Tube::~Tube()
{
}

bool Tube::setSelfLoopsInSymmetricAttributes(const unsigned int hyperplaneId, const unsigned int lastSymmetricAttributeId, const unsigned int dimensionId)
{
  // Never called because self loops are inserted first, the tubes are initially sparse and the method is overridden in those classes
  return false;
}

unsigned int Tube::setPresentAfterPresentValueMet(const vector<Attribute*>::iterator attributeIt) const
{
  presentFixPotentialOrAbsentValuesAfterPresentValueMet(**attributeIt);
  return presentFixPresentValuesAfterPresentValueMet(**attributeIt);
}

unsigned int Tube::setSymmetricPresentAfterPresentValueMet(const vector<Attribute*>::iterator attributeIt) const
{
  return presentFixPresentValuesAfterPresentValueMet(**attributeIt);
}

unsigned int Tube::setPresentAfterPresentValueMetAndPotentialOrAbsentUsed(const vector<Attribute*>::iterator attributeIt) const
{
  return presentFixPresentValuesAfterPresentValueMetAndPotentialOrAbsentUsed(**attributeIt);
}

unsigned int Tube::setAbsent(const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  // *this necessarily relates to the absent attribute
  return noiseOnValue((*absentAttributeIt)->getChosenAbsentValueDataId());
}

unsigned int Tube::setIrrelevant(const vector<Attribute*>::iterator irrelevantAttributeIt, const vector<unsigned int>& irrelevantValueDataIds, const vector<Attribute*>::iterator attributeIt) const
{
  // *this necessarily relates to the irrelevant attribute
  return noiseOnValues(irrelevantValueDataIds);
}

unsigned int Tube::setAbsentAfterAbsentValueMet(const vector<Attribute*>::iterator attributeIt) const
{
  absentFixAbsentValuesAfterAbsentValueMet((*attributeIt)->absentBegin(), (*attributeIt)->absentEnd());
  return absentFixPresentOrPotentialValuesAfterAbsentValueMet(**attributeIt);
}

unsigned int Tube::setSymmetricAbsentAfterAbsentValueMet(const vector<Attribute*>::iterator attributeIt) const
{
  // The first absent value actually is the value set absent and there is no noise to be found at the intersection of a vertex (seen as an outgoing vertex) and itself (seen as an ingoing vertex)
  absentFixAbsentValuesAfterAbsentValueMet((*attributeIt)->absentBegin() + 1, (*attributeIt)->absentEnd());
  return absentFixPresentOrPotentialValuesAfterAbsentValueMet(**attributeIt);
}

unsigned int Tube::setAbsentAfterAbsentUsed(const vector<Attribute*>::iterator absentAttributeIt, const vector<Attribute*>::iterator attributeIt) const
{
  // *this necessarily relates to the absent attribute
  return noiseOnValue((*absentAttributeIt)->getChosenAbsentValueDataId());
}

unsigned int Tube::setIrrelevantAfterAbsentUsed(const vector<Attribute*>::iterator irrelevantAttributeIt, const vector<unsigned int>& irrelevantValueDataIds, const vector<Attribute*>::iterator attributeIt) const
{
  // *this necessarily relates to the irrelevant attribute
  return noiseOnValues(irrelevantValueDataIds);
}

unsigned int Tube::setAbsentAfterAbsentValueMetAndAbsentUsed(const vector<Attribute*>::iterator attributeIt) const
{
  return absentFixPresentOrPotentialValuesAfterAbsentValueMetAndAbsentUsed(**attributeIt);
}


void Tube::printTuple(const vector<unsigned int>& prefix, const unsigned int lastElement, const float membership, ostream& out)
{
  for (const unsigned int id : prefix)
    {
      out << id << ' ';
    }
  out << lastElement << ' ' << membership << endl;
}
