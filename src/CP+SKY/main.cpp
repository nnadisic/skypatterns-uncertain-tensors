#include <gecode/int.hh>
#include <gecode/search.hh>
#include <gecode/minimodel.hh>

#include "src/DataReader.cpp"
#include "src/Solutions.cpp"

#include <vector>
#include <list>
#include <map>
#include <set>
#include <fstream>
#include <sstream>

using namespace Gecode;
using namespace std;

vector<Solution*> candidates;
vector<bool> usedmeasures;
int nbCandidates  = 0;
int nMeasuresUsed = 0;
bool hasExtension = false;
const int nbAttributs = 1;
map <string, bool> child_equal;
map <string, vector<Solution*> > child_solution;

vector<string> &split(const string &s, char delim, vector<string> &elems) {
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim))
		elems.push_back(item);
	return elems;
}
	
vector<string> split(const string &s, char delim) {
	vector<string> elems;
	split(s, delim, elems);
	return elems;
}

///CONSTRAINTS

void exists_superior(Space& space, IntVarArgs measures, int b_measures[], BoolVar superior) {
	BoolVarArgs d;
	for(unsigned int i = 0; i < usedmeasures.size(); i++) {
		if(usedmeasures.at(i))
			d << expr(space, measures[i] > b_measures[i]);
	}
	rel(space, superior == (sum(d) > 0));
}

void is_equal(Space& space, IntVarArgs measures, int b_measures[], BoolVar equal) {
	BoolVarArgs d;
	for(unsigned int i = 0; i < usedmeasures.size(); i++) {
		if(usedmeasures.at(i))
			d << expr(space, measures[i] == b_measures[i]);
	}
	rel(space, equal == (sum(d) == nMeasuresUsed));
}

void not_dominated_by(Space& space, IntVarArgs measures, int b_measures[]) {
	BoolVar hasSuperior(space, 0, 1);
	exists_superior(space, measures, b_measures, hasSuperior);
	BoolVar isEqual(space, 0, 1);
	is_equal(space, measures, b_measures, isEqual);
	rel(space, hasSuperior, BOT_OR, isEqual, 1);
}

void not_equal(Space& space, IntVarArray item, vector<int> _pattern, int nr_i) {
	BoolVarArgs d;
	for(vector<int>::iterator it = _pattern.begin(); it != _pattern.end(); ++it)
		d << expr(space, item[(*it) - 1] != 1);
	BoolVarArgs s;
	for(int i = 0 ; i < nr_i; i++)
		s << expr(space, item[i] == 1);
	rel(space, sum(d) > 0 || sum(s) != _pattern.size());
}

class Skypattern : public Space {
protected:
	vector<vector<float> > tdb;
	/// Item Variables (booleans)
	BoolVarArray bitem;
	/// Item Variables (channeled integers)
	IntVarArray item;
	/// Item Variables (booleans) for reified frequency
	BoolVarArray bfitem;
	/// Transaction Variables (booleans)
	BoolVarArray btransaction, btransaction1;
	/// Transaction Variables (channeled integers)
	IntVarArray transaction, transaction1;
	/// Values Variables
	IntVar frequency, frequency1;
	IntVarArray maxValue;
	IntVarArray minValue;
	IntVarArray mean;
	IntVar growthrate;
	IntVar area;
	IntVar size;
	/// Data Variables
	IntVarArray values[nbAttributs];
public:
	/// Number of items
	int nr_i;
	/// Number of transactions
	int nr_t;

	Skypattern(string filename) {
		tdb  = get_data(filename);
		nr_t = tdb.size();
		nr_i = tdb[1].size();
		//----------------INITIAL VARIABLES---------------------//
		item          = IntVarArray (*this, nr_i, 0, 1);
		bitem         = BoolVarArray(*this, nr_i, 0, 1);
		bfitem        = BoolVarArray(*this, nr_i, 0, 1);
		transaction   = IntVarArray (*this, nr_t, 0, 1);
		btransaction  = BoolVarArray(*this, nr_t, 0, 1);
		transaction1  = IntVarArray (*this, nr_t, 0, 1);
		btransaction1 = BoolVarArray(*this, nr_t, 0, 1);
		frequency     = IntVar(*this, 0, nr_t);
		frequency1    = IntVar(*this, 0, nr_t);
		size          = IntVar(*this, 0, nr_i);
		area          = IntVar(*this, 0, nr_i * nr_t);
		maxValue      = IntVarArray(*this, nbAttributs, -100000, 100000);
		minValue      = IntVarArray(*this, nbAttributs, -100000, 100000);
		mean          = IntVarArray(*this, nbAttributs, -100000, 100000);
		growthrate    = IntVar(*this, 0, Gecode::Int::Limits::max);
		//--------------
		IntArgs col0(nr_t), col1(nr_t), col2(nr_t), row0(nr_i);
		IntVarArgs tranz0(transaction), tranz1(transaction1), itemz0(item);
		// item constraints
		BoolVarArray add_item1[nbAttributs+2];
		BoolVarArray add_item2[nbAttributs+2];
		for (int j = 0; j < nbAttributs+2; j++) {
			add_item1[j] = BoolVarArray(*this, nr_i, 0, 1);
			add_item2[j] = BoolVarArray(*this, nr_i, 0, 1);
			rel(*this, add_item1[j][0] == 1);
			rel(*this, add_item2[j][1] == 1);
		}
		rel(*this, item[0] == 0);
		rel(*this, item[1] == 0);
		for (int i = 2; i < nr_i; i++) {
			// make col0
			for (int t = 0; t < nr_t; t++) {
				col0[t] = (int)tdb[t][i];
				col1[t] = (1 - (int)tdb[t][i]);
				if(tdb[t][0] == 1)
					col2[t] = (1 - (int)tdb[t][i]);
				else
					col2[t] = 1;
			}
			// freq: the item is supported by sufficiently many trans
			// sum(col0(i_k)*Trans) >= Freq <=> bf_k
			linear(*this, col0, tranz0, IRT_GQ, 0, bfitem[i]);
			// bi_k => bf_k :: bi_k =< bf_k
			rel(*this, bitem[i], IRT_LQ, bfitem[i]);
			// bi_k = i_k
			channel(*this, bitem[i], item[i]);
			// frequent closed
			linear(*this, col1, tranz0, IRT_EQ, 0, add_item1[0][i]);
			rel(*this, bitem[i], IRT_EQ, add_item1[0][i], add_item2[0][i]);
			if(usedmeasures.at(nbAttributs+1)) {
				// frequent1 closed
				linear(*this, col2, tranz1, IRT_EQ, 0, add_item1[nbAttributs+1][i]);
			     
					rel(*this, bitem[i], IRT_EQ, add_item1[nbAttributs+1][i], add_item2[nbAttributs+1][i]);
			}
			for (int j = 0; j < nbAttributs; j++) {
				if(usedmeasures.at(j + 1)) {
					// min closed
					rel(*this, minValue[j], IRT_LQ,        values[j][i], add_item1[j + 1][i]);
					rel(*this,    bitem[i], IRT_EQ, add_item1[j + 1][i], add_item2[j + 1][i]);
				}
			}
		}
		BoolVarArray closed_expresion(*this, nbAttributs+2, 0, 1);
		rel(*this, BOT_AND, add_item2[0], closed_expresion[0]);
		for (int j = 1; j < nbAttributs+2; j++) {
			if(usedmeasures.at(j))
				rel(*this, BOT_AND, add_item2[j], closed_expresion[j]);
			else
				rel(*this, closed_expresion[j], IRT_EQ, 0);
		}
		rel(*this, BOT_OR, closed_expresion, 1);
		// transaction constraints
		int d1 = 0;
		for (int t = 0; t < nr_t; t++) {
			// make row
			row0[0] = 1;
			row0[1] = 1;
			for (int c = 2; c < nr_i; c++)
				row0[c] = (1 - (int)tdb[t][c]);
			// coverage: the trans its complement has no supported items
			// sum((1-row0(t_k))*Items) = 0 <=> bt_k
			linear(*this, row0, itemz0, IRT_EQ, 0, btransaction[t]);
			// bt_k = t_k
			channel(*this, btransaction[t], transaction[t]);
			if(tdb[t][0] == 1){ //class 1
				d1 += 1;
				linear (*this, row0, itemz0, IRT_EQ, 0, btransaction1[t]);
				channel(*this, btransaction1[t], transaction1[t]);
			}
			else {
				rel    (*this, btransaction1[t], IRT_EQ, 0);
				channel(*this, btransaction1[t], transaction1[t]);
			}
		}
		//----------------CALCULATE SIZE---------------------//
		count(*this, item, 1, IRT_EQ, size);
		rel(*this, size > 0);
		//----------------CALCULATE FREQUENCY---------------------//
		count(*this, transaction, 1, IRT_EQ, frequency);
		count(*this, transaction1, 1, IRT_EQ, frequency1);
		//----------------CALCULATE AREA---------------------//
		mult(*this, frequency, size, area);
		//----------------CALCULATE GROWTH RATE---------------------//
		rel(*this, (frequency1 != 0 || growthrate == 0) && (frequency > frequency1 || growthrate == Gecode::Int::Limits::max) && (frequency == frequency1 || (d1 * (frequency - frequency1)) * growthrate <= ((nr_t - d1) * frequency1)));
		//rel(*this, growthrate < Gecode::Int::Limits::max);
		//----------------CALCULATE MAX VALUE AND MIN VALUE---------------------//
		IntVarArray values_max[nbAttributs];
		IntVarArray values_min[nbAttributs];
		for (int j = 0; j < nbAttributs; j++) {
			values_max[j] = IntVarArray(*this, nr_i, -100000, 100000);
			values_min[j] = IntVarArray(*this, nr_i, -100000, 100000);
		}
		for (int i = 0; i < nr_i; i++) {
			for (int j = 0; j < nbAttributs; j++) {
				rel(*this, values_max[j][i] == item[i] * values[j][i]);
				rel(*this, (item[i] == 0 || values_min[j][i] == values[j][i]) && (item[i] == 1 || values_min[j][i] == 100000));
			}
		}
		for (int j = 0; j < nbAttributs; j++) {
			max(*this, values_max[j], maxValue[j]);
			min(*this, values_min[j], minValue[j]);
		}
		//----------------CALCULATE MEAN---------------------//
		for (int j = 0; j < nbAttributs; j++)
			rel(*this, mean[j] == (minValue[j] + maxValue[j]) / 2);
		/** search **/
		branch(*this, item, INT_VAR_DEGREE_MAX, INT_VAL_MAX);
	}

	Skypattern(bool share, Skypattern& s) : Space(share,s), nr_i(s.nr_i), nr_t(s.nr_t) {
		item.update(*this, share, s.item);
		bitem.update(*this, share, s.bitem);
		bfitem.update(*this, share, s.bfitem);
		transaction.update(*this, share, s.transaction);
		transaction1.update(*this, share, s.transaction1);
		btransaction.update(*this, share, s.btransaction);
		btransaction1.update(*this, share, s.btransaction1);
		frequency.update(*this, share, s.frequency);
		frequency1.update(*this, share, s.frequency1);
		for (int j = 0; j < nbAttributs; j++)
			values[j].update(*this, share, s.values[j]);
		maxValue.update(*this, share, s.maxValue);
		minValue.update(*this, share, s.minValue);
		mean.update(*this, share, s.mean);
		growthrate.update(*this, share, s.growthrate);
		size.update(*this, share, s.size);
		area.update(*this, share, s.area);
	}	

	// CONSTRAIN FUNCTION
	virtual void constrain(const Space& _b) {		
		const Skypattern& b = static_cast<const Skypattern&>(_b);
		IntVarArgs measures(usedmeasures.size());
		measures[0] = frequency;		
		for (int j = 0; j < nbAttributs; j++)
			measures[j + 1] = mean[j];
		measures[nbAttributs+1] = growthrate;
		measures[nbAttributs+2] = area;
		int b_measures[usedmeasures.size()];
		b_measures[0] = b.frequency.val();
		for (int j = 0; j < nbAttributs; j++)
			b_measures[j + 1] = b.mean[j].max();
		b_measures[nbAttributs+1] = b.growthrate.max();
		b_measures[nbAttributs+2] = b.area.max();
		not_dominated_by(*this, measures, b_measures);
	}
	
	virtual Space* copy(bool share) { return new Skypattern(share, *this); }

	/// Load data from file
	const vector< vector<float> >& get_data(const string filename) {
		DataReader* reader = new DataReader();
		reader->read(filename);
		/**********/
		nr_i = reader->data[1].size();
		/*****LOAD VALUES*****/
		for (int i = 0; i < nbAttributs; i++) {
			values[i]   = IntVarArray(*this, nr_i, -100000, 100000);
			string name = filename.substr(0, filename.find_last_of('.')) + ".val" + convert(i);
			ifstream file(name.c_str());
			if (!file) {
				std::cerr << "\tError: file "<< name << " does not exist !" << std::endl;
				exit(1);
			}
			string line;
			int j = 0;
			while(getline(file, line)) {
				double val = 100 * (double)atof(line.c_str());
				values[i][j] = IntVar(*this, val, val);
				j += 1;
			}
			file.close();
		}
		/**********/
		return reader->data;
	}
	
	vector<int> get_extension(void) const {
		vector<int> _extension;
		for(int t = 0; t < nr_t; t++) {
			if(transaction[t].val())
				_extension.push_back((t + 1));
		}
		return _extension;
	}
	
	vector<int> get_measures(void) const {
		vector<int> _measures;
		_measures.push_back(frequency.val());
		for (int i = 0; i < nbAttributs; i++)
			_measures.push_back(mean[i].max());		
		_measures.push_back(growthrate.max());
		_measures.push_back(area.val());
		return _measures;
	}
	
	vector<int> get_pattern(void) const {
		vector<int> _pattern;
		for(int i = 0; i < nr_i; i++) {
			if(item[i].val())
				_pattern.push_back((i + 1));
		}
		return _pattern;
	}
	
	bool add_to_vector(Solution* sol) {
		for(vector<Solution*>::iterator it = candidates.begin() ; it != candidates.end();++it) {
			if((*it)->equals(sol))
				return false;
		}
		candidates.push_back(sol);
		return true;
	}
	
};

//
string timeToString(clock_t clock1, clock_t clock2) {
	double diffticks = clock2 - clock1;
	double diffms    = (diffticks * 1000) / (double) CLOCKS_PER_SEC;	
	int ms    = ((int)diffms) % 1000;
	int diffs = diffms / 1000;
	int s     = diffs % 60;
	int m     = (diffs % 3600) / 60;
	int h     = diffs / 3600;
	stringstream time;
	if(h > 0)
		time << h << "h";
	if(m > 0)
		time << m << "m";
	time << s << "s." << ms << "ms (" << diffms << "ms)";
	return time.str();
}

/// MAIN FUNCTION
int main(int argc, char* argv[]) {	
    bool isPrinted = false;
    bool isStats   = false;
	string filename = "";
    for(int i = 1; i < argc; i++) {
		if(strcmp(argv[i],"-data") == 0 || strcmp(argv[i],"-d") == 0)
			filename = argv[i + 1];
		else if(strcmp(argv[i],"-measures") == 0 || strcmp(argv[i],"-m") == 0) {
			string s = argv[i + 1];
			if((s.find("F") != string::npos) || (s.find("f") != string::npos))
				usedmeasures.push_back(true);
			else
				usedmeasures.push_back(false);
			if((s.find("0") != string::npos) || (s.find("n") != string::npos))
				usedmeasures.push_back(true);
			else
				usedmeasures.push_back(false);
			if((s.find("G") != string::npos) || (s.find("g") != string::npos))
				usedmeasures.push_back(true);
			else
				usedmeasures.push_back(false);
			if((s.find("A") != string::npos) || (s.find("a") != string::npos))
				usedmeasures.push_back(true);
			else
				usedmeasures.push_back(false);
		}
		else if(strcmp(argv[i],"-print") == 0 || strcmp(argv[i],"-p") == 0)
    		isPrinted = true;
		else if(strcmp(argv[i],"-extension") == 0 || strcmp(argv[i],"-e") == 0)
    		hasExtension = true;
    	else if(strcmp(argv[i],"-stats") == 0 || strcmp(argv[i],"-s") == 0)
    		isStats   = true;
		else if(strcmp(argv[i],"-help") == 0 || strcmp(argv[i],"-h") == 0 || strcmp(argv[i],"--help") == 0) {
		    cout << "Usage: ./CP+SKY [-options] [args]" << endl;
			cout << "where there are two types of options:" << endl;
			cout << "1) type: [option] [arg] include:" << endl;
			cout << "-data     | -d		datafile to use" << endl;
			cout << "-measures | -m 		measures to use for the skyline operator" << endl;
			cout << "			(f:frequency, 0:m_0, ..., 9:m_9, g:growthrate and a:area)" << endl;
			cout << "-delta 			delta value for delta-skypatterns" << endl;
			cout << "2) type: [option] include:" << endl;
			cout << "-print | -p    		print solutions" << endl;
			cout << "-stats | -s		print statistics" << endl;
			cout << "-edge     		edge-skypatterns" << endl;
			cout << "-help | --help | -h	show help options" << endl;
			cout << endl;
			cout << "example: ./CP+SKY+PROG -data data/zoo.txt -m fa" << endl << endl;
			exit(1);
		}
	}
	for(vector<bool>::iterator it = usedmeasures.begin() ; it != usedmeasures.end(); ++it) {
		if(*it)
			nMeasuresUsed += 1;
	}
	Skypattern* model = new Skypattern(filename);
	Gecode::Search::Options o;
	o.c_d = 0;
	o.a_d = 0;
	BAB<Skypattern> solver(model, o);
	delete model;
	clock_t begin = clock();
	while (Skypattern* s = solver.next()) {		
		//nbCandidates += 1;
		candidates.push_back(new Solution(s->get_pattern(), s->get_measures(), s->get_extension()));
		delete s;
	}

	//SKY OPERATOR
	vector<Solution*> skypatterns;
	for(vector<Solution*>::iterator it1 = candidates.begin() ; it1 != candidates.end();) {
		bool isSkypattern = true;
		for(vector<Solution*>::iterator it2 = it1 + 1; it2 != candidates.end(); ++it2) {
			int nsup   = 0;
			int nequ   = 0;
			for(unsigned int k = 0; k < usedmeasures.size(); k++) {
				if(usedmeasures.at(k)) {
					if((*it2)->getMeasure(k) > (*it1)->getMeasure(k))
						nsup += 1;
					else if((*it2)->getMeasure(k) == (*it1)->getMeasure(k))
						nequ += 1;
				}
			}
			if(nsup >= 1 && nsup + nequ == nMeasuresUsed)
				isSkypattern = false;
			if(nMeasuresUsed == 1 || nequ == nMeasuresUsed) {
				(*it1)->set_indistinct();
				(*it2)->set_indistinct();
			}
			if(!isSkypattern)
				break;
		}
		if(isSkypattern) {
			skypatterns.push_back(*it1);
			++it1;
		}
		else
			it1 = candidates.erase(it1);
	}
		
	// PRINTING LIST OF SKYPATTERNS
	if(isPrinted) {
		cout << "Pattern | ";
		if(usedmeasures.at(0))
			cout << " Frequency ";
		if(usedmeasures.at(1))
			cout << " Mean ";
		if(usedmeasures.at(2))
			cout << " Growth-rate";
		if(usedmeasures.at(3))
			cout << " Area";
		if(hasExtension)
			cout << " | Extension";
		cout << endl;
		for(vector<Solution*>::iterator it = skypatterns.begin() ; it != skypatterns.end(); ++it)
			(*it)->print(usedmeasures, hasExtension);
	}
	clock_t endResearch = clock();
	// PRINTING STATS
	if(isStats) {
		//cout << "# Candidates        = " << convert(nbCandidates) << endl;
		cout << "# Skypatterns       = " << skypatterns.size()    << endl;
		cout << "Time                = " << timeToString(begin,endResearch) << endl;
	}
}
