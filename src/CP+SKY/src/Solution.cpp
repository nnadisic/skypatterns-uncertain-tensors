#include <vector>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm> 

using namespace std;

string convert(long n) {
	ostringstream oss;
    oss << n;
    return oss.str();
}

class Solution {
private:
	vector<int> pattern;
	vector<int> measures;
	vector<int> extension;
	bool indistinct;
public:
	/**********/
	Solution(Solution* _solution) {
		vector<int> _pattern = _solution->getPattern();
		vector<int> _measures = _solution->getMeasures();
		vector<int> _extension = _solution->getExtension();
		for(unsigned int i = 0; i < _pattern.size(); i++)
			pattern.push_back(_pattern.at(i));
		for(unsigned int i = 0; i < _measures.size(); i++)
			measures.push_back(_measures.at(i));
		for(unsigned int i = 0; i < _extension.size(); i++)
			extension.push_back(_extension.at(i));
		indistinct = false;
	}

	Solution(vector<int> _pattern, vector<int> _measures, vector<int> _extension) {
		for(unsigned int i = 0; i < _pattern.size(); i++)
			pattern.push_back(_pattern.at(i));
		for(unsigned int i = 0; i < _measures.size(); i++)
			measures.push_back(_measures.at(i));
		for(unsigned int i = 0; i < _extension.size(); i++)
			extension.push_back(_extension.at(i));
		indistinct = false;
	}
	
	Solution(vector<int> _pattern, vector<int> _measures) {
		for(unsigned int i = 0; i < _pattern.size(); i++)
			pattern.push_back(_pattern.at(i));
		for(unsigned int i = 0; i < _measures.size(); i++)
			measures.push_back(_measures.at(i));
		/*for(unsigned int i = 0; i < _extension.size(); i++)
			extension.push_back(_extension.at(i));*/
		indistinct = false;
	}

	~Solution() { }	

	/**********/
	vector<int> getPattern() { return pattern; }

	/**********/
	vector<int> getExtension() { return extension; }
	
	/**********/
	int getItem(int i) { return pattern.at(i); }

	/**********/
	vector<int> getMeasures() { return measures; }
	
	/**********/
	int getMeasure(int i) { return measures.at(i); }
	
	/**********/
	unsigned int getSize() { return pattern.size(); }

	bool is_indistinct() { return indistinct; }

	/**********/
	bool dominates(Solution _solution) {
		bool allgreaterorequalorequal = true;
		for(unsigned int i = 0; i < measures.size(); i++) {
			if(measures.at(i) < _solution.getMeasure(i)) {
				allgreaterorequalorequal = false;
				break;
			}
		}
		bool existgreater = false;
		for(unsigned int i = 0; i < measures.size(); i++) {
			if(measures.at(i) > _solution.getMeasure(i)) {
				allgreaterorequalorequal = true;
				break;
			}
		}
		return allgreaterorequalorequal && existgreater;
	}
	
	/**********/
	bool contains(Solution _solution) {
		vector<int> pattern2 = _solution.getPattern();		
		for(vector<int>::iterator it1 = pattern.begin() ; it1 != pattern.end(); ++it1) {
			if(find(pattern2.begin(), pattern2.end(), (*it1)) != pattern2.end()) {
			}
			else
				return false;
		}
		return true;
	}
	
	/**********/
	bool equals(Solution _solution) {
		if(pattern.size() == _solution.getSize()) {
			vector<int> pattern2 = _solution.getPattern();
			for(unsigned int i = 0; i < pattern.size(); i++) {
				if(pattern.at(i) != pattern2.at(i) )
					return false;
			}
			return true;
		}
		else
			return false;
	}	

	/**********/
	void print(vector<bool> usedmeasures, bool hasExtension) {
		for(unsigned int i = 0; i < pattern.size(); i++) {
			printf("%d", pattern.at(i));
			printf(" ");
		}
		printf("| ");
		for(unsigned int m = 0; m < measures.size(); m++) {
			if(usedmeasures.at(m)) {
				printf("%d", measures.at(m));
				printf(" ");
			}
		}
		if(hasExtension) {
			printf("| ");
			for(unsigned int i = 0; i < extension.size(); i++) {
				printf("%d", extension.at(i));
				printf(" ");
			}
		}
		printf("\n");
	}
	
	/**********/
	void set_indistinct( ) { indistinct = true; }
	
	/**********/
	string patternToString() {
		string s = "";
		for(unsigned int i = 0; i < pattern.size(); i++) {
			ostringstream os;
			os << pattern.at(i);
			s += os.str();
			if(i < pattern.size() - 1)
				s += " ";
		}
		return s;
	}
};
