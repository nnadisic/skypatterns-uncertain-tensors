#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include "Solution.cpp"

class Solutions {
private:
	vector<Solution*> solutions;
	vector<string> stats;

	/**********/
	void writeTxt(string outputnameTxt) {
		ofstream fichierTxt(outputnameTxt.c_str(), ios::out | ios::trunc);
		printf("Writing... ");
		printf("%s", outputnameTxt.c_str());		
		vector<Solution*>::iterator it;
		int i = 1;
		for (it = solutions.begin(); it!=solutions.end(); ++it) {			
			vector<int> measures = (*it)->getMeasures();
			vector<int> pattern  = (*it)->getPattern();
			for(unsigned int j = 0; j < pattern.size(); j++)
				fichierTxt << pattern.at(j) << " ";			
			fichierTxt << ": ";			
			for(unsigned int j = 0; j < measures.size(); j++) {
				if(j == measures.size() - 2) {
					if(measures.at(j) == 101)
						fichierTxt << "inf";
					else
						fichierTxt << measures.at(j);
				}
				else
					fichierTxt << measures.at(j);
				if(j < measures.size() - 1)
					fichierTxt << " : ";
			}			
			fichierTxt << endl;
			i += 1;
		}
		/*fichierTxt << "--------------" << endl;
		fichierTxt << "Time          = " << stats.at(0) << endl;
		fichierTxt << "# Solutions   = " << solutions.size() << endl;
		fichierTxt << "# Changes     = " << stats.at(2) << endl;
		fichierTxt << "# Failure     = " << stats.at(3) << endl;
		fichierTxt << "# Node        = " << stats.at(4) << endl;
		fichierTxt << "# Propagation = " << stats.at(5) << endl;
		fichierTxt << "Memory        = " << stats.at(6) << endl;
		printf("	done\n");*/
		fichierTxt.close();
	}	

	/**********/
	string normalise(int n) {
		string s = "";
		if (n < 10)
			s = "  ";
		else if (n < 100)
			s = " ";
		ostringstream oss;
		oss << s << n;
		return oss.str();
	}

	/**********/
	bool compareTo(Solution* _solution1, Solution* _solution2) { return !_solution1->dominates(_solution2); }
	
public:
	/**********/
	Solutions() { }
	
	~Solutions() { }

	/**********/
	int getSize() { return solutions.size(); }
	
	/**********/
	Solution* getPattern(int i) { return solutions.at(i); }
	
	/**********/
	void add(Solution* _solution) {		
		vector<Solution*>::iterator it;
		for (it = solutions.begin(); it!=solutions.end(); ++it) {
			if(compareTo(*it, _solution))
				break;
		}
		solutions.insert(it, _solution);
	}
	
	/**********/
	void apply() {
		
	}
	
	/**********/
	void removeMinimum() { solutions.pop_back(); }

	/**********/
	Solution* minimum() { return solutions.back(); }
	
	/**********/
	void print() {
		printf("----------\n");
		int i = 1;
		vector<Solution*>::iterator it;
		for (it = solutions.begin(); it!=solutions.end(); ++it) {			
			vector<int> measures = (*it)->getMeasures();			
			vector<int> pattern  = (*it)->getPattern();
			printf("%s", normalise(i).c_str());
			printf(" | ");			
			for(unsigned int j = 0; j < measures.size(); j++) {
				printf("%s", normalise(measures.at(j)).c_str());
				printf(" ");
			}
			printf("| ");			
			for(unsigned int j = 0; j < pattern.size(); j++) {
				printf("%d", pattern.at(j));
				printf(" ");
			}
			printf("\n");
			i += 1;
		}
		printf("----------\n");
		/*printf("Time          = %s\n", stats.at(0));
		printf("# Solutions   = %d\n", solutions.size());
		printf("# Changes     = %d\n", stats.at(2));
		printf("# Failure     = %d\n", stats.at(3));
		printf("# Node        = %d\n", stats.at(4));
		printf("# Propagation = %d\n", stats.at(5));
		printf("Memory        = %d\n", stats.at(6));*/
	}

	/**********/
	void write(string filename, vector<string> _stats) {
		stats = _stats;
		writeTxt(filename + ".txt");		
		printf("\n");
	}
};
