#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <string>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

class DataReader {
private:
	vector<map<string,float> > dictionary;
	bool is_space (char pos) {
		return pos == ' ' || pos == '\n' || pos == '\t' || pos == ',' || pos == '\r';
	}
	
	char *read_spaces(char *pos) {
		while (is_space(*pos))
			if(*pos == 0)
				return pos;
			else
				++pos;
		return pos;
	}

	char *get_attribute(char *&pos) {
		static char attribute[1000];
		char *pos2 = attribute;
		pos = read_spaces(pos);
		if (*pos == 0)
			return NULL;
		while(!is_space(*pos) && *pos != 0) {
			*pos2 = *pos;
			++pos2;
			++pos;
		}
		*pos2 = 0;
		return attribute;
	}

public:
	vector<vector<float> > data;
	DataReader() { }
	~DataReader() { }
	void add(vector<string> row) {
		vector<float> newrow;
		unsigned int pos = 0;
		for (vector<string>::iterator i = row.begin (); i != row.end (); ++i, ++pos ) {
			if (pos >= dictionary.size())
				dictionary.resize(pos + 1);
			if ((*i)[0] >= '0' && (*i)[0] <= '9')
				newrow.push_back(atof(i->c_str()));
			else {
				int num = dictionary[pos].size() + 1; // 0 is reserved for nulls in the data
				pair<map<string,float>::iterator,bool > p = 
					dictionary[pos].insert(make_pair(i->c_str(), num));
				newrow.push_back(p.first->second);
			}
		}
		data.push_back(newrow);
	}

	void read(string filename) {
		ifstream input(filename.c_str());
		if (!input) {
			std::cerr << "\tError: file " << filename << " does not exist !" << std::endl;
			exit(1);
		}
		//cout << filename << endl;		
		const int longueur = 10000;		
		char line[longueur];
		input.getline(line, longueur);		
		while(!input.eof()) {			
			char *pos = line;
			pos = read_spaces(pos);			
			if(*pos != '@' && *pos != '%' && *pos != 0) {
				vector<string> row;				
				while (*pos != 0) {
					char *p = get_attribute(pos);
					if(p)
						row.push_back(p);
				}				
				add(row);
			}			
			input.getline(line, longueur);
		}
	}
};
