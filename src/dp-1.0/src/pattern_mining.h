/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#ifndef dp_pattern_mining_h
#define dp_pattern_mining_h

#include "common.h"
#include "reader_eliz.h"

typedef vector<vector<bool> > TDB;

void load_tdb( const string& filename, TDB& tdb, int& nr_i, int& nr_t) {
    Reader_Eliz<bool> data;
    data.read(filename);
    tdb = data.data;
    nr_i = (int)tdb[0].size();
    nr_t = (int)tdb.size();
}

void load_tdb_class( const string& filename,
                    TDB& tdb0,
                    TDB& tdb1,
                    int& nr_i, int& nr_t0, int& nr_t1) {
    Reader_Eliz<bool> data;
    data.read(filename);
    tdb0 = data.classSplit(0).data;
    tdb1 = data.classSplit(1).data;
    nr_i = (int)tdb0[0].size();
    nr_t0 = (int)tdb0.size();
    nr_t1 = (int)tdb1.size();
}


#define PRINT_USAGE cerr << "Usage: " << argv[0] << " " << (argc > 1 ? argv[1] : "<setting>") << " <datafile> [<options>]" << endl;
#define PRINT_HELPLINE cerr << " See " << argv[0] << " help for more information." << endl;

#define CHECK_ARGS(N) if (argc < N+1) { PRINT_USAGE; PRINT_HELPLINE; exit(1); }

#define LOAD_DATA CHECK_ARGS(2); TDB tdb; int nr_i; int nr_t; load_tdb(argv[2], tdb, nr_i, nr_t);

#define LOAD_DATA_CLASS CHECK_ARGS(2); TDB tdb0; TDB tdb1; int nr_i; int nr_t0; int nr_t1; \
load_tdb_class(argv[2], tdb0, tdb1, nr_i, nr_t0, nr_t1);

#define LOAD_FREQ float freq = (float)process_int(argc,argv,"-f",10); if (freq > 0) freq /= 100.0;

#define LOAD_SIZE int size = (int)process_int(argc,argv,"-s",0);

#define PROBLEM_FREQ \
RootOperator() \
<< DefineOperator("items", 0, 1, nr_i) \
<< DefineOperator("cover", 0, 1, nr_t) \
<< CoverConstraint("items", "cover", tdb) \
<< FrequencyConstraint("items", "cover", tdb, frequency)




#include <gecode/minimodel.hh>
#include "DPSpace.h"


FunctionConstraint CoverConstraint(const string& item_alias,const string& cover_alias, vector<vector<bool > >& tdb) {
	
	auto func = [item_alias, cover_alias, tdb] (DPSpace& space) {
		
		BoolVarArgs items = space.getBVA(item_alias);
		BoolVarArgs transactions = space.getBVA(cover_alias);
		
		int nr_i = items.size();
		int nr_t = transactions.size();
		
        BoolVarArgs none(0);
        
        for (int t=0; t!=nr_t; t++) {
            // count row
            int row_sum = nr_i;
            for (int i=0; i!=nr_i; i++)
                row_sum -= tdb[t][i];
            
            BoolVarArgs row(row_sum);
            // make row
            for (int i=0; i!=nr_i; i++) {
                if (1-tdb[t][i])
                    row[--row_sum] = items[i];
            }
            
            // coverage: the trans its complement has no supported items
            // !row_compl[1] AND !row_compl[2] ...  <=> t_k
            clause(space, BOT_AND, none, row, transactions[t]);
        }
	
	};
	
	return FunctionConstraint(func);
}

FunctionConstraint FrequencyConstraint(const string& item_alias,const string& cover_alias, vector<vector<bool > >& tdb, float frequency) {

	auto func = [item_alias, cover_alias, tdb, frequency] (DPSpace& space) {

		BoolVarArgs items = space.getBVA(item_alias);
		BoolVarArgs transactions = space.getBVA(cover_alias);
		
		int nr_i = items.size();
		int nr_t = transactions.size();
		
		int freq;
		if (frequency < 0) {
			freq = (int)-frequency;
		} else {
			freq = (int)(frequency*nr_t+0.5);
		}
		IntArgs col(nr_t);
		for (int i=0; i!=nr_i; i++) {
			// make col
			for (int t=0; t!=nr_t; t++)
				col[t] = tdb[t][i];
			
			rel(space, items[i] >> (sum(col,transactions) >= freq));
		}
	};
	
	return FunctionConstraint(func);
    
};



#endif
