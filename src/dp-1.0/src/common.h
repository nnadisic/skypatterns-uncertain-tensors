/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#ifndef dp_common_h
#define dp_common_h

#include "DefineOperator.h"
#include "ConstraintOperator.h"
#include "OutputOperator.h"
#include "DominanceOperator.h"
#include "OperatorChain.h"
#include "SearchOrderOperator.h"
#include "FunctionConstraint.h"

#include "cmdarg.h"

typedef OperatorChain (*SettingFunction)(int, char**);


#endif
