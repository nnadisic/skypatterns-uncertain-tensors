/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "common.h"
#include "pattern_mining.h"

#include <gecode/int.hh>

OutputOperator no_output() {
    return OutputOperator(cout);
}


OutputOperator default_output() {
    return OutputOperator(cout,
                          OutSpec(OutSpecType::ARR2SET, "items") &
                          OutSpec(OutSpecType::CONSTANT, "| ") &
                          OutSpec(OutSpecType::SETSIZE, "cover"));
    
}

OperatorChain subgroup_equivalent(int argc, char* argv[]) {
    LOAD_DATA_CLASS;
    LOAD_FREQ;
    
    return OperatorChain()
    << DefineOperator("items", 0, 1, nr_i)
    << DefineOperator("cover-", 0, 1, nr_t0)
    << DefineOperator("cover+", 0, 1, nr_t1)
    << CoverConstraint("items", "cover-", tdb0)
    << CoverConstraint("items", "cover+", tdb1)
    << FrequencyConstraint("items", "cover+", tdb1, freq)
    << DominanceOperator( Preorder("cover+",GEQ) & Preorder("cover-",LEQ) )
    << OutputOperator(cout,
            OutSpec(OutSpecType::ARR2SET, "items") &
            OutSpec(OutSpecType::CONSTANT, "| ") &
            OutSpec(OutSpecType::SETSIZE, "cover+") &
            OutSpec(OutSpecType::CONSTANT, " ") &
            OutSpec(OutSpecType::SETSIZE, "cover-"));

}

OperatorChain subgroup_hack(int argc, char* argv[]) {
    LOAD_DATA_CLASS;
    LOAD_FREQ;
    
    return OperatorChain()
    << DefineOperator("items", 0, 1, nr_i)
    << DefineOperator("cover-", 0, 1, nr_t0)
    << DefineOperator("cover+", 0, 1, nr_t1)
    << SearchOrderOperator( Preorder("items", GEQ ) )
    << CoverConstraint("items", "cover-", tdb0)
    << CoverConstraint("items", "cover+", tdb1)
    << FrequencyConstraint("items", "cover+", tdb1, freq)
    << DominanceOperator( Preorder("cover+",GEQ) & Preorder("cover-",LEQ), false )
    << OutputOperator(cout,
                      OutSpec(OutSpecType::ARR2SET, "items") &
                      OutSpec(OutSpecType::CONSTANT, "| ") &
                      OutSpec(OutSpecType::SETSIZE, "cover+") &
                      OutSpec(OutSpecType::CONSTANT, " ") &
                      OutSpec(OutSpecType::SETSIZE, "cover-"));
    
}


OperatorChain subgroup_unique(int argc, char* argv[]) {
    LOAD_DATA_CLASS;
    LOAD_FREQ;
    
    return OperatorChain()
    << DefineOperator("items", 0, 1, nr_i)
    << DefineOperator("cover-", 0, 1, nr_t0)
    << DefineOperator("cover+", 0, 1, nr_t1)
    << CoverConstraint("items", "cover-", tdb0)
    << CoverConstraint("items", "cover+", tdb1)
    << FrequencyConstraint("items", "cover+", tdb1, freq)
    << DominanceOperator( Preorder("cover+",GEQ) & Preorder("cover-",LEQ) )
    << DominanceOperator( Preorder("cover+",EQ) & Preorder("cover-",EQ) & Preorder("items", GEQ) )
    << OutputOperator(cout,
                      OutSpec(OutSpecType::ARR2SET, "items") &
                      OutSpec(OutSpecType::CONSTANT, "| ") &
                      OutSpec(OutSpecType::SETSIZE, "cover+") &
                      OutSpec(OutSpecType::CONSTANT, " ") &
                      OutSpec(OutSpecType::SETSIZE, "cover-"));
    
}

OperatorChain subgroup_closed(int argc, char* argv[]) {
    LOAD_DATA_CLASS;
    LOAD_FREQ;
    
    return OperatorChain()
    << DefineOperator("items", 0, 1, nr_i)
    << DefineOperator("cover-", 0, 1, nr_t0)
    << DefineOperator("cover+", 0, 1, nr_t1)
    << CoverConstraint("items", "cover-", tdb0)
    << CoverConstraint("items", "cover+", tdb1)
    << FrequencyConstraint("items", "cover+", tdb1, freq)
    << DominanceOperator( Preorder("items", GEQ) & Preorder("cover+",EQ) )
    << DominanceOperator( Preorder("cover+", GEQ) & Preorder("cover-",LEQ) )
    << OutputOperator(cout,
                      OutSpec(OutSpecType::ARR2SET, "items") &
                      OutSpec(OutSpecType::CONSTANT, "| ") &
                      OutSpec(OutSpecType::SETSIZE, "cover+") &
                      OutSpec(OutSpecType::CONSTANT, " ") &
                      OutSpec(OutSpecType::SETSIZE, "cover-"));
    
}


OperatorChain frequent_raw(int argc, char* argv[]) {
    LOAD_DATA;
    LOAD_FREQ;
    
    return OperatorChain()
    << DefineOperator("items", 0, 1, nr_i)
    << DefineOperator("cover", 0, 1, nr_t)
    << CoverConstraint("items", "cover", tdb)
    << FrequencyConstraint("items", "cover", tdb, freq);
}

OperatorChain frequent(int argc, char* argv[]) {
    return frequent_raw(argc, argv)
    << default_output();
}

OperatorChain closed(int argc, char* argv[]) {
    return frequent_raw(argc,argv)
    << DominanceOperator( Preorder("items", GEQ) & Preorder( "cover", EQ))
	<< default_output();
}

OperatorChain maximal(int argc, char* argv[]) {
    return frequent_raw(argc, argv)
    << DominanceOperator( Preorder("items", GEQ) )
    << default_output();
}

OperatorChain free(int argc, char* argv[]) {
    return frequent_raw(argc,argv)
    << DominanceOperator( Preorder("items", LEQ) & Preorder( "cover", EQ))
    << default_output();
}

OperatorChain skyline(int argc, char* argv[]) {
    LOAD_DATA;
    LOAD_FREQ;
    
    auto freqdef = [](DPSpace& s) { s.getIV("freq") = expr(s, sum( s.getBVA("cover") )); };
    auto areadef = [](DPSpace& s) { s.getIV("area") = expr(s, s.getIV("freq") * sum( s.getBVA("items") )); };
    
    return frequent_raw(argc, argv)
    << DefineOperator("freq",0, nr_t)
    << DefineOperator("area",0, nr_i*nr_t)
    << FunctionConstraint(freqdef)
    << FunctionConstraint(areadef)
    << DominanceOperator( Preorder("freq", GEQ) & Preorder("area",GEQ) )
    << OutputOperator(cout,
         OutSpec(OutSpecType::ARR2SET, "items") &
         OutSpec(OutSpecType::CONSTANT, "| ") &
         OutSpec(OutSpecType::INTVALUE, "freq") &
         OutSpec(OutSpecType::CONSTANT, " ") &
         OutSpec(OutSpecType::INTVALUE, "area"));

}

OperatorChain skyline_nonoptimal(int argc, char* argv[]) {
    LOAD_DATA;
    LOAD_FREQ;
    
	auto freqdef = [](DPSpace& s) { s.getIV("freq") = expr(s, sum( s.getBVA("cover") )); };
	auto areadef = [](DPSpace& s) { s.getIV("area") = expr(s, s.getIV("freq") * sum( s.getBVA("items") )); };
    
    return frequent_raw(argc, argv)
	<< SearchOrderOperator( Preorder("items", LEQ ) )
    << DefineOperator("freq",0, nr_t)
    << DefineOperator("area",0, nr_i*nr_t)
    << FunctionConstraint(freqdef)
    << FunctionConstraint(areadef)
    << DominanceOperator( Preorder("freq", GEQ) & Preorder("area",GEQ) )
    << OutputOperator(cout,
					  OutSpec(OutSpecType::ARR2SET, "items") &
					  OutSpec(OutSpecType::CONSTANT, "| ") &
					  OutSpec(OutSpecType::INTVALUE, "freq") &
					  OutSpec(OutSpecType::CONSTANT, " ") &
					  OutSpec(OutSpecType::INTVALUE, "area"));
	
}



OperatorChain closed_size(int argc, char* argv[]) {
    LOAD_SIZE;
    
	auto sizec = [size](DPSpace& s) { linear(s, s.getBVA("items"), size < 0 ? IRT_LQ : IRT_GQ, abs(size) ) ; };

    return frequent_raw(argc,argv)
    << DominanceOperator( Preorder("items", GEQ) & Preorder( "cover", EQ))
    << FunctionConstraint(sizec)
    << default_output();

}

OperatorChain size_closed(int argc, char* argv[]) {
    LOAD_SIZE;
    
	auto sizec = [size](DPSpace& s) { linear(s, s.getBVA("items"), size < 0 ? IRT_LQ : IRT_GQ, abs(size) ) ; };

    return frequent_raw(argc,argv)
    << FunctionConstraint(sizec)
    << DominanceOperator( Preorder("items", GEQ) & Preorder( "cover", EQ))
    << default_output();

}

OperatorChain maximal_smallest(int argc, char* argv[]) {
    LOAD_DATA;
    LOAD_FREQ;

    auto sizedef = [](DPSpace& s) { s.getIV("size") = expr(s, sum( s.getBVA("items") )); };
    
    
    return OperatorChain()
    << DefineOperator("items", 0, 1, nr_i)
    << DefineOperator("cover", 0, 1, nr_t)
    << CoverConstraint("items", "cover", tdb)
    << FrequencyConstraint("items", "cover", tdb, freq)
    << DefineOperator("size", 0, nr_i, 1)
    << FunctionConstraint(sizedef)
    << DominanceOperator( Preorder("items", GEQ) )
    << DominanceOperator( Preorder("size", LEQ) )
    << default_output();
}

OperatorChain maximal_largest(int argc, char* argv[]) {
    LOAD_DATA;
    LOAD_FREQ;

    auto sizedef = [](DPSpace& s) { s.getIV("size") = expr(s, sum( s.getBVA("items") )); };
    
    return OperatorChain()
    << DefineOperator("items", 0, 1, nr_i)
    << DefineOperator("cover", 0, 1, nr_t)
    << CoverConstraint("items", "cover", tdb)
    << FrequencyConstraint("items", "cover", tdb, freq)
    << DefineOperator("size", 0, nr_i, 1)
    << FunctionConstraint(sizedef)
    << DominanceOperator( Preorder("items", GEQ) )
    << DominanceOperator( Preorder("size", GEQ) )
    << default_output();
}


map< string, SettingFunction > settings;

OperatorChain help(int argc, char* argv[]) {
	argc = 1;
	PRINT_USAGE;
	cout << endl;
	cout << "Available settings:" << endl;
	for (map<string, SettingFunction>::iterator it = settings.begin(); it != settings.end(); ++it) {
		cout << "  * " << it->first << endl;
	}
	
	cout << endl;
	cout << "Available options:" << endl;
	cout << "  -f <freq>\t e.g. 10 (10%), -42 (42)" << endl;
	cout << "  -s <size>\t e.g. 5 (>=5), -7 (<=7)" << endl;
	
	return OperatorChain();
}


int main(int argc, char* argv[]) {
	CHECK_ARGS(1);
	
	settings["help"] = help;
    settings["frequent"] = frequent;
    settings["closed"] = closed;
    settings["maximal"] = maximal;
    settings["free"] = free;
    settings["subgroup-"] = subgroup_hack;
    settings["subgroup+"] = subgroup_equivalent;
    settings["subgroup"] = subgroup_unique;
    settings["closed+subgroup"] = subgroup_closed;
    settings["skyline"] = skyline;
    settings["skyline+"] = skyline_nonoptimal;
	
    settings["maximal/smallest"] = maximal_smallest;
    settings["maximal/largest"] = maximal_largest;
    
    settings["closed+size"] = closed_size;
    settings["size+closed"] = size_closed;
    
    map< string, SettingFunction>::iterator setting = settings.find(argv[1]);
    if (setting == settings.end()) {
        cerr << "Unknown setting '" << argv[1] << "'" << endl;
    } else {
        setting->second(argc, argv).solve();
    }
    
}
