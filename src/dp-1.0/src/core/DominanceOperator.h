/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#ifndef dp_DominanceOperator_h
#define dp_DominanceOperator_h

#include "SearchOrderOperator.h"


/**
 Imposes constraints on pairs of examples.
 */
class DominanceOperator : public SearchOrderOperator {


public:
    
	vector<const DPSpace*> solutions;
    size_t next_solution;
	
	
private:
    bool optimal;			// operator operates using optimal order
	bool equivalent;		// operator enumerates equivalent solutions
    
	DPSpace* check_space;	// space for checking successor constraints and post-processing
	
	/**
	 Post a DominancePropagator constraint
	 */
    virtual void postConstraint(DPSpace& space);
    
public:

	/**
	 Create a dominance operator based on a single variable.
	 */
    DominanceOperator(const Preorder& varref, bool equivalent=true);

	/**
	 Create a dominance operator based on a conjunction of variables.
	 */
    DominanceOperator(const vector< Preorder >& variables, bool equivalent=true);
	
	// override from Operator
    virtual DominanceOperator* clone() const;
    
	/**
	 Create a clone.
	 
	 - reset => if true, do not copy list of solutions
	 */
    virtual DominanceOperator* clone(bool reset) const;
	
	/** 
	 Called by DPSpace when a new equivalent set of solutions is started.
	 */
    void notifyNew();

	// override from SearchOrderOperator
	virtual bool applySearchOrder(DPSpace& space);
	
	/**
	 Successfully posted optimal search order.
	 Solutions are generated in optimal order.
	 
	 */
	virtual bool isOptimal() const;
	
	// override from Operator
	virtual DPSpace& initialize(int bvCount, int ivCount, bool skipConstraints);
	
	/**
	 Check the solution in the check_space.
	 */
	virtual bool checkSolution(const DPSpace& solution);
	
	// override from Operator
	virtual void solve(DPSpace& root);
	
	// override from Operator
	virtual void addSolution(const DPSpace& solution);
	
	/**
	 Constrain the given search space such that no solutions are generated that are dominated by the given one.
	 
	 - home => space to constrain
	 - sol => dominating solution
	 */
	virtual void constrain(DPSpace& home, const DPSpace& sol);
	
	// override from Operator
	virtual void finalize();
    
};

/**
 Propagator for the dominance constraint.
 */
class DominancePropagator : public Propagator {
	
private:
    
    ViewArray<Int::BoolView> bv;
	ViewArray<Int::IntView> iv;
    
	DominanceOperator& parent;
	size_t last_update;
	
public:
	
	
	DominancePropagator(Space& home, DominanceOperator& par, ViewArray<Int::BoolView>& bv_, ViewArray<Int::IntView>& iv_);
	
	DominancePropagator(Space& home, bool share, DominancePropagator& p);
        
    virtual size_t dispose(Space& home);
    
    virtual Propagator* copy(Space& home, bool share);
    
    static void post(Space& home, DominanceOperator& parent, const BoolVarArgs& bv, const IntVarArgs& iv);
    
    virtual ExecStatus propagate(Space& home, const ModEventDelta& m);
    
    virtual void constrain(Space& home, const Space& solution);
    
    virtual PropCost cost(const Space& home, const ModEventDelta& med) const;
    
};

#endif
