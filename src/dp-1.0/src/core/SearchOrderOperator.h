/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#ifndef dp_SearchOrderOperator_h
#define dp_SearchOrderOperator_h

#include "Operator.h"

enum CompareOrder { LEQ, GEQ, EQ };


typedef pair<string, CompareOrder> Preorder;

inline vector< Preorder > operator&(vector<Preorder> one, const Preorder& two) {
    one.push_back(two);
    return one;
}


inline vector< Preorder > operator&(const Preorder& one, const Preorder& two) {
    vector< Preorder > result;
    result.push_back(one);
    result.push_back(two);
    return result;
}


/**
 Explicitly sets the search order.
*/
class SearchOrderOperator : public Operator {
    
protected:
    
	vector< Preorder > variables; // variables defining the search order

public:
	
    SearchOrderOperator(const Preorder& varref);
	
    SearchOrderOperator(const vector< Preorder >& variables);
	
    virtual SearchOrderOperator* clone() const;
    
	// override from Operator
	virtual bool canApplySearchOrder() const;
		
	// override from Operator
	virtual void addSolution(const DPSpace& solution);
	
	// override from Operator
	virtual DPSpace& initialize(int bvCount, int ivCount, bool skipConstraints);
	
	/**
	 Apply the optimal search order.
	 
	 => return false if this is not possible
	 */
	virtual bool applySearchOrder(DPSpace& space);
	
    
};

#endif
