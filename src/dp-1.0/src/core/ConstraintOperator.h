/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#ifndef dp_ConstraintOperator_h
#define dp_ConstraintOperator_h

#include "Operator.h"

#include <gecode/minimodel.hh>

using namespace Gecode;

/**
 Base class for selection operator (i.e. placing a constraint on indiviual solutions.
 */
class ConstraintOperator : public Operator {

protected:
	
	/**
	 Posts the constraint on the given search space.
	 */
    virtual void postConstraint(DPSpace& space) = 0;
    
    
public:
    
	// override from Operator
    virtual DPSpace& initialize(int bvCount, int ivCount, bool skipConstraints);
    
};

#endif
