/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "DominanceOperator.h"

void DominanceOperator::postConstraint(DPSpace& space) {
    DominancePropagator::post(space, *this, space.bv, space.iv);
}

DominanceOperator::DominanceOperator(const Preorder& varref, bool equivalent) :
	SearchOrderOperator(varref) ,
	next_solution(0),
	optimal(true),
	equivalent(equivalent),
	check_space(NULL) {
	}
	
DominanceOperator::DominanceOperator(const vector< Preorder >& variables, bool equivalent) :
	SearchOrderOperator(variables),
	next_solution(0),
	optimal(true),
	equivalent(equivalent),
	check_space(NULL) {}

DominanceOperator* DominanceOperator::clone() const {
	return clone(false);
}
    
DominanceOperator* DominanceOperator::clone(bool reset) const {
	if (reset) {
		DominanceOperator* c = new DominanceOperator(variables, equivalent);
		c->check_space = check_space;
		c->setInput(getInput());
		return c;
	} else {
		return new DominanceOperator(*this);
	}
}
	
void DominanceOperator::notifyNew() {
	next_solution = solutions.size();
}

bool DominanceOperator::applySearchOrder(DPSpace& space) {
	if (!isOptimal()) return false;
	
	if (!SearchOrderOperator::applySearchOrder(space)) return false;
	
	space.optimalSearch = this;
	
	return true;
}
	
bool DominanceOperator::isOptimal() const {
	return optimal;
}
	
DPSpace& DominanceOperator::initialize(int bvCount, int ivCount, bool skipConstraints) {
	if (check_space != NULL) {
		DPSpace& s = getInput().initialize(bvCount, ivCount, skipConstraints);
		if (!skipConstraints) {
			postConstraint(s);
			optimal = applySearchOrder(s);
		} else {
			optimal = (!hasInput() || getInput().canApplySearchOrder());
		}
		return s;
	} else {
		return getInput().initialize(bvCount, ivCount, true);
	}
}
	
bool DominanceOperator::checkSolution(const DPSpace& solution) {
	if (check_space->propagators() == 0) {
		return true;
	} else {
		return check_space->check(solution);
	}
}
	
void DominanceOperator::solve(DPSpace& root) {
	if (check_space == NULL) {
		check_space = &root;
		Operator::solve();
	} else {
		Operator::solve(root);
	}
}

void DominanceOperator::addSolution(const DPSpace& solution) {
	if (!isOptimal() || solutions.size() == next_solution) {
		solutions.push_back(static_cast<const DPSpace*>(solution.clone()));
	}
	if (isOptimal() && hasOutput() && checkSolution(solution)) {
		solution.isUnique = false;
		getOutput().addSolution(solution);
	}
}

void DominanceOperator::constrain(DPSpace& home, const DPSpace& sol) {
	BoolVarArgs pos;	// should be 1
	BoolVarArgs neg;	// should be 0
	
	// Helper variable for equivalence constraint.
	BoolVar equiv(home,0,1);
	
	// Reification variable for equivalence constraint.
	Reify rEq( equiv, RM_IMP);
	
	// Flag to determine whether we need the equivalent constraint.
	// NO if equivalent is turned off or if we use optimal order and solution is unique
	bool eq = equivalent && (!optimal || !sol.isUnique) ;
	
	for (vector< Preorder >::const_iterator var = variables.begin(); var != variables.end(); ++var ) {
		alias_map_value& alias = home.getAlias(var->first);
		VariableType vartype = alias.first;
		CompareOrder varorder = var->second;
		
		for (int varid=alias.second.first; varid < alias.second.second; varid++) {
			if (vartype == BOOLVAR) { // boolean variable
				if (varorder == EQ || varorder == GEQ) {
					if (sol.bv[varid].val() == 0) {
						if (!home.bv[varid].assigned()) {
							pos << home.bv[varid];
						} else if (home.bv[varid].val() == 1) {
							return;	// constraint satisfied
						} // else skip var
					}
				}
				if (varorder == EQ || varorder == LEQ) {
					if (sol.bv[varid].val() == 1) {
						if (!home.bv[varid].assigned()) {
							neg << home.bv[varid];
						} else if (home.bv[varid].val() == 0) {
							return;	// constraint satisfied
						} // else skip var
					}
				}
				if (eq) rel(home, home.bv[varid], IRT_EQ, sol.bv[varid], rEq);
			} else if (vartype == INTVAR) { // integer variable
				BoolVar r(home,0,1);	// reification
				pos << r;
				if (varorder == EQ || varorder == GEQ) {
					rel(home,home.iv[varid], IRT_GR, sol.iv[varid], r);
				}
				if (varorder == EQ || varorder == LEQ) {
					rel(home,home.iv[varid], IRT_LE, sol.iv[varid], r);
				}
				if (eq) rel(home, home.iv[varid], IRT_EQ, sol.iv[varid], rEq);
			} else {
				assert(false); // unknown var type
			}
		}
	}
	
	if (eq) pos << equiv;
	clause(home, BOT_OR, pos, neg, 1);
	
}
	
void DominanceOperator::finalize() {
	getInput().finalize();
	
	if (isOptimal()) return;
	// flush solutions in case of not optimal
	
	DominanceOperator* c = this->clone(true);
	
	//DPSpace& check_space = c->initialize(0,0,true);
	c->postConstraint(*check_space);
	
	while (!solutions.empty()) {
		const DPSpace* sol = solutions.back();
		c->next_solution = c->solutions.size();
		bool output = (checkSolution(*sol) && hasOutput());
		c->addSolution(*sol);
		solutions.pop_back();
		
		if (output) {
			sol->isUnique = false;
			getOutput().addSolution(*sol);
		}
		
		delete sol;
	}
	
}

DominancePropagator::DominancePropagator(Space& home, DominanceOperator& par, ViewArray<Int::BoolView>& bv_, ViewArray<Int::IntView>& iv_) :  Propagator(home), bv(bv_), iv(iv_), parent(par), last_update(0) {
	bv.subscribe(home, *this, Int::PC_BOOL_VAL);
	iv.subscribe(home, *this, Int::PC_BOOL_VAL);
}

DominancePropagator::DominancePropagator(Space& home, bool share, DominancePropagator& p) : Propagator(home, share, p), parent(p.parent), last_update(p.last_update) {
	bv.update(home, share, p.bv);
	iv.update(home, share, p.iv);
}

size_t DominancePropagator::dispose(Space& home) {
	bv.cancel(home, *this, Int::PC_BOOL_VAL);
	iv.cancel(home, *this, Int::PC_BOOL_VAL);
	return sizeof(*this);
}
    
Propagator* DominancePropagator::copy(Space& home, bool share) {
	return new (home) DominancePropagator(home, share, *this);
}
    
void DominancePropagator::post(Space& home, DominanceOperator& parent, const BoolVarArgs& bv, const IntVarArgs& iv) {
	ViewArray<Int::BoolView> y0(home, bv);
	ViewArray<Int::IntView> y1(home, iv);
	(void) new (home) DominancePropagator(home, parent, y0, y1);
}

ExecStatus DominancePropagator::propagate(Space& home, const ModEventDelta& m) {
	size_t sol_size = parent.solutions.size();
	while (last_update < sol_size) {
		const Space* solution = parent.solutions[last_update];
		constrain(home, *solution);
		last_update++;
		if (home.failed()) {
			return ES_FAILED;
		}
	}
	return ES_NOFIX;	// need to keep constraint active!
}
    
void DominancePropagator::constrain(Space& home, const Space& solution) {
	parent.constrain(static_cast<DPSpace&>(home), static_cast<const DPSpace&>(solution));
}
    
PropCost DominancePropagator::cost(const Space& home, const ModEventDelta& med) const {
	return PropCost::unary(PropCost::LO);
}

