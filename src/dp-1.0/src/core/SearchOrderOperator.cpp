//
//  SearchOrderOperator.cpp
//  dp
//
//  Created by Anton Dries on 28/06/13.
//  Copyright (c) 2013 Anton Dries. All rights reserved.
//

#include "SearchOrderOperator.h"


typedef pair<string, CompareOrder> Preorder;

	
SearchOrderOperator::SearchOrderOperator(const Preorder& varref) {
	variables.push_back(varref);
}

SearchOrderOperator::SearchOrderOperator(const vector< Preorder >& variables) :
variables(variables) {}

SearchOrderOperator* SearchOrderOperator::clone() const {
	return new SearchOrderOperator(*this);
}

// override from Operator
bool SearchOrderOperator::canApplySearchOrder() const {
	return false;
}
	
void SearchOrderOperator::addSolution(const DPSpace& solution) {
	if (hasOutput()) getOutput().addSolution(solution);
}
	
DPSpace& SearchOrderOperator::initialize(int bvCount, int ivCount, bool skipConstraints) {
	DPSpace& s = getInput().initialize(bvCount, ivCount, skipConstraints);
	if (!skipConstraints) {
		applySearchOrder(s);
	}
	return s;
}
	
bool SearchOrderOperator::applySearchOrder(DPSpace& space) {
	//		if (!optimal) return false;
	if (hasInput() && !getInput().canApplySearchOrder()) return false;
	
	BoolVarArgs bvaU;
	BoolVarArgs bvaD;
	IntVarArgs ivaU;
	IntVarArgs ivaD;
	for (vector< Preorder >::const_iterator var = variables.begin(); var != variables.end(); ++var ) {
		alias_map_value& alias = space.getAlias(var->first);
		VariableType vartype = alias.first;
		CompareOrder varorder = var->second;
		
		if (vartype == BOOLVAR) {
			if (varorder == GEQ) {
				for (int varid=alias.second.first; varid < alias.second.second; varid++) {
					bvaD << space.bv[varid];
				}
			} else if (varorder == LEQ) {
				for (int varid=alias.second.first; varid < alias.second.second; varid++) {
					bvaU << space.bv[varid];
				}
			}
		} else {
			if (varorder == GEQ) {
				for (int varid=alias.second.first; varid < alias.second.second; varid++) {
					ivaD << space.iv[varid];
				}
			} else if (varorder == LEQ) {
				for (int varid=alias.second.first; varid < alias.second.second; varid++) {
					ivaU << space.iv[varid];
				}
			}
		}
	}
	branch(space, bvaD, INT_VAR_DEGREE_MAX(), INT_VAL_MAX());
	branch(space, bvaU, INT_VAR_DEGREE_MAX(), INT_VAL_MIN());
	branch(space, ivaD, INT_VAR_DEGREE_MAX(), INT_VAL_MAX());
	branch(space, ivaU, INT_VAR_DEGREE_MAX(), INT_VAL_MIN());
	
	return true;
}

