/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "DPSpace.h"
#include "DominanceOperator.h"

DPSpace::DPSpace(int bvCount, int ivCount) :
	bv(*this, bvCount, 0, 1),
	iv(*this, ivCount),
	aliases(* new alias_map()),
	optimalSearch(NULL),
	isUnique(false) {}
    
DPSpace::DPSpace(bool share, DPSpace& s) :
	Space(share,s),
	aliases(s.aliases),
	optimalSearch(s.optimalSearch),
	isUnique(s.isUnique)
	{
		bv.update(*this, share, s.bv);
		iv.update(*this, share, s.iv);
	}
    
DPSpace* DPSpace::copy(bool share) {
	return new DPSpace(share,*this);
}

bool DPSpace::check(const DPSpace& solution) {
	status(); // make sure all propagators have been run prior to cloning
	if (failed()) return false;
	DPSpace* s = static_cast<DPSpace*>(clone());
	s->assign(solution);
	s->status();
	bool result = !s->failed();
	delete s;
	return result;
}
	
void DPSpace::assign(const DPSpace& solution) {
	int bvs = solution.bv.size();   // note: current space could have more variables than solution
	for (int i=0; i<bvs; i++) rel(*this, bv[i], IRT_EQ, solution.bv[i].val());
	
	int ivs = solution.iv.size();   // note: current space could have more variables than solution
	for (int i=0; i<ivs; i++) rel(*this, iv[i], IRT_EQ, solution.iv[i].val());
}
	
void DPSpace::default_brancher() {
	branch(*this, &DPSpace::beforeDefaultBrancher);
	branch(*this, bv, INT_VAR_DEGREE_MAX(), INT_VAL_MAX());
	branch(*this, iv, INT_VAR_DEGREE_MAX(), INT_VAL_MAX());
}

void DPSpace::beforeDefaultBrancher(Space& home) {
    DPSpace& space = static_cast<DPSpace&>(home);
    if (space.optimalSearch != NULL) {
		// if we are using optimal search
		// and the current space if fully assigned
		// then there can be no solutions equivalent to this one
        if (space.bv.assigned() && space.iv.assigned()) space.isUnique = true;
        space.optimalSearch->notifyNew();
    }
}


