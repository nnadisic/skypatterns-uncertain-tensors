/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#ifndef dp_DefineOperator_h
#define dp_DefineOperator_h

#include "DPSpace.h"
#include "Operator.h"

/**
 Operator for defining variables in the constraint model and supplying an alias for them.
 */
class DefineOperator : public Operator {
    
private :
    string alias;		// alias of the variable
    int minval;			// minimum value
    int maxval;			// maximal value
    int size;			// number of variables defined
    
	/**
	 Check whether it defines a boolean variable (i.e. minval = 0 and maxval = 1).
	 */
    inline bool isBoolVar() const { return minval == 0 && maxval == 1; }
    
	/**
	 Return number of boolean variables defined.
	 */
    inline int boolVarCount() const {
        if (isBoolVar()) return size; else return 0;
    }
    
	/**
	 Return number of integer variables defined.
	 */
    inline int intVarCount() const {
        if (!isBoolVar()) return size; else return 0;
    }
    
public:
    
	/**
	 Create a new variable definition.
	 
	 - alias => variable name
	 - minval => minimum value of variable domain
	 - maxval => maximal value of variable domain
	 - size => number of variables (if >1, creates an array of variables)
	 */
    DefineOperator(const string& alias, int minval, int maxval, int size=1);
    
	// override from Operator
    virtual DPSpace& initialize(int bvCount, int ivCount, bool skipConstraints);
    
	// override from Operator
    virtual DefineOperator* clone() const;
	
};


#endif
