/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#ifndef dp_FunctionConstraint_h
#define dp_FunctionConstraint_h

#include "ConstraintOperator.h"

/**
 Posts a constraint based on a given lambda function.
 */
class FunctionConstraint : public ConstraintOperator {
    
private:
    function<void(DPSpace&)> func;	// function that posts the constraint
    
protected:
	
	// override from ConstraintOperator
    virtual void postConstraint(DPSpace& space);
	
	public :
    FunctionConstraint( function<void(DPSpace&)> func );
	
    virtual Operator* clone() const;
	
};

#define _IV(alias) s.getIV(alias)
#define _BVA(alias) s.getBVA(alias)
#define _CF(C) [](DPSpace& s) { C; }
#define _CFV(C,V) [V](DPSpace& s) { C; }

#endif
