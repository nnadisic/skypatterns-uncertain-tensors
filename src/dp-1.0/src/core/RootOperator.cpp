/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "RootOperator.h"

RootOperator::RootOperator() : Operator() {}
	
void RootOperator::solve(DPSpace& root) {
	root.default_brancher();
	DFS<DPSpace> e(&root);
	DPSpace* solution = e.next();
	while (solution) {
		if (hasOutput()) getOutput().addSolution(*solution);
		delete solution;
		solution = e.next();
	}
}

bool RootOperator::canApplySearchOrder() const {
	return true;
}

DPSpace& RootOperator::initialize(int bvCount, int ivCount, bool skipConstraints) {
	return *(new DPSpace(bvCount, ivCount));
}

RootOperator* RootOperator::clone() const {
	return new RootOperator(*this);
}

void RootOperator::finalize() {}
	
