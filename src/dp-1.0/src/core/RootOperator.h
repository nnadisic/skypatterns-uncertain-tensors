/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#ifndef dp_RootOperator_h
#define dp_RootOperator_h

#include <string>
#include <gecode/int.hh>
#include <gecode/search.hh>

#include "Operator.h"

using namespace Gecode;
using namespace std;

class OperatorChain;	// see OperatorChain.h

/**
 Root of the operator chain.
 
 A root operator is automatically created by the operator chain.
 */
class RootOperator : public Operator {
	
private:
	friend class OperatorChain;
	
	RootOperator();
	
public:
		
	// override from Operator
    virtual void solve(DPSpace& root);
	
public:

	// override from Operator
	virtual bool canApplySearchOrder() const;
    
	
protected :
    
	// override from Operator
    virtual DPSpace& initialize(int bvCount, int ivCount, bool skipConstraints);
    
public :
    
    
	// override from Operator
    virtual RootOperator* clone() const;
    
	// override from Operator
	virtual void finalize();

};

#endif
