/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "Operator.h"

/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

Operator::Operator() : input(NULL), output(NULL) {}
    
void Operator::solve() {
	DPSpace& space = initialize(0,0,false);
	solve(space);
	finalize();
}
	
void Operator::solve(DPSpace& root) {
	getInput().solve(root);
}

bool Operator::canApplySearchOrder() const {
	return getInput().canApplySearchOrder();
}

void Operator::finalize() {
	getInput().finalize();
}
    
void Operator::addSolution(const DPSpace& solution) {
	if (hasOutput()) getOutput().addSolution(solution);
}
    
Operator& Operator::getRoot() {
	if (hasInput()) return getInput().getRoot(); else return *this;
}
	
const Operator& Operator::getRoot() const {
	if (hasInput()) return getInput().getRoot(); else return *this;
}
