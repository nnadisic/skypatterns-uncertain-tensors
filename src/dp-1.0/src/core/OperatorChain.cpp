/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "OperatorChain.h"
#include "RootOperator.h"

OperatorChain::OperatorChain() : root(NULL), tail(NULL) {}

OperatorChain& OperatorChain::operator<<(const Operator& op) {
	Operator* op_clone = op.clone();
	if (tail != NULL) {
		tail->setOutput(*op_clone); // also sets input
	}
	if (root == NULL) {
		root = op_clone;
	}
	tail = op_clone;
	return *this;
}

OperatorChain& OperatorChain::operator<<(const OperatorChain& next) {
	if (tail != NULL) {
		tail->setOutput(*next.root); // also sets input
	}
	if (root == NULL) {
		root = next.root;
	}
	tail = next.tail;
	return *this;
}

void OperatorChain::solve() const {
	if (root == NULL) return;
	RootOperator rootOp;
	rootOp.setOutput(*root);
	tail->solve();
}
