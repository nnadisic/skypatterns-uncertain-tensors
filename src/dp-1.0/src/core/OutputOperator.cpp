/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "OutputOperator.h"


OutputOperator::OutputOperator(ostream& os) : os(os), solcount(0) {}
    
OutputOperator::OutputOperator(ostream& os, vector< OutSpec > outspec) : os(os), solcount(0), outspec(outspec) {}
    
DPSpace& OutputOperator::initialize(int bvCount, int ivCount, bool skipConstraints) {
	return getInput().initialize(bvCount, ivCount, skipConstraints);
}

void OutputOperator::addSolution(const DPSpace& solution) {
	solcount++;
	for (vector< OutSpec >::const_iterator iter = outspec.begin(); iter != outspec.end(); ++iter) {
		switch(iter->first) {
			case CONSTANT :
				os << iter->second;
				break;
			case INTVALUE :
				os << solution.getIV(iter->second);
				break;
			case BOOLVALUE :
				os << solution.getBVA(iter->second);
				break;
			case ARR2SET :
			{
				BoolVarArgs var = solution.getBVA(iter->second);
				for (int i=0; i<var.size();i++) if (var[i].val()) os << i << " ";
			}
				break;
			case SETSIZE :
			{
				BoolVarArgs var = solution.getBVA(iter->second);
				int n=0;
				for (int i=0; i<var.size();i++) if (var[i].val()) n++;
				os << n;
			}
				break;
				
		}
	}
	if (!outspec.empty()) os << endl;
}

void OutputOperator::finalize() {
	getInput().finalize();
	os << "Number of solutions: " << solcount << endl;
}

Operator* OutputOperator::clone() const {
	return new OutputOperator(*this);
}


