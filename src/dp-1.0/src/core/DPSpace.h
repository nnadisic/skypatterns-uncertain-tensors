/*
 
Main authors:
	Anton Dries <anton.dries@cs.kuleuven.be>
	
Copyright:
	Anton Dries, 2013
 
This file is part of the Dominance Programming Framework, and uses Gecode.
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef dp_DPSpace_h
#define dp_DPSpace_h

#include <gecode/int.hh>
#include <map>

class DominanceOperator;	// see DominanceOperator.h

using namespace Gecode;
using namespace std;

/**
Types of variables currently supported.
 */
enum VariableType {
    BOOLVAR,
    INTVAR
};

typedef pair< VariableType, pair<int, int > > alias_map_value;
typedef map< string, alias_map_value > alias_map;

/**
 General modelling space for use with DP algebra.
 */
class DPSpace : public Space {
    
public :
    BoolVarArray bv;	// boolean variables defined in the model
    IntVarArray iv;		// integer variables defined in the model
    alias_map& aliases;	// mapping between variable names and type/index
    
    DominanceOperator* optimalSearch;		// dominance operator that dictates optimal search order (or NULL)
    mutable bool isUnique;					// flag determining whether equivalent solutions can exist (according to optimalSearch dominance operator)
    
	/**
	 Create a new space with the given number of boolean and integer variables.
	 */
    DPSpace(int bvCount, int ivCount);
    
	/**
	 Copy constructor required by Gecode.
	 */
    DPSpace(bool share, DPSpace& s);
    
	/**
	 Copy method required by Gecode.
	 */
    virtual DPSpace* copy(bool share);
	   
	/**
	 Register a variable alias.
	 
	 - alias => name that can be used to refer to the variable (range)
	 - vartype => type of variable
	 - varrange => index range of the variables (upper bound exclusive)
	 */
    inline void registerAlias(const string& alias, VariableType vartype, pair<int, int> varrange) {
        aliases[ alias ] = alias_map_value( vartype, varrange );
    }

    /**
	 Get an array of Gecode BoolVars by alias.
	 
	 - alias => variable alias
	 */
    inline BoolVarArgs getBVA(const string& alias) {
        alias_map_value& var = getAlias(alias);
        pair<int,int> range = var.second;
        return bv.slice(range.first, 1, range.second-range.first);
    }

	inline BoolVarArgs getBVA(const string& alias) const {
		return const_cast<DPSpace&>( *this ).getBVA(alias);
	}
	
	/**
	 Get a Gecode IntVar by alias.
	 
	 - alias => variable alias
	 */
    inline IntVar& getIV(const string& alias) {
        alias_map_value& var = getAlias(alias);
        pair<int,int> range = var.second;
        return iv[range.first];
    }
    
    inline IntVar getIV(const string& alias) const {
		return const_cast<DPSpace&>( *this ).getIV(alias);
	}


	/**
	 Get variable type and range for the given alias.
	 
	 - alias => variable alias
	 */
    inline alias_map_value& getAlias(const string& alias) const {
		alias_map::iterator result = aliases.find(alias);
		assert(result != aliases.end());
        return result->second;
    }
    
	/**
	 Check whether the given solution is possible in this space.
	 
	 - solution => solution to check
	 */
	bool check(const DPSpace& solution);

private :
	/** 
	 Assign the given solution in this space.
	 */
	void assign(const DPSpace& solution);

public:
	
	/**
	 Apply the default brancher.
	 This makes sure that all generated solution are completely instantiated.
	 */
	void default_brancher();
    
private:
	
	/**
	 Method called before default branchers are executed.

	 If we are working according to an optimal search strategy,
	  at this point we will start enumerating equivalent solutions.
	 */
    static void beforeDefaultBrancher(Space& home); // implementation in DominanceOperator.h
    
};


#endif
