/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#ifndef dp_OutputOperator_h
#define dp_OutputOperator_h

#include "Operator.h"

/**
 Defines output specification types.
 */
enum OutSpecType {
    CONSTANT,	// output argument as is
    ARR2SET,	// print boolean array as set (arg = alias)
    SETSIZE,	// print size of set represented by boolean array (arg = alias)
    BOOLVALUE,	// print boolean variable (arg = alias)
    INTVALUE,	// print integer variable (arg = alias)
};

/**
 Output specification consists of an OutSpecType and a string argument.
 */
typedef pair<OutSpecType, string> OutSpec;


/**
 Operator for combining OutSpecs.
 */
inline vector< OutSpec > operator&(vector<OutSpec> one, const OutSpec& two) {
	one.push_back(two);
    return one;
}

/**
 Operator for combining OutSpecs.
 */
inline vector< OutSpec > operator&(const OutSpec& one, const OutSpec& two) {
    vector< OutSpec > result;
    result.push_back(one);
    result.push_back(two);
    return result;
}

/**
 Operator for outputting results.
 */
class OutputOperator : public Operator {
    
private:
    
    ostream& os;				// output stream to write to
    int solcount;				// number of solutions
    vector< OutSpec > outspec;	// output specification for solutions
        
public:
    
	/**
	 Create an output operator that does not print individual solutions.
	 */
    OutputOperator(ostream& os);
	
	/**
	 Create an output operator that outputs solutions according to the given specification.
	 */
    OutputOperator(ostream& os, vector< OutSpec > outspec);
    
	// override from Operator
    virtual DPSpace& initialize(int bvCount, int ivCount, bool skipConstraints);
    
	// override from Operator
    virtual void addSolution(const DPSpace& solution);
    
	// override from Operator
    virtual void finalize();

	// override from Operator
    virtual Operator* clone() const;
    
};



#endif
