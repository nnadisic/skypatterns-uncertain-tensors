/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "DefineOperator.h"

DefineOperator::DefineOperator(const string& alias, int minval, int maxval, int size)
: alias(alias), minval(minval), maxval(maxval), size(size) {}

DPSpace& DefineOperator::initialize(int bvCount, int ivCount, bool skipConstraints) {
	// get parent space
	DPSpace& result = getInput().initialize(bvCount + boolVarCount(), ivCount + intVarCount(), skipConstraints);
	
	// determine start index of variables (we want them in order)
	int bvStart = result.bv.size() - bvCount - size;
	int ivStart = result.iv.size() - ivCount - size;
	
	if (isBoolVar()) {
		// register the alias
		result.registerAlias(alias, BOOLVAR, pair<int,int>(bvStart, bvStart+size) );
	} else {
		// initialize integer variables with correct ranges
		for (int i=0; i<size; i++) {
			result.iv[i + ivStart] = IntVar(result,minval,maxval);
		}
		// register the alias
		result.registerAlias(alias, INTVAR, pair<int,int>(ivStart, ivStart+size) );
	}
	return result;
}

DefineOperator* DefineOperator::clone() const {
	return new DefineOperator(*this);
}
