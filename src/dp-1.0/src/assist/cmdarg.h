/*
 
 Main authors:
 Anton Dries <anton.dries@cs.kuleuven.be>
 
 Copyright:
 Anton Dries, 2013
 
 This file is part of the Dominance Programming Framework, and uses Gecode.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#ifndef cpsampling_cmdarg_h
#define cpsampling_cmdarg_h

#include <string>

bool process_flag(int args, char* argv[], const char* flag) {
    bool result = false;
    for (int i=0; i<args; i++) {
        if (argv[i] != NULL && strcmp(argv[i],flag)==0) {
            argv[i] = NULL;
            result = true;
        }
    }
    return result;
}

int process_int(int args, char* argv[], const char* flag, int def) {
    int result = def;
    for (int i=0; i<args; i++) {
        if (argv[i] != NULL && strcmp(argv[i],flag)==0) {
            argv[i] = NULL;
            result = atoi(argv[i+1]);
            argv[i+1] = NULL;
        }
    }
    return result;
}

const char* process_string(int args, char* argv[], const char* flag, const char* def) {
    char* result = NULL;
    for (int i=0; i<args; i++) {
        if (argv[i] != NULL && strcmp(argv[i],flag)==0) {
            argv[i] = NULL;
            result = argv[i+1];
            argv[i+1] = NULL;
        }
    }
    if (result == NULL) return def;
    return result;
}


#endif
