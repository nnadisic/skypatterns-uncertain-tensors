#!/bin/bash
size=$(cat ./bin/$1|grep -v "#"| wc|awk '{print $1}')
first=$(cat ./bin/$1|grep -v "#"|awk 'BEGIN{first=0}{if ($1==1) {first=first+1;}}END{print first}')
./micmac-0.0.5/micmac -i ./bin/$1 -p 3 -g $2 -V | grep -v "#" |sort -u |  awk -F" : " -v size=$size -v first=$first '{printf($1);printf(" : ");printf($2);printf("\n");}'| awk -F',' '{for (i=1; i<NF;i++) {printf($i);printf(".")} print($NF)}' | awk '{pv=0; for (i=1; i<=NF; i++) {if (($i==":")&&(pv==0)) {pv=i} printf($i); printf(" ")}printf(": ");printf((pv-1)*$(pv+1));printf("\n");}' > closed.tmp
./skypattern-0.0.2/skypattern -i closed.tmp 


