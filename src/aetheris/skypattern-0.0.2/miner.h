struct Pattern {
  char* m_pattern;
  float* m_measures;
  Pattern* m_next;
};

class Miner {
 public:
  Miner(Info*);
  ~Miner();
  void listSkypatterns();
  unsigned int getNbSkypatterns();
  void setPatternSet(char*);
  unsigned long getNbPatterns();
private:
  bool addPattern(Pattern*);
  Pattern* copyPattern(Pattern*);
  Pattern* removePattern(Pattern*);
  bool isDominatedBy(Pattern*, Pattern*);

  Info* m_info;
  Pattern* m_skypatterns;
  unsigned int m_nbMeas;
  std::ostream* m_out;
  unsigned int m_verbose;
  char m_name[256];
  unsigned long m_nbPatterns;
};
