#include <iostream>
#include <sstream>
#include <fstream>
#include "string.h"
#include "stdlib.h"
#include "global.h"
#include "interpreter/option.h"
#include "interpreter/interpreter.h"
#include "info.h"
#include "miner.h"

using namespace std;

Miner::Miner(Info* info) {
  m_info=info;
  m_skypatterns=NULL;
  m_nbMeas=0;
  m_out=info->getOut();
  m_verbose=info->getVerbose();
}

Miner::~Miner() {
  m_info=NULL;
  if (m_skypatterns) {
    Pattern* current=m_skypatterns;
    while (current) {
      Pattern* tmp=current;
      current=tmp->m_next;
      delete tmp->m_measures;
      delete tmp->m_pattern;
      tmp->m_next=NULL;
      delete tmp;
    }
  }
}

Pattern* Miner::copyPattern(Pattern* pattern) {
  Pattern* newP=new Pattern;
  newP->m_pattern=new char [strlen(pattern->m_pattern)+1];
  strcpy(newP->m_pattern, pattern->m_pattern);
  newP->m_measures=new float [m_nbMeas];
  for (unsigned int i=0; i<m_nbMeas; i++)
    newP->m_measures[i]=pattern->m_measures[i];
  newP->m_next=NULL;
  return newP;
}

Pattern* Miner::removePattern(Pattern* pattern) {
  Pattern* tmp=pattern;
  Pattern* current=tmp->m_next;
  delete tmp->m_measures;
  delete tmp->m_pattern;
  tmp->m_next=NULL;
  delete tmp;
  return current;
}

bool Miner::isDominatedBy(Pattern* p1, Pattern* p2) {
  unsigned int k=0;
  if (m_info->isDescendant()) {
    Pattern* tmp=p1;
    p1=p2;
    p2=tmp;
  }
  while ((k<m_nbMeas)&&(p1->m_measures[k]<=p2->m_measures[k])) {
    k++;
  }
  if (k<m_nbMeas)
    return false;
  k=0;
  while ((k<m_nbMeas)&&(p1->m_measures[k]==p2->m_measures[k]))
    k++;
  if (k<m_nbMeas)
    return true;
  return false;
}

bool Miner::addPattern(Pattern* pattern) {
  Pattern* current=m_skypatterns;
  Pattern* previous=NULL;
  while (current) {
    if (isDominatedBy(pattern, current))
      return false;
    if (isDominatedBy(current, pattern)) {
      Pattern* next=removePattern(current);
      if (previous)
	previous->m_next=next;
      else
	m_skypatterns=next;
      current=next;
    }
    else {
      previous=current;
      current=current->m_next;
    }
  }
  if (previous)
    previous->m_next=pattern;
  else
    m_skypatterns=pattern;
  return true;
}

void Miner::listSkypatterns() {
  if (m_info->getVerbose()>=1)
    cerr<<"Print the skyline"<<endl;
  Pattern* current=m_skypatterns;
  while (current) {
    (*m_out)<<current->m_pattern;
    for (unsigned int i=0; i<m_nbMeas; i++) 
      (*m_out)<<" : "<<current->m_measures[i];
    (*m_out)<<endl;
    current=current->m_next;
  }
}

unsigned int Miner::getNbSkypatterns() {
  Pattern* current=m_skypatterns;
  unsigned int k=0;
  while (current) {
    current=current->m_next;
    k++;
  }
  return k;
}

void Miner::setPatternSet(char* patterns) {
  Pattern* previous=NULL;
  float measures[100];
  if (m_verbose>=1)
    cerr<<"Loading "<<patterns<<endl;
  strcpy(m_name,patterns);
  char buffer[MAX_TRANS]; 
  fstream file(patterns);
  while(!file.eof()) {
    file.getline(buffer,MAX_TRANS,'\n');
    if ((strlen(buffer)>0) && (buffer[0]!='#')) {
      Pattern* pattern=new Pattern;
      pattern->m_next=NULL;
      unsigned int k=0;
      // graphie du pattern
      while (buffer[k]!=':')
	k++;
      buffer[k-1]='\0';
      pattern->m_pattern=new char [strlen(&buffer[0])+1];
      strcpy(pattern->m_pattern, &buffer[0]);

      // mesures
      k=k+1;
      unsigned int l=k;
      OBJ freq=0;
      char end;
      do {
	k=k+1;
	l=k;
	while ((buffer[k]!=' ')&&(buffer[k]!='\n')&&(buffer[k]!='\0')) {
	  k++;
	}
	end=buffer[k];
     	buffer[k]='\0';
	float tmp=(float)atof(&buffer[l]);
	measures[freq++]=tmp;
	k=k+2;
      }
      while (end==' ');
      if (m_nbMeas<freq) {
	m_nbMeas=freq;
      }
      pattern->m_measures=new float [freq];
      for (OBJ i=0; i<freq; i++)
	pattern->m_measures[i]=measures[i];
      // ajouter au pattern set
      if ((strlen(pattern->m_pattern)==0)||(!addPattern(pattern))) {
	delete pattern->m_pattern;
	delete pattern->m_measures;
	delete pattern;
      }
      m_nbPatterns++;
    }
  }
  file.close();
}

unsigned long Miner::getNbPatterns() {
  return m_nbPatterns;
}
