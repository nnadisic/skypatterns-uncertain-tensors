class Info {
public:
  Info();
  ~Info();
  void setOut(std::ostream*);
  std::ostream* getOut();
  void setVerbose(unsigned char);
  unsigned char getVerbose();
  void setStatistics(bool);
  bool getStatistics();
  bool isDescendant();
  void setDescendant();
private:

  std::ostream* m_out;
  unsigned char m_verbose;
  bool m_statistics;
  bool m_descendant;
};
