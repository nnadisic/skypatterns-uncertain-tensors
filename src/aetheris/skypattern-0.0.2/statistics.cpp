#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "stdlib.h"
#include "global.h"
#include "info.h"
#include "statistics.h"

#define NB_HISTO 20

using namespace std;

Statistics::Statistics(Info* info) {
  m_info=info;
  m_originalMean=0;
  m_originalStd=0;
  m_balancedMean=0;
  m_balancedStd=0;
  m_weightMean=0;
  m_weightStd=0;
  m_nbPatterns=0;
  m_nbRelevantPatterns=0;
  m_nbIterations=0;
  m_avgDifference=0;
  m_histoSupp=new long [NB_HISTO];
  m_histoPB=new long [NB_HISTO];
  for (int i=0; i<NB_HISTO; i++) {
    m_histoSupp[i]=0;
    m_histoPB[i]=0;
  }
}

Statistics::~Statistics() {
  m_info=NULL;
}

void Statistics::print() {
  ostream* m_out=m_info->getOut();
  (*m_out).precision(3);
  (*m_out)<<"# Statistics"<<endl;
  (*m_out)<<"# ----------"<<endl;
  (*m_out)<<"# Original mean: "<<m_originalMean<<endl;
  (*m_out)<<"# Balanced mean: "<<m_balancedMean<<endl;
  (*m_out)<<"# Mean gain: "<<m_originalMean/m_balancedMean<<endl;
  (*m_out)<<"# Original stddev: "<<m_originalStd<<endl;
  (*m_out)<<"# Balanced stddev: "<<m_balancedStd<<endl;
  (*m_out)<<"# Stddev gain: "<<m_originalStd/m_balancedStd<<endl;
  //  (*m_out)<<"# Mean of weight: "<<m_weightMean<<endl;
  //(*m_out)<<"# Stddev of weight: "<<m_weightStd<<endl;
  (*m_out)<<"# Number of patterns to balance: "<<m_nbPatterns<<endl;
  (*m_out)<<"# Number of relevant patterns: "<<m_nbRelevantPatterns<<endl;
  (*m_out)<<"# Number of iterations: "<<m_nbIterations<<endl;
  //(*m_out)<<"# Number of iterations: "<<m_nbIterations<<endl;
  (*m_out)<<"# Average diff: "<<m_avgDifference/m_nbRelevantPatterns<<endl;
  (*m_out)<<"# Support histogram: ";
  for (int i=0; i<NB_HISTO; i++)
    (*m_out)<<m_histoSupp[i]<<" ";
  (*m_out)<<endl;
  (*m_out)<<"# BS histogram: ";
  for (int i=0; i<NB_HISTO; i++)
    (*m_out)<<m_histoPB[i]<<" ";
  (*m_out)<<endl;
}

void Statistics::printOriginal() {
  ostream* m_out=m_info->getOut();
  (*m_out)<<"# Iteration 0 : "<<m_originalMean<<" "<<m_originalStd<<endl;
}

void Statistics::printBalanced() {
  ostream* m_out=m_info->getOut();
  (*m_out)<<"# Iteration "<<m_nbIterations<<" : "<<m_balancedMean<<" "<<m_balancedStd<<endl;
}

void Statistics::setOriginalMean(double originalMean) {
  m_originalMean=originalMean;
}

void Statistics::setOriginalStd(double originalStd) {
  m_originalStd=originalStd;
}

void Statistics::setBalancedMean(double balancedMean) {
  m_balancedMean=balancedMean;
}

void Statistics::setBalancedStd(double balancedStd) {
  m_balancedStd=balancedStd;
}

void Statistics::setWeightMean(double weightMean) {
  m_weightMean=weightMean;
}

void Statistics::setWeightStd(double weightStd) {
  m_weightStd=weightStd;
}

void Statistics::incrementNbPatterns() {
  m_nbPatterns++;
}

void Statistics::incrementNbRelevantPatterns() {
  m_nbRelevantPatterns++;
}

void Statistics::incrementNbIterations() {
  m_nbIterations++;
}

long Statistics::getNbIterations() {
  return m_nbIterations;
}

void Statistics::setSuppPB(double supp, double PB) {
  m_histoSupp[(int)(supp/(1./NB_HISTO))]++;
  m_histoPB[(int)(PB/(1./NB_HISTO))]++;
  if (supp>PB)
    m_avgDifference+=supp-PB;
  else
    m_avgDifference+=PB-supp;
}
