#include <iostream>
#include <sstream>
#include <fstream>
#include "string.h"
#include "stdlib.h"
#include "global.h"
#include "interpreter/option.h"
#include "interpreter/interpreter.h"
#include "info.h"
#include "miner.h"

using namespace std;

// Initialisation de l'interpréteur des commandes
void initialize(Interpreter *interpreter) {
  interpreter->setInformations(
"skypattern mines all the skypatterns of a given set of patterns",
			      "Example: skypattern -i dataset.patt",
			      "<arnaud.soulet@univ-tours.fr>");
  // Aide
  Option* help=new Option("help",'h',"give this message");
  interpreter->addOption(help);
  // Version
  Option* version=new Option("version",'v',"give the version number");
  interpreter->addOption(version);
  // Input
  Option* input=new Option("input",'i',"pattern set to balance");
  input->setParameters(true,1,1,"FILE");
  interpreter->addOption(input);
  // Output
  Option* output=new Option("output",'o',"patterns with support and balanced support (=cout)");
  output->setParameters(false,1,1,"FILE");
  interpreter->addOption(output);
  // Verbose
  Option* verbose=new Option("verbose",'V',"verbose mode");
  verbose->setParameters(false,0,1,"NUMBER");
  interpreter->addOption(verbose);
  // Descandant
  Option* descendant=new Option("descendant",'d',"descendant order");
  interpreter->addOption(descendant);
  // statistics
  Option* statistics=new Option("statistics",'s',"compute statistics");
  interpreter->addOption(statistics);

  interpreter->setHV('h','v');
}


int main (int argc, char * const argv[]) {
  clock_t end, start;
  long double execution_time;

  Interpreter interpreter(NAME,VERSION);
  initialize(&interpreter);
  if (interpreter.run(argc, argv)==1)
    return 1;

  Info* info=new Info();

  // option -o
  std::ostream* out=&cout;
  if (interpreter.getOption('o')->getInit()>0) {
    out=new ofstream(interpreter.getOption('o')->getParameter(0),ios::out);
    if (out==&cout)
      cerr<<NAME<<": file error"<<endl;
  }
  info->setOut(out);

  // option -V
  unsigned char verbose=0;
  if (interpreter.getOption('V')->getInit()>=0) {
    if (interpreter.getOption('V')->getInit()==1)
      verbose=(int)atoi(interpreter.getOption('V')->getParameter(0));
    else
      verbose=1;
  }
  info->setVerbose(verbose);

  // option -d
  if (interpreter.getOption('d')->getInit()>=0)
    info->setDescendant();

  // option -s
  if (interpreter.getOption('s')->getInit()>=0)
    info->setStatistics(true);

  Miner* miner=new Miner(info);
  start=clock();

  miner->setPatternSet(interpreter.getOption('i')->getParameter(0));
  miner->listSkypatterns();
  end=clock();
  execution_time=((long double)(end-start))/CLOCKS_PER_SEC;

  if (info->getStatistics()) {
    (*out)<<"# Statistics"<<endl;
    (*out)<<"# ----------"<<endl;
    (*out)<<"# Number of input patterns: "<<miner->getNbPatterns()<<endl;
    (*out)<<"# Number of skypatterns: "<<miner->getNbSkypatterns()<<endl;
    (*out)<<"# Execution time: "<<execution_time<<endl;
  }
  delete miner;
  delete info;
  return 0;
}
