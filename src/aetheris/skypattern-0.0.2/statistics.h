class Statistics {
public:
  Statistics(Info* info);
  ~Statistics();
  void print();
  void printOriginal();
  void printBalanced();
  void setOriginalMean(double originalMean);
  void setOriginalStd(double originalStd);
  void setBalancedMean(double balancedMean);
  void setBalancedStd(double balancedStd);
  void setWeightMean(double weightMean);
  void setWeightStd(double weightStd);
  void incrementNbPatterns();
  void incrementNbRelevantPatterns();
  void incrementNbIterations();
  long getNbIterations();
  void setSuppPB(double, double);
private:
  Info* m_info;

  double m_originalMean;
  double m_originalStd;
  double m_balancedMean;
  double m_balancedStd;
  double m_weightMean;
  double m_weightStd;
  long m_nbPatterns;
  long m_nbRelevantPatterns;
  long m_nbIterations;
  double m_avgDifference;
  long* m_histoSupp;
  long* m_histoPB;
};
