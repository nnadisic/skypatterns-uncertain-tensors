///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//  File: interpreter.cpp                                                    //
//  Last modification: 15/12/05                                              //
//  Class: Interpreter                                                       //
//  Description: Syntaxic analyser of cammand lines                          //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// TO DO
// - mettre un typage sur les param�tres
// - choisir le flux de sortie pour les messages en distinguant erreurs et 
// informations
// - Passer les diff�rents champs restreints � 256 caract�res en dynamique
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "stdlib.h"
#include "string.h"
#include "option.h"
#include "interpreter.h"

using namespace std;


///////////////////////////////////////////////////////////////////////////////
// Constructeurs
///////////////////////////////////////////////////////////////////////////////
Interpreter::Interpreter(const char name[], const char  version[]) {
  strcpy(m_name,name);
  strcpy(m_version,version);
  m_nbOptMax=1;
  m_nbOpt=0;
  m_options=new Option* [m_nbOptMax];
  strcpy(m_aim,"");
  strcpy(m_example,"");
  strcpy(m_contact,"");
  m_versionSC=0;
  m_helpSC=0;
}

Interpreter::Interpreter(Interpreter & interpreter) {
  strcpy(m_name, interpreter.m_name);
  strcpy(m_version, interpreter.m_version);
  strcpy(m_aim, interpreter.m_aim);
  strcpy(m_example, interpreter.m_example);
  strcpy(m_contact, interpreter.m_contact);
  m_nbOptMax=interpreter.m_nbOptMax;
  m_nbOpt=interpreter.m_nbOpt;
  m_options=new Option* [m_nbOptMax];
  for (unsigned short i=0; i<m_nbOpt;  i++)
    m_options[i]=new Option(*interpreter.m_options[i]);
  m_versionSC=interpreter.m_versionSC;
  m_helpSC=interpreter.m_helpSC;
}


///////////////////////////////////////////////////////////////////////////////
// Destructeurs
///////////////////////////////////////////////////////////////////////////////
Interpreter::~Interpreter() {
  if (m_options) {
    for (unsigned short i=0; i<m_nbOpt; i++)
      delete m_options[i];
    delete m_options;
    m_options=NULL;
  }
}


///////////////////////////////////////////////////////////////////////////////
// Affiche le message par d�faut d'utilisation du programme
///////////////////////////////////////////////////////////////////////////////
void Interpreter::printDefault() {
  printUse();
  if (m_helpSC!=0)
    cerr<<"Try '"<<m_name<<" -"<<m_helpSC<<"' for more information."<<endl;
}


///////////////////////////////////////////////////////////////////////////////
// Analyse syntaxique proprement dite
// - programmation assez laide
// - v�rifie si les options existent
// - v�rifie la pr�sence des options obligatoires
// - v�rifie le nombre des argument pour chaque option
///////////////////////////////////////////////////////////////////////////////
unsigned short Interpreter::run(int argc, char * const argv[]) {
  // Aucun argument
  if (argc==1) {
    printDefault();
    return 1;
  }
  // Cherche l'aide ou le numéro de Version
  unsigned short k=1;
  while (k<argc) {
    if (isOption(argv[k])) {
      Option* tmp=readOption(argv[k]);
      if (!tmp)
	return 1;
      if (tmp->getShortCut()==m_helpSC) {
	printHelp();
	return 1;
      }
      if (tmp->getShortCut()==m_versionSC) {
	printVersion();
	return 1;
      }
    }
    k++;
  }
  // Vérification des commandes obligatoires
  for (unsigned short j=0; j<m_nbOpt; j++)
    if (m_options[j]->isNecessary()) {
      k=1;
      while ((k<argc)&&((!isOption(argv[k]))||((isOption(argv[k]))&&(readOption(argv[k])!=m_options[j]))))
	k++;
      if (k==argc) {
	printDefault();
	return 1;
      }				
    }
  
  // Vérification du bon nombre d'arguments
  if (!isOption(argv[1])) {
    printDefault();
    return 1;
  }
  k=1;
  unsigned short i=1;
  Option* current=NULL; //readOption(argv[1]);
  unsigned short nbParam=0;
  while (k<argc) {
    if (!isOption(argv[k]))
      nbParam++;
    if (isOption(argv[k])) {
      if (current) {
	int tmp=current->checkParam(nbParam);
	if (tmp!=0) {
	  if (tmp==-1) {
	    cerr<<m_name<<": too less number of parameters for "<<argv[i]<<endl;
	  }
	  else
	    cerr<<m_name<<": too many number of parameters for "<<argv[i]<<endl;		
	  printDefault();
	  return 1;
	}
	current->setRealParameters(k-i-1,&argv[i+1]);
      }
      i=k;
      current=readOption(argv[k]);
      nbParam=0;
    }
    k++;
  }
  if (current) {
    int tmp=current->checkParam(nbParam);
    if (tmp!=0) {
      if (tmp==-1) {
	cerr<<m_name<<": too less number of parameters for "<<argv[i]<<endl;
      }
      else
	cerr<<m_name<<": too many number of parameters for "<<argv[i]<<endl;		
      printDefault();
      return 1;
    }
	current->setRealParameters(k-i-1,&argv[i+1]);
  }
  return 0;
}


///////////////////////////////////////////////////////////////////////////////
// Retourne vraie si la chaine de caract�re est une option - ou --
///////////////////////////////////////////////////////////////////////////////
bool Interpreter::isOption(char * const param) {
  if ((param[0]=='-')&&(param[1]=='-'))
    return true;
  if ((strlen(param)==2)&&(param[0]=='-')&&(((param[1]>='a')&&(param[1]<='z'))||((param[1]>='A')&&(param[1]<='Z'))))
    return true;
  return false;
}


///////////////////////////////////////////////////////////////////////////////
// Lit une option sans v�rifier qu'il en s'agit bien d'une!!!
///////////////////////////////////////////////////////////////////////////////
Option* Interpreter::readOption(char * const param) {
  if ((param[0]=='-')&&(param[1]=='-'))
    return getOption(&param[2]);		
  else
    return getOption(param[1]);
}


///////////////////////////////////////////////////////////////////////////////
// Permet de fixer l'option et la version par d�faut 
///////////////////////////////////////////////////////////////////////////////
void Interpreter::setHV(char help, char version) {
  m_versionSC=version;
  m_helpSC=help;
}


///////////////////////////////////////////////////////////////////////////////
// Retourne l'option correspondant au raccourci shortCut
///////////////////////////////////////////////////////////////////////////////
Option* Interpreter::getOption(char shortCut) {
  unsigned short k=0;
  while ((k<m_nbOpt)&&(m_options[k]->getShortCut()!=shortCut))
    k++;
  if (k<m_nbOpt)
    return m_options[k];
  else {
    cerr<<"clope: invalid option -"<<shortCut<<endl;
    printDefault();
    return NULL;
  }
}


///////////////////////////////////////////////////////////////////////////////
// Retourne l'option correspondant au nom name
///////////////////////////////////////////////////////////////////////////////
Option* Interpreter::getOption(char * const name) {
  unsigned short k=0;
  while ((k<m_nbOpt)&&(strcmp(m_options[k]->getName(),name)!=0))
    k++;
  if (k<m_nbOpt)
    return m_options[k];
  else {
    cerr<<"clope: invalid option --"<<name<<endl;
    printDefault();
    return NULL;
  }
}


////////////////////////////////////////////////////////////////////////////////
// Ajoute une option au programme
////////////////////////////////////////////////////////////////////////////////
void Interpreter::addOption(Option* option) {
  if (m_nbOpt==m_nbOptMax) {
    Option** tmp=new Option* [m_nbOptMax*2];
    for (unsigned short i=0; i<m_nbOpt; i++) {
      tmp[i]=m_options[i];
      m_options[i]=NULL;
    }
    delete m_options;
    m_options=tmp;
    m_nbOptMax=m_nbOptMax*2;
  }
  m_options[m_nbOpt++]=option;
}


////////////////////////////////////////////////////////////////////////////////
// Donner quelques informations de base : le but, un exemple et un e-mail
////////////////////////////////////////////////////////////////////////////////
void Interpreter::setInformations(const char aim[], const char example[], const char contact[]) {
  strcpy(m_aim, aim);
  strcpy(m_example, example);
  strcpy(m_contact, contact);
}


////////////////////////////////////////////////////////////////////////////////
// Afffiche l'usage avec traitement automatique des commandes
////////////////////////////////////////////////////////////////////////////////
void Interpreter::printUse() {
  cerr<<"Use: "<<m_name<<" ";
  unsigned short k=0;
  while (k<m_nbOpt) {
    if (m_options[k]->isNecessary()) {
      cerr<<"-"<<m_options[k]->getShortCut()<<" ";
      m_options[k]->printUsage();
    }
    k++;
  }
  cerr<<" [OPTION] ..."<<endl;
}


////////////////////////////////////////////////////////////////////////////////
// Affiche l'aide int�grale
////////////////////////////////////////////////////////////////////////////////
void Interpreter::printHelp() {
  printUse();
  if (strlen(m_aim)>0)
    cerr<<m_aim<<endl;
  if (strlen(m_example)>0)
    cerr<<m_example<<endl;
  cerr<<endl;
  cerr<<"Options:"<<endl;
  for (unsigned short i=0; i<m_nbOpt; i++)
    m_options[i]->print();
  cerr<<endl;
  if (strlen(m_contact)>0)
    cerr<<"Report bug to "<<m_contact<<"."<<endl;
}


////////////////////////////////////////////////////////////////////////////////
// Affiche le num�ro de version
////////////////////////////////////////////////////////////////////////////////
void Interpreter::printVersion() {
  cerr<<" "<<m_name<<" "<<m_version<<endl;
  cerr<<endl;
}



