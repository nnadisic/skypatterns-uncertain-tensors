///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//  File: option.h                                                           //
//  Last modification: 15/12/05                                              //
//  Class: Option                                                            //
//  Description: Options of cammand lines                                    //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

class Option {
public:
	Option();
	Option(const char [], char, const char []);
	Option(Option & );
	~Option();
	char * const getName();
	char getShortCut();
	char * const getHelp();
	void print();
	void setParameters(bool necessary, unsigned short minParam, unsigned short maxParam, const char nameParam[]="PARAM");
	void defaultParameters(unsigned short nbParam, char ** const parameters);
	bool isNecessary();
	void printUsage();
  int checkParam(unsigned short );
  void setRealParameters(unsigned short nbParam, char * const* parameters);
  char *getParameter(unsigned short param);
  int getInit();

private:
  int m_initialize;
	char m_name[256];
	char m_shortCut;
	char m_help[256];
	unsigned short m_minParam;
	unsigned short m_maxParam;
	unsigned short m_nbParam;
	char**  m_parameters;
	char* m_nameParam;
	bool m_necessary;
};

