///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//  File: option.cpp                                                         //
//  Last modification: 15/12/05                                              //
//  Class: Option                                                            //
//  Description: Options of cammand lines                                    //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "stdlib.h"
#include "string.h"
#include "option.h"
 
using namespace std;


///////////////////////////////////////////////////////////////////////////////
// Constructeurs
///////////////////////////////////////////////////////////////////////////////
Option::Option() {
	strcpy(m_name,"");
	m_shortCut=0;
	strcpy(m_help,"");
	m_minParam=0;
	m_maxParam=0;
	m_nbParam=0;
	m_parameters=NULL;
	m_nameParam=NULL;
	m_necessary=false;
	m_initialize=-1;
}

Option::Option(const char name[], char shortCut, const char help[]) {
	strcpy(m_name, name);
	m_shortCut=shortCut;
	strcpy(m_help, help);
	m_minParam=0;
	m_maxParam=0;
	m_nbParam=0;
	m_parameters=NULL;
	m_nameParam=NULL;
	m_necessary=false;
	m_initialize=-1;
}

Option::Option(Option & option) {
	strcpy(m_name, option.m_name);
	m_shortCut=option.m_shortCut;
	strcpy(m_help, option.m_help);
	m_minParam=option.m_minParam;
	m_maxParam=option.m_maxParam;
	m_nbParam=option.m_nbParam;
	m_parameters=NULL;
	m_parameters=new char* [m_maxParam];
	for (unsigned short i=0; i<m_nbParam; i++) {
		m_parameters[i]=new char [strlen(option.m_parameters[i])+1];
		strcpy(m_parameters[i],option.m_parameters[i]);
	}		
	m_nameParam=new char [strlen(option.m_nameParam)+1];
	strcpy(m_nameParam,option.m_nameParam);
	m_necessary=option.m_necessary;
	m_initialize=option.m_initialize;
}


///////////////////////////////////////////////////////////////////////////////
// Destructeurs
///////////////////////////////////////////////////////////////////////////////
Option::~Option() {
	if (m_maxParam>0) {
		for (unsigned short i=0; i<m_nbParam; i++)
			delete m_parameters[i];
		delete m_parameters;
		m_parameters=NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////
// Fixer les diff�rents champs de l'option
///////////////////////////////////////////////////////////////////////////////

// Le nom
char * const Option::getName() {
	return m_name;
}

// le raccourci
char Option::getShortCut() {
	return m_shortCut;
}

// l'aide
char * const Option::getHelp() {
	return m_help;
}

// tous sur les param�tres
void Option::setParameters(bool necessary, unsigned short minParam, unsigned short maxParam, const char nameParam[]) {
	m_necessary=necessary;
	m_minParam=minParam;
	m_maxParam=maxParam;
	m_parameters=new char* [m_maxParam];
	m_nameParam=new char [strlen(nameParam)+1];
	strcpy(m_nameParam,nameParam);
}

// Fixer les param�tres
void Option::setRealParameters(unsigned short nbParam, char *const *parameters) {
	if (nbParam>m_maxParam) {
		cerr<<"Bad initialization of default parameters."<<endl;
	}
	m_nbParam=nbParam;
	for (unsigned short i=0; i<m_nbParam; i++) {
		m_parameters[i]=new char [strlen(parameters[i])+1];
		strcpy(m_parameters[i], parameters[i]);
	}
	m_initialize=nbParam;
}


///////////////////////////////////////////////////////////////////////////////
// R�cup�rer un param�tre
///////////////////////////////////////////////////////////////////////////////
char *Option::getParameter(unsigned short param) {
  if (param>m_maxParam) {
    cerr<<"This parameter is not initialized."<<endl;
    return NULL;
  }
  return m_parameters[param];
}

int Option::getInit() {
  return m_initialize;
}


///////////////////////////////////////////////////////////////////////////////
// Affichages
///////////////////////////////////////////////////////////////////////////////

// pour l'aide int�grale
void Option::print() {
	unsigned short length=0;
	cerr<<" -"<<m_shortCut<<", --"<<m_name<<" ";
	for (unsigned short i=0; i<m_minParam; i++) {
		cerr<<"<"<<m_nameParam<<"> ";
		length += 3+strlen(m_nameParam);
	}
	if (m_maxParam>m_minParam) {
		cerr<<"... ";	
		length += 4;
	}
	for (unsigned short i=0; i<35-(strlen(m_name)+8+length); i++)
		cerr<<" ";
	cerr<<m_help<<endl;
}

// l'usage de l'option
void Option::printUsage() {
  for (unsigned short i=0; i<m_minParam; i++)
    cerr<<"<"<<m_nameParam<<"> ";
  if (m_maxParam>m_minParam)
    cerr<<"... ";
}


///////////////////////////////////////////////////////////////////////////////
// Quelques v�rifications
///////////////////////////////////////////////////////////////////////////////

// v�rifie que l'utilisateur donne le bon nombre d'argument
int Option::checkParam(unsigned short nbParam) {
  if (nbParam<m_minParam)
    return -1;
  if (nbParam>m_maxParam)
    return 1;
  return 0;
}

// v�rifie que le programmeur donne le bon nombre d'argument
// !!!!!!! NON TESTE !!!!!!
void Option::defaultParameters(unsigned short nbParam, char ** const parameters) {
	if (nbParam>m_maxParam) {
		cerr<<"Bad initialization of default parameters."<<endl;
	}
	m_nbParam=nbParam;
	for (unsigned short i=0; i<m_nbParam; i++) {
		m_parameters[i]=new char [strlen(parameters[i])+1];
		strcpy(m_parameters[i], parameters[i]);
	}
}

// retourne vraie si l'option est n�cessaire sur une ligne de commande
bool Option::isNecessary() {
	return m_necessary;
}

