#include <iostream>
#include <fstream>
#include <string>
#include "stdlib.h"
#include "math.h"
#include "global.h"
#include "info.h"
#include "miner.h"

using namespace std;


Info::Info() {
  m_out=&cout;
  m_verbose=0;
  m_descendant=false;
  m_statistics=false;
}


Info::~Info() {
}

void Info::setOut(ostream * out) {
  m_out=out;
}

ostream* Info::getOut() {
  return m_out;
}
void Info::setVerbose(unsigned char verbose) {
  m_verbose=verbose;
}

unsigned char Info::getVerbose() {
  return m_verbose;
}

void Info::setStatistics(bool statistics) {
  m_statistics=statistics;
}

bool Info::getStatistics() {
  return m_statistics;
}

void Info::setDescendant() {
  m_descendant=true;
}

bool Info::isDescendant() {
  return m_descendant;
}
