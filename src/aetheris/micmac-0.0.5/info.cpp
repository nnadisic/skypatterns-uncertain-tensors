/******************************************************
 
  info.cpp
  author: Arnaud Soulet
  last update: 23/09/08
 
******************************************************/

using namespace std;

#include <iostream>
#include <fstream>
#include <sstream>
#include <ostream>
#include "string.h"
#include "stdlib.h"
#include "math.h"
#include "global.h"
#include "values/values.h"
#include "info.h"


Info::Info(char* name) {
  m_dataset=NULL;
  cerr<<"Loading "<<name<<endl;
  readDataset(name); // Initialise le format du jeu de donn�es
  loadDataset(name); // Charge le jeu de donn�es
  m_gamma=1;
  m_out=&cout;
  m_verbose=0;
  m_values=NULL;
  m_pattern=0;
  m_class=0;
  m_nbClass=0;
}

Info::Info() {
  m_dataset=NULL;
  m_gamma=1;
  m_out=&cout;
  m_verbose=0;
  m_values=NULL;
  m_pattern=0;
  m_class=0;
  m_nbClass=0;
}

void Info::setDataset(char* dataset) {
  if (m_verbose>=1)
    cerr<<"Loading "<<dataset<<endl;
  readDataset(dataset); // Initialise le format du jeu de donn�es
  loadDataset(dataset); // Charge le jeu de donn�es
}


Info::~Info() {
  if (m_dataset!=NULL) {
    for (OBJ i=0; i<m_nbObj; i++)
      if (m_dataset[m_nbObj]!=NULL) {
	delete m_dataset[i];
	m_dataset[i]=NULL;
      }
    delete m_dataset;
    m_dataset=NULL;
  }
}

///////////////////////////////////////////////////////////////////////////////
// Gestion du jeu de donn�es
///////////////////////////////////////////////////////////////////////////////

// Initialise le format du jeu de donn�es
void Info::readDataset(char *name) {
  char *buffer=new char [MAX_TRANS];
  m_nbObj=0;
  m_nbAtt=0;
  fstream file(name);
  while(!file.eof()) {
    file.getline(buffer,MAX_TRANS,'\n');
    if ((strlen(buffer)>0) && (buffer[0]!='#')) {
      m_nbObj++;
      unsigned long i=0, j=0;
      while ((buffer[i]!='\0') && (buffer[i]!='\n')) {
	if (buffer[i]==' ') {
	  buffer[i]='\0';
	  ATT tmp=atoi(&buffer[j]);
	  if (tmp>m_nbAtt)
	    m_nbAtt=tmp;
	  j=i+1;
	}
	i++;
      }
      buffer[i]='\0';
      ATT tmp=atoi(&buffer[j]);
      if (tmp>m_nbAtt)
	m_nbAtt=tmp;
    }
  }
  file.close();
  m_dataset=new bool* [m_nbObj];
  for (OBJ i=0; i<m_nbObj; i++)
    m_dataset[i]=new bool [m_nbAtt+1];
  for (OBJ i=0; i<m_nbObj; i++)
    for (ATT j=0; j<m_nbAtt+1; j++)
      m_dataset[i][j]=false;
  delete buffer;
}

// Charge le jeu de donn�es
void Info::loadDataset(char *name) {
  OBJ obj=0;
  char *buffer=new char [MAX_TRANS];
  fstream file(name,ios::in);
  while(!file.eof()) {
    file.getline(buffer,MAX_TRANS,'\n');
    if ((strlen(buffer)>0) && (buffer[0]!='#')) {
      unsigned long i=0, j=0;
      while ((buffer[i]!='\0') && (buffer[i]!='\n')) {
	if (buffer[i]==' ') {
	  buffer[i]='\0';
	  ATT tmp=atoi(&buffer[j]);
	  if (tmp>0)
	    m_dataset[obj][tmp]=true;
	  j=i+1;
	}
	i++;
      }
      buffer[i]='\0';
      ATT tmp=atoi(&buffer[j]);
      if (tmp>0)
	m_dataset[obj][tmp]=true;
      ATT c=1;
      while (!m_dataset[obj][c])
	c++;
      if (c>m_nbClass)
	m_nbClass=c;
      obj++;
    }
  }
  file.close();
  delete buffer;
}

// Affiche le jeu de donn�es
void Info::showDataset() {
  for (OBJ i=0; i<m_nbObj; i++) {
    for (ATT j=1; j<m_nbAtt+1; j++)
      if (m_dataset[i][j]==true)
	cerr<<j<<" ";
    cerr<<endl;
  }
}

// Retourne le jeu de donn�es
bool ** Info::getDataset() {
  return m_dataset;
}

ATT Info::getNbAtt() {
  return m_nbAtt;
}

OBJ Info::getNbObj() {
  return m_nbObj;
}

OBJ Info::getGamma() {
  return m_gamma;
}

void Info::setGamma(OBJ gamma) {
  m_gamma=gamma;
}

void Info::setOut(ostream * out) {
  m_out=out;
}

ostream* Info::getOut() {
  return m_out;
}
void Info::setThreshold(float threshold) {
  m_gamma=(int)ceil(m_nbObj*threshold);
  if (m_verbose>=2)
    cerr<<"Minimal frequency threshold: "<<m_gamma<<endl;
}

void Info::setVerbose(unsigned char verbose) {
  m_verbose=verbose;
}

unsigned char Info::getVerbose() {
  return m_verbose;
}

void Info::setValues(Values* values) {
  m_values=values;
}

Values* Info::getValues() {
  return m_values;
}

void Info::setPattern(unsigned int pattern) {
  m_pattern=pattern;
}

unsigned int Info::getPattern() {
  return m_pattern;
}

void Info::setClass(unsigned int c) {
  m_class=c;
}

unsigned int Info::getClass() {
  return m_class;
}

unsigned int Info::getNbClass() {
  return m_nbClass;
}







