/******************************************************
 
  main.cpp
  author: Arnaud Soulet
  last update: 23/09/08
 
******************************************************/

using namespace std;

#include <iostream>
#include <fstream>
#include "string.h"
#include "stdlib.h"
#include "math.h"
#include "global.h"
#include "interpreter/option.h"
#include "interpreter/interpreter.h"
#include "values/values.h"
#include "info.h"
#include "node.h"
#include "miner.h"

// Initialisation de l'interpr�teur des commandes
void initialize(Interpreter *interpreter) {
  interpreter->setInformations(
			      "Mining adequate condensed representations",
			      "Example: micmac -i dataset.bin -a min values.att",
			      "<arnaud.soulet@univ-tours.fr>");
  // Aide
  Option* help=new Option("help",'h',"give this message");
  interpreter->addOption(help);
  // Version
  Option* version=new Option("version",'v',"give the version number");
  interpreter->addOption(version);
  // Input
  Option* input=new Option("input",'i',"dataset to mine");
  input->setParameters(true,1,1,"FILE");
  interpreter->addOption(input);
  // Output
  Option* output=new Option("output",'o',"constrained patterns");
  output->setParameters(false,1,1,"FILE");
  interpreter->addOption(output);
  // Seuil minimal de fr�quence absolu
  Option* gamma=new Option("gamma",'g',"minimal absolute frequency threshold");
  gamma->setParameters(false,1,1,"NUMBER");
  interpreter->addOption(gamma);
  // Seuil minimal de fr�quence relatif
  Option* threshold=new Option("threshold",'t',"minimal frequency threshold");
  threshold->setParameters(false,1,1,"[0,1]");
  interpreter->addOption(threshold);
  // Verbose
  Option* verbose=new Option("verbose",'V',"verbose mode");
  verbose->setParameters(false,0,1,"NUMBER");
  interpreter->addOption(verbose);
  // Class
  Option* cl=new Option("class",'c',"class-aware mining");
  cl->setParameters(false,0,1,"NUMBER");
  interpreter->addOption(cl);
  // Agr�gat
  Option* aggregate=new Option("aggregate",'a',"aggregate and values");
  aggregate->setParameters(false,1,20,"AGG> <VAL");
  interpreter->addOption(aggregate);
  // option -p: different kind of patterns
  Option* pattern=new Option("pattern",'p',"0 = hybrid / 1 = frequent / 2 = free / 3 = closed");
  pattern->setParameters(false,0,1,"NUMBER");
  interpreter->addOption(pattern);

  interpreter->setHV('h','v');
}

int main (int argc, char * const argv[]) {
  Interpreter interpreter("micmac","0.0.5");
  initialize(&interpreter);
  if (interpreter.run(argc, argv)==1)
    return 1;

  // option -o
  ostream* out=&cout;
  if (interpreter.getOption('o')->getInit()>0) {
    out=new ofstream(interpreter.getOption('o')->getParameter(0),ios::out);
    if (out==&cout)
      cerr<<"music: file error"<<endl;
  }

  // option -t
  float threshold=-1;
  if (interpreter.getOption('t')->getInit()>0)
    threshold=(float)atof(interpreter.getOption('t')->getParameter(0));

  // option -g
  int gamma=0;
  if (interpreter.getOption('g')->getInit()>0)
    gamma=(int)atoi(interpreter.getOption('g')->getParameter(0));

  // option -V
  unsigned char verbose=0;
  if (interpreter.getOption('V')->getInit()>=0) {
    if (interpreter.getOption('V')->getInit()==1)
      verbose=(int)atoi(interpreter.getOption('V')->getParameter(0));
    else
      verbose=1;
  }

  // option -c
  unsigned char cl=0;
  if (interpreter.getOption('c')->getInit()>=0) {
    if (interpreter.getOption('c')->getInit()==1)
      cl=(int)atoi(interpreter.getOption('c')->getParameter(0));
    else
      cl=1;
  }

  //option -p
  unsigned char pattern=0;
  if (interpreter.getOption('p')->getInit()>=0) {
    if (interpreter.getOption('p')->getInit()==1)
      pattern=(int)atoi(interpreter.getOption('p')->getParameter(0));
    else
      pattern=1;
  }

  Info* info=new Info();
  info->setVerbose(verbose);
  info->setClass(cl);
  info->setPattern(pattern);
  info->setDataset(interpreter.getOption('i')->getParameter(0));

  ///////////////////////////////////////////////////////////////
  // option -a
  Values* values=NULL;
  if (interpreter.getOption('a')->getInit()>0) {
    unsigned char param=interpreter.getOption('a')->getInit();
    if (param%2!=0) {
      cerr<<"micmac : bad number of parameters for -a"<<endl;
      return 1;
    }
    values=new Values(param/2,info->getNbAtt());
    for (unsigned char i=0; i<param/2; i++)
      values->addValues(interpreter.getOption('a')->getParameter(i*2),interpreter.getOption('a')->getParameter(i*2+1));
  }
  else
    values=new Values(0,0);
  info->setValues(values);
  ///////////////////////////////////////////////////////////////

  info->setOut(out);
  if (gamma>0)
    info->setGamma(gamma);
  if (threshold>=0)
    info->setThreshold(threshold);

  Miner* miner=new Miner(info);
  miner->run();
  delete miner;
  delete info;
  return 0;
}
