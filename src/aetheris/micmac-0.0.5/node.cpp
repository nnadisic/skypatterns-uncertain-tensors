/******************************************************
 
  node.cpp
  author: Arnaud Soulet
  last update: 23/09/08
 
******************************************************/

using namespace std;

#include <iostream>
#include <fstream>
#include "string.h"
#include "stdlib.h"
#include "math.h"
#include <sstream>
#include "global.h"
#include "values/values.h"
#include "info.h"
#include "node.h"

static bool ** m_dataset;
static ATT m_nbAtt;
static OBJ m_nbObj;
static Node* m_rootCand;
static OBJ m_gamma;
static ostream* m_out;
static Values* m_values;
static unsigned char m_nbAgg;

static ATT m_level; // niveau de travail
static ATT* attributes; // attributs de travail

static unsigned int m_pattern=0;

static unsigned int m_nbClass=0;
static unsigned int m_class=0;

Node::Node() {
  m_bottom=NULL;
  m_right=NULL;
  m_closure=NULL;
  m_count=0;
  m_aggregates=NULL;
  m_countClass=0;
}

Node::Node(ATT attribute, Node* bottom, Node* right) {
  m_attribute=attribute;
  m_bottom=bottom;
  m_right=right;
  m_count=0;
  m_closure=NULL;
  m_aggregates=NULL;
  m_countClass=0;
}

Node::~Node() {
  if (m_bottom)
    delete m_bottom;
  if (m_right)
    delete m_right;
  if (m_closure)
    delete m_closure;
  if (m_aggregates)
    delete m_aggregates;
}

void Node::init(Info* info) {
  m_dataset=info->getDataset();
  m_nbAtt=info->getNbAtt();
  m_nbObj=info->getNbObj();
  m_rootCand=NULL;
  attributes=new ATT [m_nbAtt];
  m_level=1;
  m_gamma=info->getGamma();
  m_out=info->getOut();
  m_values=info->getValues();
  m_nbAgg=m_values->getNbAgg();
  m_pattern=info->getPattern();
  m_class=info->getClass();
  if (m_class>0)
    m_nbClass=info->getNbClass();
  else
    m_nbClass=0;
}


void Node::generateInit() {
  for (int i=m_nbAtt; i>m_nbClass; i--) {
    m_rootCand=new Node(i, NULL, m_rootCand);

    // aggregate
      m_rootCand->m_aggregates=new float [m_nbAgg];
      for (unsigned char j=0; j<m_nbAgg; j++)
	m_rootCand->m_aggregates[j]=m_values->getValue(j,i);
  }
}

////////////////////////////////////////////////////////////////////////////////
// Scan de la base
////////////////////////////////////////////////////////////////////////////////

static bool* m_dataObj;

//initialise la cloture
Node* Node::initClosure(ATT iObj, ATT iPat, int level) {
  if (iObj==m_nbAtt+1)
    return NULL;
  if ((iPat>=level)||(iObj<attributes[iPat])) {
    if (m_dataObj[iObj]) {
      for (unsigned char j=0; j<m_nbAgg; j++) {
	switch ((int)m_values->getAggregate(j)) {
	case 1 : { 
	  if (m_values->getValue(j,iObj)<m_aggregates[j])
	  return initClosure(iObj+1, iPat, level);
	    break;}
	case 2 : { 
	  if (m_values->getValue(j,iObj)>m_aggregates[j])
	  return initClosure(iObj+1, iPat, level);
	  break; }
	}
      }
      if (iObj>m_nbClass)
	return new Node(iObj, initClosure(iObj+1, iPat, level), NULL);
      else
	return initClosure(iObj+1, iPat, level);
    }
    return initClosure(iObj+1, iPat, level);
  }
  return initClosure(iObj+1, iPat+1, level);
}

Node* Node::checkClosure() {
  Node* previous=NULL;
  Node* current=m_closure;
  while (current) {
    if (!m_dataObj[current->m_attribute]) {
      Node* bottom=current->m_bottom;
      current->m_bottom=NULL;
      delete current;
      if (previous)
	previous->m_bottom=bottom;
      else
	m_closure=bottom;
      current=bottom;
    }
    else {
      previous=current;
      current=current->m_bottom;
    }
  }
}


// Fonction principale
void Node::scan(int level) {
  m_level=level;
  for (OBJ i=0; i<m_nbObj; i++) {
    m_dataObj=&m_dataset[i][0];
    m_rootCand->scanObject(1);
  }
}

// Fonction auxiliaire
void Node::scanObject(int k) {
  if (m_dataObj[m_attribute]) {
    attributes[k-1]=m_attribute;
    if (!m_bottom) {// le motif est pr�sent dans l'objet
      if (k==m_level) {
	if (m_count==0)
	  m_closure=initClosure(1,0,k);
	else
	  checkClosure();
	m_count++; // on augmente sa fr�quence
	if (m_nbClass>0) {
	  ATT c=1;
	  while (!m_dataObj[c])
	    c++;
	  if (c==m_class)
	    m_countClass++;
	}	  
      }
    }
    else
      m_bottom->scanObject(k+1); // on cherche la fin du motif
  }
  if (m_right!=NULL)
    m_right->scanObject(k);
}

// g�n�ration des candidats de longueur n+1
void Node::generateCand(int level) {
  m_level=level;
  m_rootCand->generate(0);
}

// v�rifie que tous les sous-motifs sont pr�sents
bool Node::subPattern(int k) {
  ATT j=0;
  while (j<k) { // test des sous-motifs du candidat
    Node* current=m_rootCand;
    ATT l=0;
    while (l<=k+1) { // passer tous les attributs du sous-motif
      if (l==j)
	l++;
      while ((current)&&(current->m_attribute<attributes[l])) { //se d�placer dans l'arbre
	current=current->m_right;
      }
      if ((!current)||(current->m_attribute>attributes[l]))
	return false;
      // item attributes[j] dans la fermeture du motif sans le j�me item?
      Node* closure=current->m_closure;
      while ((closure)&&(closure->m_attribute<attributes[j]))
	closure=closure->m_bottom;
      if ((l==k+1)&&(closure)&&(closure->m_attribute==attributes[j])) // test de libert� !!!
	return false;
      current=current->m_bottom;
      l++;
    }
    j++;
  }
  return true;
}

void Node::generate(int k) {
  attributes[k]=m_attribute;
  if (k+1==m_level) { // on voit ce que l'on peut g�n�rer avec ce motif
    Node* right=m_right; // prochain g�n�rateur
    Node* insert=NULL; // position d'insertion
    Node* closure=m_closure;
    while (right) { // Pour chaque candidat...
      attributes[k+1]=right->m_attribute; // construction du candidat
      while ((closure)&&(closure->m_attribute<attributes[k+1]))
	closure=closure->m_bottom;
      Node* clos=right->m_closure;
      while ((clos)&&(clos->m_attribute<m_attribute))
	clos=clos->m_bottom;
      bool isFree=(((!clos)||(clos->m_attribute!=m_attribute))&&((!closure)||(closure->m_attribute!=attributes[k+1]))&&(subPattern(k)));
      if (m_pattern==1)
	isFree=true;
      if (isFree) {
      //if (((!clos)||(clos->m_attribute!=m_attribute))&&((!closure)||(closure->m_attribute!=attributes[k+1]))&&(subPattern(k))) {
	  Node* tmpInsert=new Node(right->m_attribute, NULL, NULL);
	  // aggregate
	  tmpInsert->m_aggregates=new float [m_nbAgg];
	  for (unsigned char j=0; j<m_nbAgg; j++) {
	    switch ((int)m_values->getAggregate(j)) {
	    case 1 : {
	    if (m_aggregates[j]>right->m_aggregates[j])
	      tmpInsert->m_aggregates[j]=right->m_aggregates[j];
	    else
	      tmpInsert->m_aggregates[j]=m_aggregates[j];
	    break;
	    }
	    case 2 : {
	    if (m_aggregates[j]<right->m_aggregates[j])
	      tmpInsert->m_aggregates[j]=right->m_aggregates[j];
	    else
	      tmpInsert->m_aggregates[j]=m_aggregates[j];
	    break;
	    }
	    }
	  }
	if (insert) {
	  insert->m_right=tmpInsert;
	  insert=insert->m_right;
	  //insert->count(k+2); // count rapide
	}
	else {
	  m_bottom=tmpInsert;
	  insert=m_bottom;
	  //insert->count(k+2); // count rapide
	}
      }
      right=right->m_right;
    }
  }
  else { // voir dessous
    if (m_bottom)
      m_bottom->generate(k+1);
  }
  if (m_right) { // voir � droite
    m_right->generate(k);
  }
}

// Elagage
void Node::pruneCand(int level) {
  m_level=level;
  m_rootCand=m_rootCand->prune(1);
}

// Elagage fnct auxiliaire
Node* Node::prune(int k) {
  attributes[k-1]=m_attribute;
  if ((k<m_level)&&(m_closure)) {
    delete m_closure;
    m_closure=NULL;
  }
  if (!m_bottom) {
    if (k==m_level) { // on est au bon niveau d'�lagage (motif de longueur k)
      if (m_count<m_gamma) { // motif infr�quent
	Node* right=m_right;
	m_right=NULL;
	delete this;
	if (right)
	  return right->prune(k);
	else
	  return NULL;
      }
      else {
	// affichage ligne
#ifndef TEST
	// motif //
	if (m_pattern!=3) {
	  for (ATT i=0; i<k; i++) // affichage motif
	    (*m_out)<<attributes[i]<<" ";
	  if ((m_closure)&&(m_pattern==0)) { // affichage cloture
	    (*m_out)<<", ";
	    Node* current=m_closure;
	    while (current) {
	      (*m_out)<<current->m_attribute<<" ";
	      current=current->m_bottom;
	    }
	  }
	}
	else {
	  unsigned int i=0;
	  Node* current=m_closure;
	  while ((i<k)||(current)) {
	    if (current)
	      if (i<k)
		if (attributes[i]<current->m_attribute) {
		  (*m_out)<<attributes[i]<<" ";
		  i++;
		}
		else {
		  (*m_out)<<current->m_attribute<<" ";
		  current=current->m_bottom;
		}
	      else {
		  (*m_out)<<current->m_attribute<<" ";
		  current=current->m_bottom;
	      }
	    else {
		  (*m_out)<<attributes[i]<<" ";
		  i++;
	    }
	  }
	}
	// mesures //
	(*m_out)<<": "<<m_count; // affichage fr�quence
	if (m_nbClass>0)
	  (*m_out)<<" : "<<m_countClass;
	if ((m_nbAgg>0)&&(m_aggregates))
	  for (unsigned char j=0; j<m_nbAgg; j++)
	    (*m_out)<<" : "<<m_aggregates[j];
	(*m_out)<<endl;
#endif
      }
    }
  }
  else {
    m_bottom=m_bottom->prune(k+1);
  }
  if (m_right) {
    m_right=m_right->prune(k);
  }
  if ((!m_bottom)&&(k<m_level)) {
    Node* right=m_right;
    m_right=NULL;
    delete this;
    return right;
  }
  return this;
}

// Pour savoir s'il reste des candidats
bool Node::isEnd() {
   return (m_rootCand==NULL);
}

