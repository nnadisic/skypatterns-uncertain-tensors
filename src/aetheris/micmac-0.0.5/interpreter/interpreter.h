///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//  File: interpreter.h                                                      //
//  Last modification: 15/12/05                                              //
//  Class: Interpreter                                                       //
//  Description: Syntaxic analyser of cammand lines                          //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

class Interpreter {
public:
	Interpreter(const char name[], const char version[]);
	Interpreter(Interpreter & inter);
	~Interpreter();
	unsigned short run(int argc, char * const argv[]);
	void addOption(Option *);
	void setInformations(const char [], const char [], const char []);
	Option* getOption(char shortCut);
	Option* getOption(char * const name);
	void setHV(char help, char version);
private:
	bool isOption(char * const param);
	Option* readOption(char * const param);
	void printHelp();
	void printVersion();
	void printUse();
	void printDefault();
	char m_name[256];
	char m_version[256];
	char m_example[256];
	char m_contact[256];
	char m_aim[256];
	Option** m_options;
	unsigned short m_nbOpt;
	unsigned short m_nbOptMax;
	char m_versionSC;
	char m_helpSC;
};

