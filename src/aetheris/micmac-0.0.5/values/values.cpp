/******************************************************
 
  values.cpp
  author: Arnaud Soulet
  last update: 23/09/08
 
******************************************************/

#include "stdlib.h"
#include "string.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include "../global.h"
#include "values.h"

using namespace std;

Values::Values(unsigned char nbAgg, unsigned char nbAtt) {
  m_nbAgg=nbAgg;
  m_nbAtt=nbAtt;
  m_values=new float* [nbAgg];
  m_aggregates=new unsigned char [nbAgg];
  for (unsigned char i=0; i<m_nbAgg; i++)
    m_values[i]=new float [m_nbAtt+1];
  m_index=0;
}

Values::~Values() {
  if (m_aggregates)
    delete m_aggregates;
  if (m_values) {
    for (unsigned char i=0; i<m_nbAgg; i++)
      delete m_values[i];
    delete m_values;
  }
}

float Values::getValue(unsigned char agg, ATT att) {
  return m_values[agg][att];
}

unsigned char Values::getAggregate(unsigned char agg) {
  return m_aggregates[agg];
}

void Values::addValues(char* agg, char* name) {
  unsigned char kind=0;
  if (strcmp(agg,"min")==0)
      kind=1;
  if (strcmp(agg,"max")==0)
      kind=2;
  if (kind==0)
    cerr<<"warning : wrong aggregate funtion"<<endl;
  else { // lecture des valeurs associ�es � l'aggr�gat
    m_aggregates[m_index]=kind;
    ifstream file(name, ios::in);
    char *buffer=new char [MAX_TRANS];
    ATT k=1;
    while (!file.eof()) {
      file.getline(buffer,MAX_TRANS,'\n');
      if ((strlen(buffer)>0) && (buffer[0]!='#')) {
	if (k<=m_nbAtt)
	  m_values[m_index][k++]=(float)atof(buffer);
	else
	  k++;
      }
    }
    if (k<m_nbAtt+1)
      cerr<<"warning : too less values in the "<<(int)m_index+1<<"th file"<<endl;
    if (k>m_nbAtt+1)
      cerr<<"warning : too many values in the "<<(int)m_index+1<<"th file"<<endl;
    delete buffer;
  }
  m_index++;
}

void Values::showValues(unsigned char agg) {
  for (ATT i=0; i<=m_nbAtt; i++)
    cerr<<m_values[agg][i]<<" ";
  cerr<<endl;
}

unsigned char Values::getNbAgg() {
  return m_nbAgg;
}
