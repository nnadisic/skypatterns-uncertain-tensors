/******************************************************
 
  values.h
  author: Arnaud Soulet
  last update: 23/09/08
 
******************************************************/

#include <iostream>
#include <sstream>
#include <fstream>
#include "string.h"
#include "stdlib.h"
#include "../global.h"

class Values {
 public:
  Values(unsigned char, unsigned char);
  ~Values();
  unsigned char getNbAgg();
  float getValue(unsigned char, ATT);
  unsigned char getAggregate(unsigned char);
  void addValues(char*, char*);
  void showValues(unsigned char);
 private:
  float** m_values;
  unsigned char *m_aggregates;
  ATT m_nbAtt;
  unsigned char m_nbAgg;
  unsigned char m_index;
};
