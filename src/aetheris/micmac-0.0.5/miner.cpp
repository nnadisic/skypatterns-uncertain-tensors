/******************************************************
 
  miner.cpp
  author: Arnaud Soulet
  last update: 23/09/08
 
******************************************************/

using namespace std;

#include <iostream>
#include <fstream>
#include "string.h"
#include "stdlib.h"
#include "math.h"
#include <sstream>
#include "global.h"
#include "values/values.h"
#include "info.h"
#include "node.h"
#include "miner.h"

static unsigned char m_verbose; 
static ostream* m_out;

Miner::Miner(Info* info) {
  Node* tmpNode=new Node();
  tmpNode->init(info);
  delete tmpNode;
  m_verbose=info->getVerbose();
  m_out=info->getOut();
}

// MicMac algorithm
void Miner::run() {
  (*m_out)<<"# micmac 0.0.5"<<endl;
  Node* tmpNode=new Node();
  tmpNode->generateInit();
  tmpNode->scan(1);
  tmpNode->pruneCand(1);
  int k=1;
  while (!tmpNode->isEnd()) {
    if (m_verbose==1)
      cerr<<"level "<<k<<endl;
    // Generating candidates
    if (m_verbose>=2)
      cerr<<"generate "<<k<<endl;
    tmpNode->generateCand(k);
    // Scanning the dataset
    if (m_verbose>=2)
      cerr<<"scan "<<k<<endl;
    tmpNode->scan(k+1);
    // Pruning non-frequent itemsets
    if (m_verbose>=2)
      cerr<<"prune "<<k<<endl;
    tmpNode->pruneCand(k+1);
    k++;
  }
  delete tmpNode;
}
