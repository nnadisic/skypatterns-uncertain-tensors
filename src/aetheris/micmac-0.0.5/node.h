/******************************************************
 
  node.h
  author: Arnaud Soulet
  last update: 23/09/08
 
******************************************************/

class Node {
public:
  Node();
  Node(ATT attribute, Node* bottom=NULL, Node* right=NULL);
  ~Node();
  void init(Info*);
  void generateInit();
  void generateCand(int );
  void scan(int );
  void pruneCand(int);
  bool isEnd();
private:
  Node* initClosure(ATT , ATT , int );
  Node* checkClosure();
  void scanObject(int);
  void generate(int);
  Node* prune(int);
  bool subPattern(int);

  Node* m_bottom;
  Node* m_right;
  Node* m_closure;
  OBJ m_count;
  ATT m_attribute;
  float* m_aggregates;
  OBJ m_countClass;
};
