/******************************************************
 
  info.h
  author: Arnaud Soulet
  last update: 23/09/08
 
******************************************************/

class Info {
public:
  Info(char* );
  Info();
  ~Info();
  void showDataset();
  void setDataset(char*);
  bool **getDataset();
  ATT getNbAtt();
  OBJ getNbObj();
  void setGamma(OBJ);
  OBJ getGamma();
  void setOut(ostream*);
  ostream* getOut();
  void setThreshold(float);
  void setVerbose(unsigned char);
  unsigned char getVerbose();
  void setValues(Values*);
  Values* getValues();
  void setPattern(unsigned int);
  unsigned int getPattern();
  void setClass(unsigned int);
  unsigned int getClass();
  unsigned int getNbClass();
private:
  void readDataset(char *);
  void loadDataset(char *);

  bool **m_dataset;
  ATT m_nbAtt;
  OBJ m_nbObj;
  OBJ m_gamma;
  ostream* m_out;
  unsigned char m_verbose;
  Values* m_values;
  unsigned int m_pattern;
  unsigned int m_class;
  unsigned int m_nbClass;
};
