#!/bin/bash

DATA=data/*.bin
DIR="res/$(eval date -u +'%y-%m-%d-%H-%M-%S')"

mdh=bin/multidupehack
twodh=bin/2dupehack
aeth=bin/aetheris.sh
dp=bin/dp
cpsky=bin/CP+SKY

TIMEOUT="2h"

mkdir -p $DIR


for dataset in $DATA; do
    dataset="${dataset%.*}"
    dataname=`basename $dataset`
    resfile="$DIR/$dataname.res"
    outtim=/tmp/time
    outpat=/tmp/patterns

    echo $dataname
    echo -e "algorithm\ttime\tmemory\tpatterns" > $resfile

    # 2dupehack
    timeout $TIMEOUT /usr/bin/time -o $outtim -f "%e\t%M" $twodh --sky-s "0" --sky-a -o $outpat $dataset.mdh > /dev/null 2>&1
    echo -en "----2dupehack\t" >> $resfile
    ex=$?
    if [ $ex -eq 124 ]; then # timeout
    	echo -e ">$TIMEOUT\t-\t-"
    else
    	cat $outtim | tr -d '\n' >> $resfile
    	echo -ne "\t" >> $resfile
    	cat $outpat | wc -l >> $resfile
    fi

    # multidupehack
    timeout $TIMEOUT /usr/bin/time -o $outtim -f "%e\t%M" $mdh --sky-s "0" --sky-a -o $outpat $dataset.mdh > /dev/null 2>&1
    echo -en "multidupehack\t" >> $resfile
    ex=$?
    if [ $ex -eq 124 ]; then # timeout
    	echo -e ">$TIMEOUT\t-\t-"
    else
    	cat $outtim | tr -d '\n' >> $resfile
    	echo -ne "\t" >> $resfile
    	cat $outpat | wc -l >> $resfile
    fi

    # Aetheris
    timeout $TIMEOUT /usr/bin/time -o $outtim -f "%e\t%M" $aeth $dataname.bin 1 > $outpat 2>/dev/null
    echo -en "-----aetheris\t" >> $resfile
    ex=$?
    if [ $ex -eq 124 ]; then # timeout
	echo -e ">$TIMEOUT\t-\t-"
    else
	cat $outtim | tr -d '\n' >> $resfile
	echo -ne "\t" >> $resfile
	cat $outpat | sed '/^\s*$/d' | wc -l >> $resfile
    fi

    # dp
    timeout $TIMEOUT /usr/bin/time -o $outtim -f "%e\t%M" $dp skyline $dataset.bin > $outpat
    echo -en "-----------dp\t" >> $resfile
    ex=$?
    if [ $ex -eq 124 ]; then # timeout
    	echo -e ">$TIMEOUT\t-\t-"
    else
    	cat $outtim | tr -d '\n' >> $resfile
    	echo -ne "\t" >> $resfile
    	cat $outpat | sed -e '/^N/d' -e '/^|/d' | wc -l >> $resfile
    fi

    # CP+Sky
    timeout $TIMEOUT /usr/bin/time -o $outtim -f "%e\t%M" $cpsky -data $dataset.txt -m fa -p > $outpat
    echo -en "-------CP+SKY\t" >> $resfile
    ex=$?
    if [ $ex -eq 124 ]; then # timeout
    	echo -e ">$TIMEOUT\t-\t-"
    else
    	cat $outtim | tr -d '\n' >> $resfile
    	echo -ne "\t" >> $resfile
    	cat $outpat | sed -e '/^P/d' | wc -l >> $resfile
    fi

done
