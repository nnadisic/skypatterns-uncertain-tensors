#!/bin/bash

# To be executed with ../src/aetheris/*.bin

# Performance governor for cpufreq settings
# for i in $(seq 0 $(expr $(getconf _NPROCESSORS_ONLN) - 1))
# do
#     sudo cpufreq-set -c $i -g performance
# done

# Headers for totals
printf '# dataset patterns time\n'

TMP=`mktemp -t mine.sh.XXXXXX`
trap "rm $TMP* 2>/dev/null" 0

# Write the file in argument of sky-gr option
printf '0 1' > $TMP

for dataset in "$@"
do
    dataname=$(basename $dataset)
    # Headers for the results
    printf '# class patterns time\n' > "$dataset.res"
    # Create the group files
    awk -v out=$TMP.group 'NR != 0 { printf "," NR >> out $1 }' "$dataset"
    for group in $TMP.group*
    do
	sed -i -e 's/^,/0 /' -e 's/$/\n/' $group
    done
    # The number of classes is the number of group files
    nb_of_classes=$(ls $TMP.group* | wc -l)
    for i in $(seq $nb_of_classes)
    do
	printf "$i\t"
	# Mine skypatterns maximizing growth rate from all classes but the i-th to the i-th
	cat $dataset | awk -v c=$i '{ if ($1 == 1) $1 = c; else if ($1 == c) $1 = 1; print }' > $dataset-class$i
	pushd ../src/aetheris > /dev/null
	/usr/bin/time -o /tmp/time -f "%e" ./skypattern-freq_gr_area.sh $(basename $dataset)-class$i 1 2> /dev/null | wc -l > /tmp/patterns
	popd > /dev/null
	cat /tmp/patterns | tr '\n' '\t'
	cat /tmp/time
	rm $dataset-class$i
    done >> "$dataset.res"

    # Compute totals
    printf "$dataset\t"
    awk '{ patterns += $2; time += $3 } END { print patterns, time }' "$dataset.res"
    # Clean files for next dataset
    rm $TMP.*
done
