#!/bin/sh

# Performance governor for cpufreq settings
# for i in $(seq 0 $(expr $(getconf _NPROCESSORS_ONLN) - 1))
# do
#     sudo cpufreq-set -c $i -g performance
# done

# Headers for totals
printf '# dataset patterns time\n'

TMP=`mktemp -t mine.sh.XXXXXX`
trap "rm $TMP* 2>/dev/null" 0

# Write the file in argument of sky-gr option
printf '0 1' > $TMP

for dataset in "$@"
do
    dataname=$(basename $dataset)
    # Headers for the results
    printf '# class patterns time\n' > "$dataset.res"
    # Create the group files
    awk -v out=$TMP.group '{ for (i = 0; $++i == 0; ) {} printf "," NR >> out i }' "$dataset"
    for group in $TMP.group*
    do
	sed -i -e 's/^,/0 /' -e 's/$/\n/' $group
    done
    # The number of classes is the number of group files
    nb_of_classes=$(ls $TMP.group* | wc -l)
    for i in $(seq $nb_of_classes)
    do
	printf "$i\t"
	# Mine skypatterns maximizing growth rate from all classes but the i-th to the i-th
	cat $dataset | awk -v i=$i '{ tmp = $1; $1 = $i; $i = tmp; print }' | CP+SKY -d /dev/stdin -m fag -s | grep -e Sky -e Time | awk '{gsub("[(][^)]*[)]","")}1' | sed 's/[^0-9|.]*//g' | tr '\n' '\t' && echo ''
    done >> "$dataset.res"
    # Compute totals
    printf "$dataset\t"
    awk '{ patterns += $2; time += $3 } END { print patterns, time }' "$dataset.res"
    # Clean files for next dataset
    rm $TMP.*
done
