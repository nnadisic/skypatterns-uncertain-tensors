#!/bin/sh

# The last version of 2dupehack must be compiled with NB_OF_CLOSED_N_SETS, TIME and GNUPLOT defined in Parameters.h

# Performance governor for cpufreq settings
for i in $(seq 0 $(expr $(getconf _NPROCESSORS_ONLN) - 1))
do
    sudo cpufreq-set -c $i -g performance
done

# Headers for totals
printf '# dataset patterns time\n'

TMP=`mktemp -t mine.sh.XXXXXX`
trap "rm $TMP* 2>/dev/null" 0

# Write the file in argument of sky-gr option
printf '0 1' > $TMP

for dataset in "$@"
do
    # Headers for the results
    printf '# class patterns time\n' > "$dataset.res"
    # Create the group files
    awk -v out=$TMP.group '{ for (i = 0; $++i == 0; ) {} printf "," NR >> out i }' "$dataset"
    for group in $TMP.group*
    do
	sed -i -e 's/^,/0 /' -e 's/$/\n/' $group
    done
    # The number of classes is the number of group files
    nb_of_classes=$(ls $TMP.group* | wc -l)
    # Create the data for multidupehack
    # awk -v p=$nb_of_classes '{ for (i = p; ++i <= NF; ) if ($i == 1) print NR, i, 1 }' "$dataset" > $TMP.data # one tuple per row
    awk -v p=$nb_of_classes '{ for (i = p; ++i <= NF && $i == 0; ) {} } i <= NF { printf NR " " i; while (++i <= NF) if ($i == 1) printf "," i; print " 1" }' "$dataset" > $TMP.data
    for i in $(seq $nb_of_classes)
    do
	printf "$i\t"
	# Mine skypatterns maximizing growth rate from all classes but the i-th to the i-th
    	if [ $i -eq 1 ]
    	then
	    cat $TMP.group$(seq -s " $TMP.group" 2 $nb_of_classes)
    	else
	    if [ $i -eq $nb_of_classes ]
	    then
		cat $TMP.group$(seq -s " $TMP.group" $(expr $nb_of_classes - 1))
	    else
		cat $TMP.group$(seq -s " $TMP.group" $(expr $i - 1)) $TMP.group$(seq -s " $TMP.group" $(expr $i + 1) $nb_of_classes)
	    fi
    	fi | 2dupehack --sky-s 0 --sky-a -g "$TMP.group$i -" --sky-gr $TMP $TMP.data -o /dev/null 2> /dev/null
    done >> "$dataset.res"
    # Compute totals
    printf "$dataset\t"
    awk '{ patterns += $2; time += $3 } END { print patterns, time }' "$dataset.res"
    # Clean files for next dataset
    rm $TMP.*
done
