#!/bin/bash

# Convert ".bin" data files (format used by Ugarte et al. (2015)) to a
# multidupehack-compatible format

# Ex: ./bin2mdh.sh data/*.bin

binfiles=${@}

for bf in $binfiles
do
    echo "Converting ${bf%.*}..."
    cat $bf | grep -v '^#'  | sed 's/[ \t]*$//' | tr " " "," | awk '{print NR,$1,1}' > "${bf%.*}.mdh"
done
