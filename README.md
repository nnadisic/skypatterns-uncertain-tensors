# Mining Skypatterns in Uncertain Tensors

Algorithms and data for the experiments of the article "Mining
Skypatterns in Fuzzy Tensors" (Nadisic et al. 2019).

We use the terms "Fuzzy" and "Uncertain" indistinctively. The change 
of name is due to a reviewer's suggestion after the submission of 
the article.

multidupehack is the algorithm presented in the article. The version
provided is the one used for the experiments features in the article;
a newer version may be available at
http://homepages.dcc.ufmg.br/~lcerf/en/prototypes.html. It
is available under the terms of the GNU GPLv3. 2dupehack is an
implementation of multidupehack slightly modified to be faster when
mining 2-way tensors (matrices). The algorithm is the same: only
implementation details were changed.


## Experiments on UCI datasets

Aetheris is the algorithm presented in the paper "Mining Dominant
Patterns in the Sky" (Soulet et al, 2015). The authors made it
available at https://forge.greyc.fr/projects/skymining/files.

CP+SKY is the algorithm presented in the paper "Mining (Soft-)
Skypatterns Using Dynamic CSP" (Ugarte et al., 2014). The authors made
it available at https://forge.greyc.fr/projects/skymining/files.

DP is the algorithm presented in the paper "Dominance Programming for
Itemset Mining" (Negrevergne et al, 2013). The authors made it
available at
http://www.lamsade.dauphine.fr/~bnegrevergne/webpage/software/dp/.

The `data/` directory contains 23 datasets from the UCI repository, in
the formats managed by the provided prototypes. The datasets in the
formats managed by Aetheris and CP+SKY were kindly provided by Arnaud
Soulet. The script `bin2mdh.sh` was used to convert the files from the
`.bin` format to the `.mdh` format, a multidupehack-compatible format.

### Experiments for freq+area

To run the experiments on the 23 UCI datasets, compile the source
codes provided in `src/`, copy the binaries in `bin/` and run the
script `runtests.sh`. You can select the algorithms to test by
(un)commenting the corresponding blocs in `runtests.sh`.

### Experiments for freq+area+grothwrate

To run the experiments, compile 2dupehack and CP+SKY and put the
executables in your `$PATH`, then execute the two scripts presents
in `xp-freq-area-gr/`, with the desired .txt datasets as parameters.
For example, `./xp-2dh.sh ../*.txt`.

The protocol is different from the previous one because of the
growth-rate measure special input format. For a given dataset, these scripts
consider the growth-rate measure for each classe i, from [all classes but i]
to i. The result produced for a dataset is the sum of all executions concerning
this dataset (one for every class).


## Experiments for skyline frequency-utility patterns

SFUPMinerUemax is the algorithm presented in the paper "A More
Efficient Algorithm to Mine Skyline Frequent Utility" (Lin et al.,
2016). It is part of the SPMF open-source library, available under the
terms of the GNU GPLv3 license at
http://www.philippe-fournier-viger.com/spmf/index.php?link=download.php. The
datasets with utility values used in our paper are available at
http://www.philippe-fournier-viger.com/spmf/index.php?link=datasets.php.


## Experiments on uncertain tensors

The `uncertain-tensors` directory contains the 2 real-life uncertain
tensors used in our paper. We present the commands used to run the
experiments presented in our paper:

For the Twitch/Starcraft experiment: `multidupehack twitch.tv -s "3 3 3" -e "1 1 1" --sky-s "0 1" --slope-points starcraftII_week_points --sky-slope`

To add the contiguity constraint on weeks, add to the previous command: `-t "2 0 0"`

For the Vélov (bicycle network) experiment: `multidupehack velov -s "2 2 4 4" -e "10 10 5 5" -c "2 3" --sky-s "0 1 2"`
